#include "blis.h"
#include <omp.h>

unsigned long long rdtsc()
{
    unsigned a, d;
    __asm__ volatile("rdtsc" : "=a" (a), "=d" (d));
    return ((unsigned long long)a) | (((unsigned long long)d) << 32);
}

extern blksz_t*          gemm_mr;
extern blksz_t*          gemm_kr;

int main()
{
    bli_init();
    _Pragma("omp parallel"){
    obj_t tom_hanks; //In the movie Big
    bli_obj_create(BLIS_DOUBLE, 4096, 4096, 0, 0, &tom_hanks);
    
    for( int i = 8; i <= 512; i+= 8){
        //Create and allocate matrix a
        obj_t a;
        bli_obj_create(BLIS_DOUBLE, i, i, 0, 0, &a);
        //bli_obj_create(BLIS_DOUBLE, 4096, 4096, 0, 0, &a);
        bli_setm( &BLIS_ZERO, &a );

        //Create and allocate packed buffer a_pack
        obj_t a_pack;
        bli_obj_init_pack( &a_pack );
        packm_t* pack_cntl =  bli_packm_cntl_obj_create( BLIS_BLOCKED,
                            BLIS_VARIANT1, 
                            gemm_mr,
                            gemm_kr,
                            FALSE, // do NOT invert diagonal
                            FALSE, // reverse iteration if upper?
                            FALSE, // reverse iteration if lower?
                            BLIS_PACKED_ROW_PANELS,
                            BLIS_BUFFER_FOR_GEN_USE );
        
        bli_packm_init( &a, &a_pack, pack_cntl );


        //Setup checksum vectors
        obj_t w, d;
        bli_obj_create( BLIS_DOUBLE, bli_obj_width(a), 1, 0, 0, &w );
        bli_obj_create( BLIS_DOUBLE, bli_obj_length(a), 1, 0, 0, &d );
        bli_setv( &BLIS_ONE, &w ); 
        bli_setv( &BLIS_ZERO, &d ); 


        bool_t checksum=1;

        double bestPerf = 0.0;
        for (int repeat=0; repeat < 5; repeat++)
        {
            long long t0 = rdtsc();
            if( checksum ) {
                bli_packm_gemv_int( &a, &a_pack, &w, &d, 1, pack_cntl, &BLIS_PACKM_SINGLE_THREADED );
            }
            else
            {
                bli_packm_int( &a, &a_pack, pack_cntl, &BLIS_PACKM_SINGLE_THREADED );
            }
            long long t1 = rdtsc();
            if( (1.0 / (t1 - t0)) > bestPerf )
                bestPerf = 1.0 / (t1 - t0);

            //Clear caches
            bli_setm( &BLIS_MINUS_ONE, &tom_hanks );
        }
        if( omp_get_thread_num() == 0 ){
            printf( "size:\t%d\t", i );
            printf( "Performance:\t%lf\tGB/s\n", 2.701 * 8.0*(BLIS_DEFAULT_MC_D * BLIS_DEFAULT_KC_D) * bestPerf );
        }
        _Pragma("omp barrier");
    }
}}

