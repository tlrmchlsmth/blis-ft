/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2014, The University of Texas at Austin

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas at Austin nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "blis.h"
#include "immintrin.h"
#include "ft_defs.h"
#include "assert.h"
double threshold = .00000000001;
bool_t do_err_packa;
bool_t do_err_packb;

int macro_id[240];
int packa_micro_id[240];
int next_potential_A_err_index[240];
int next_potential_B_err_index[240];
int next_potential_C_err_index[240];

error_location_t A_err_locs[1000];
error_location_t B_err_locs[1000];
error_location_t C_err_locs[1000];
int num_a_err_locs;
int num_b_err_locs;
int num_c_err_locs;

int error_tracker = 0;

obj_t local_Cvs[240];
void* local_Cvs_toReduces[15][16];

void bli_reset_err_locs()
{
    for(int i = 0; i < 240; i++){
        macro_id[i] = 0;
        next_potential_A_err_index[i] = 0;
        next_potential_B_err_index[i] = 0;
        next_potential_C_err_index[i] = 0;
    }
    num_a_err_locs = 0;
    num_b_err_locs = 0;
    num_c_err_locs = 0;
}

//clear out elements with indices modulo 30 and 31 
//cuz of some block size extension bullshit
void clear_out_deadspace( obj_t* vec )
{
    double* vecBuf = bli_obj_buffer_at_off( *vec );
    dim_t m = bli_obj_length( *vec );
    for(int i = 0; i < m; i+= 32)
    {
        vecBuf[i + 30] = 0.0;
        vecBuf[i + 31] = 0.0;
    }
}

void recompute_blocks( obj_t* alpha_obj, obj_t *a_obj, obj_t *b_obj, obj_t* beta_obj, obj_t *c_obj,
                       recompute_block_t *blocks, dim_t num_blocks, gemm_thrinfo_t* thread )
{
    if(num_blocks == 0)
        return;
    dim_t rs_a = bli_obj_row_stride( *a_obj );
    dim_t cs_a = bli_obj_col_stride( *a_obj );
    dim_t rs_b = bli_obj_row_stride( *b_obj );
    dim_t cs_b = bli_obj_col_stride( *b_obj );
    dim_t rs_c = bli_obj_row_stride( *c_obj );
    dim_t cs_c = bli_obj_col_stride( *c_obj );
    
    double* a = bli_obj_buffer_at_off( *a_obj );
    double* b = bli_obj_buffer_at_off( *b_obj );
    double* c = bli_obj_buffer_at_off( *c_obj );
    double alpha = ((double*)bli_obj_buffer( *alpha_obj ))[0];
    double beta  = ((double*)bli_obj_buffer( *beta_obj ))[0];

    dim_t MR = 30;
    dim_t NR = 8;
    dim_t k  = BLIS_DEFAULT_KC_D;
    if( bli_obj_width( *a_obj )  < k )
        k = bli_obj_width( *a_obj );

    __declspec(align(64)) double utile[30*8];
    for( int block = thread_id( thread ); block < num_blocks; block += thread_num_threads( thread ) )
    {
        dim_t m_coord = blocks[block].m_coord;
        dim_t n_coord = blocks[block].n_coord;
        dim_t k_coord = blocks[block].k_coord;
        for(int i = 0; i < MR; i++){
            for(int j = 0; j < NR; j++)
            {
                double d = 0.0;
                for(int p = 0; p < k; p++){
                    double a_val = a[ (i+m_coord)*rs_a + p+k_coord ];
                    double b_val = b[ (p+k_coord)*rs_b + (j+n_coord) ];
                    d += a_val * b_val;
                }
                double *c_elem = &c[(m_coord+i)*rs_c + (n_coord+j)*cs_c];
                *c_elem = beta * *c_elem + alpha * d;
            }
        }
/*            
        for( int i = 0; i < MR; i++){
            for( int j = 0; j < NR; j++){
                utile[i*8 + j] = 0.0;
            }
        }
        printf("%d\t%d\t%d\n", m_coord, n_coord, k_coord);
    
            for( int p = 0; p < k; p++)
            {
                for( int i = 0; i < 30; i++)
                {
                    for( int j = 0; j < 8; j++)
                    {
                        double a_val = a[ (i+m_coord)*rs_a + p+k_coord ];
                        double b_val = b[ (p+k_coord)*rs_b + (j+n_coord) ];
                        
                        utile[i*8 + j] += a_val * b_val ;
                    }
                }
            }

        for( int i = 0; i < MR; i++){
            for( int j = 0; j < NR; j++){
                double *c_elem = &c[(m_coord+i)*rs_c + (n_coord+j)*cs_c];
                *c_elem = beta * *c_elem + alpha * utile[i*8 + j];
            }
        }
        */
    }
}

//void rollback_microtile( double* c, dim_t cs_c, double* chkpt, dim_t cs_chkpt, dim_t m, dim_t n)
void rollback_microtile( double* c, dim_t rs_c, double* chkpt, dim_t rs_chkpt, dim_t m, dim_t n)
{
    for(int i = 0; i < BLIS_DEFAULT_MR_D; i++)
    {
        for(int j = 0; j < BLIS_DEFAULT_NR_D; j+=8)
        {
            __m512d reg = _mm512_load_pd( chkpt + (m+i)*rs_chkpt + (n+j));
            _mm512_store_pd( c + (m+i)*rs_c + (n+j), reg);
        }
    }
}

//Location of error in Cv gives the M coord of the error in C
//Location of error in wC gives the N coord of the error in C
bool_t scanErrors( obj_t *Cv, obj_t *wC, obj_t* c, obj_t* chkpt, 
                   dim_t offsetM, dim_t offsetN, dim_t offsetK,
                   recompute_block_t *blocks, dim_t *n_blocks )
{
    double *wC_cast = (double*) bli_obj_buffer( *wC );
    double *Cv_cast = (double*) bli_obj_buffer( *Cv );
    
    double *c_cast      = (double*) bli_obj_buffer_at_off( *c );
    double *chkpt_cast  = (double*) bli_obj_buffer_at_off( *chkpt );

    dim_t cs_c = bli_obj_col_stride( *c );
    dim_t cs_chkpt = bli_obj_col_stride( *chkpt );
    dim_t rs_c = bli_obj_row_stride( *c );
    dim_t rs_chkpt = bli_obj_row_stride( *chkpt );

    dim_t num_wC_errs = 0;
    dim_t last_wC_err_coord = 0;
    dim_t num_Cv_errs = 0;
    dim_t last_Cv_err_coord = 0;
    
//    dim_t m = bli_obj_length( *Cv );
    dim_t m = 120;
    dim_t n = bli_obj_width( *wC );

    dim_t MR = BLIS_DEFAULT_MR_D;
    dim_t NR = BLIS_DEFAULT_NR_D;

    bool_t need_to_repack_b = 0;

    dim_t jcoords[240];
    dim_t icoords[240];

    for( int j = 0; j < n; j += NR ){
        int foundErr = 0;
        for( int jj = 0; jj < NR; jj++) {
            if(bli_abs(wC_cast[j+jj]) > threshold){
                foundErr = 1;
            }
        }
        if(foundErr){
            if( num_wC_errs < 16 )
                jcoords[num_wC_errs] = j;
            num_wC_errs++;
            last_wC_err_coord = j;
        }
    }
    for( int i = 0; i < m; i += MR ){
        int foundErr = 0;
        for( int ii = 0; ii < MR; ii++) {
            if(bli_abs(Cv_cast[ii + i / MR * 32]) > threshold){
                foundErr = 1;
            }
        }
        if(foundErr){
            if( num_Cv_errs < 16 )
                icoords[num_Cv_errs] = i;
            num_Cv_errs++;
            last_Cv_err_coord = i;
        }
    }
    
    //No errors detected. Shouldn't happen since this function only gets called if the norm of Cv > thresh
    if( num_wC_errs == 0 && num_Cv_errs == 0){
    }
    
    //1 row of C micro-panels are corrputed
    else if ( num_Cv_errs == 1 && num_wC_errs == n / NR ){
        //printf("1 row of C micro-kernels corrupted. %d\n", last_Cv_err_coord );
        for( int j = 0; j < n; j+=NR ){
            dim_t bindex;
            _Pragma("omp atomic capture")
                bindex = (*n_blocks)++;
            assert( *n_blocks < MAX_NUM_BLOCKS );
            blocks[bindex].n_coord = j + offsetN;
            blocks[bindex].m_coord = last_Cv_err_coord + offsetM;
            blocks[bindex].k_coord = offsetK;
            rollback_microtile( c_cast, rs_c, chkpt_cast, rs_chkpt, last_Cv_err_coord, j);
        }
    }

    //1 column of C micro-panels are corrputed
    else if ( num_wC_errs == 1 && num_Cv_errs == m / MR){
        //printf("1 col of C micro-kernels corrupted. %d\n", last_wC_err_coord );
        need_to_repack_b = 1;
        for( int i = 0; i < m; i+=MR ){
            dim_t bindex;
            _Pragma("omp atomic capture")
                bindex = (*n_blocks)++;
            assert( *n_blocks < MAX_NUM_BLOCKS );
            blocks[bindex].n_coord = last_wC_err_coord + offsetN;
            blocks[bindex].m_coord = i + offsetM;
            blocks[bindex].k_coord = offsetK;
//            rollback_microtile( c_cast, cs_c, chkpt_cast, cs_chkpt, i + offsetM, last_wC_err_coord );
            rollback_microtile( c_cast, rs_c, chkpt_cast, rs_chkpt, i, last_wC_err_coord );
        }
    }

    //some micro-kernel corrupted
    else if ( num_Cv_errs <= 16 && num_wC_errs <= 16 ) {
        for( int i = 0; i < num_Cv_errs; i++ )
        {
            for( int j = 0; j < num_wC_errs; j++ )
            {
//                printf("1 error detected at %d, %d\n", icoords[i], jcoords[j]);
                dim_t bindex;
                _Pragma("omp atomic capture")
                    bindex = (*n_blocks)++;
                assert( *n_blocks < MAX_NUM_BLOCKS );
                blocks[bindex].n_coord = jcoords[j] + offsetN;
                blocks[bindex].m_coord = icoords[i] + offsetM;
                blocks[bindex].k_coord = offsetK;
                rollback_microtile( c_cast, rs_c, chkpt_cast, rs_chkpt, icoords[i], jcoords[j] );
            }
        }
    }


    
    //Whole C is corrputed
    else{
        printf("WHOLE C CORRUPTED!! num Cv errs: %d\t num wC errs: %d\n", num_Cv_errs, num_wC_errs);
        printf("whole C corrupted\n");
        for( int i = 0; i < m; i+=MR ){
            for( int j = 0; j < n; j+=NR ){
                dim_t bindex;
                _Pragma("omp atomic capture")
                    bindex = (*n_blocks)++;
                assert( *n_blocks < MAX_NUM_BLOCKS );
                blocks[bindex].n_coord = j + offsetN;
                blocks[bindex].m_coord = i + offsetM;
                blocks[bindex].k_coord = offsetK;
                //rollback_microtile( c_cast, cs_c, chkpt_cast, cs_chkpt, i + offsetM, j );
                rollback_microtile( c_cast, rs_c, chkpt_cast, rs_chkpt, i, j );
            }
        }
    }
    return need_to_repack_b;
}

unsigned long long rdtsc()
{
    unsigned a, d;
    __asm__ volatile("rdtsc" : "=a" (a), "=d" (d));
    return ((unsigned long long)a) | (((unsigned long long)d) << 32);
}

bool_t check_norm( obj_t* abram )
{
    if( bli_obj_is_double( *abram ) )
    {
        double* buf = bli_obj_buffer( *abram );
        double norm = *buf;
        //printf("%e\n", norm);
        return bli_abs( norm ) > 1E-12;
    }
    return 0;
}

void bli_obj_create_managed( num_t dt, dim_t m, dim_t n, dim_t rs, dim_t cs, obj_t* obj, mem_t* mem )
{
    bli_obj_create_without_buffer( dt, m, n, obj );

    size_t elem_size = bli_obj_elem_size( *obj );

    bli_adjust_strides( m, n, &rs, &cs );
    dim_t rs_abs = bli_abs( rs );
    dim_t cs_abs = bli_abs( cs );
    dim_t n_elem = 0;

    // Determine how much object to allocate.
    if ( m == 0 || n == 0 ) 
        n_elem = 0;
    else if ( rs_abs == 1 || rs_abs < cs_abs ){
        cs     = bli_align_dim_to_size( cs, elem_size,
                                        BLIS_HEAP_STRIDE_ALIGN_SIZE );
        n_elem = bli_abs( cs ) * n;
    }   
    else if ( cs_abs == 1 || cs_abs < rs_abs ) {
        rs     = bli_align_dim_to_size( rs, elem_size,
                                        BLIS_HEAP_STRIDE_ALIGN_SIZE );
        n_elem = bli_abs( rs ) * m;
    }   
    else {
        if ( m != 1 || m != 1 ) 
            bli_check_error_code( BLIS_NOT_YET_IMPLEMENTED );

            n_elem = 1;
    }
    if(m == 1 || n == 1) {
        bli_mem_acquire_m( n_elem * elem_size, BLIS_BUFFER_FOR_CHECKSUM, mem );
    }
    else {
        bli_mem_acquire_m( n_elem * elem_size, BLIS_BUFFER_FOR_C_PANEL, mem );
    }

    bli_obj_set_buffer( bli_mem_buffer( mem ), *obj );
    bli_obj_set_incs( rs, cs, *obj );
}

void bli_obj_release_managed( obj_t* a )
{
    bli_mem_release( bli_obj_buffer( *a ) );
}

recompute_block_t blocks[MAX_NUM_BLOCKS];
dim_t n_blocks = 0;
bool_t do_err = 1;

void bli_gemm_blk_var1f( obj_t*  a,
                         obj_t*  b,
                         obj_t*  c,
                         gemm_t* cntl,
                         gemm_thrinfo_t* thread )
{
    long long pack_b_time = 0;
    long long pack_a_time = 0;
    long long macrokernel_time = 0;
    long long update_c_time = 0;
    long long norm_time = 0;

    long long startall = rdtsc();
    
    assert( bli_obj_col_stride( *c ) == 1);
    assert( bli_obj_col_stride( *a ) == 1);
    assert( bli_obj_col_stride( *b ) == 1);

    assert( bli_obj_width( *c ) % 8 == 0 );
    assert( bli_obj_length( *c ) % 30 == 0 );

#ifndef LAZY
    dim_t n_blocks_s = 0;
    dim_t *n_blocks_greedy = NULL;
    n_blocks_greedy = thread_ibroadcast( thread, &n_blocks_s );
#endif

    bool_t need_to_repack_b = 0;

    //The s is for "lives on the stack"
    obj_t b_pack_s;
    obj_t a1_pack_s;

    obj_t a1, c1; 
    obj_t* a1_pack  = NULL;
    obj_t* b_pack   = NULL;

	dim_t i;
	dim_t b_alg;
	dim_t m_trans = bli_obj_length_after_trans( *a );
    num_t dt = bli_obj_datatype(*a);
    dim_t b_alg_max = bli_determine_blocksize_f( 0, m_trans, a, cntl_blocksize( cntl ) );

    //Setup localCV used by only this thread
    mem_t local_Cv_mem;
    bli_obj_create_managed( dt, 128, 1, 0, 0, &local_Cvs[omp_get_thread_num()], &local_Cv_mem );

    //Setup v, w, and Bv checksum vectors that are used by all threads
    obj_t v_s, Bv_s, w_s;
    obj_t w1;
    mem_t v_mem, Bv_mem, w_mem;
    obj_t *v, *Bv, *w;
    obj_t* oArray_s[4];
    obj_t** oArray;

    if( thread_am_ochief( thread ) ) { 
        bli_obj_create_managed( dt, bli_obj_width_after_trans(*b), 1, 0, 0, &v_s, &v_mem );
        bli_obj_create_managed( dt, bli_obj_length_after_trans(*b), 1, 0, 0, &Bv_s, &Bv_mem );
        bli_obj_create_managed( dt, b_alg_max, 1, 0, 0, &w_s, &w_mem );
        bli_setv( &BLIS_ONE, &v_s ); 
        bli_setv( &BLIS_ZERO, &Bv_s ); 
        bli_setv( &BLIS_ONE, &w_s ); 
	    bli_obj_init_pack( &b_pack_s );
	    bli_packm_init( b, &b_pack_s,
	                    cntl_sub_packm_b( cntl ) );

        oArray_s[0] = &v_s; 
        oArray_s[1] = &Bv_s; 
        oArray_s[2] = &w_s; 
        oArray_s[3] = &b_pack_s; 
    }
    /*v = thread_obroadcast( thread, &v_s );
    Bv = thread_obroadcast( thread, &Bv_s );
    w = thread_obroadcast( thread, &w_s );*/
//    b_pack = thread_obroadcast( thread, &b_pack_s );

    oArray = thread_obroadcast( thread, &oArray_s );
    v  = oArray[0];
    Bv = oArray[1];
    w  = oArray[2];
    b_pack = oArray[3];



	// Pack B (if instructed).
    long long start_t = rdtsc();
	bli_packm_gemv_int( b, b_pack,
                   v, Bv, 0,
	               cntl_sub_packm_b( cntl ),
                   gemm_thread_sub_opackm( thread ) );
    thread_obarrier( thread );
    pack_b_time = rdtsc() - start_t;

	// Query dimension in partitioning direction.
    dim_t start, end;
    bli_get_range( thread, 0, m_trans, bli_determine_reg_blocksize( a, cntl_blocksize( cntl ) ),
                   &start, &end );

    //Create temporary buffer for updating C and checksums
    obj_t ABv_s, Cv_s, abram_s, chkpt_s, wC_s;
    obj_t ABv1, Cv1, chkpt1;
    obj_t *ABv, *Cv, *abram, *chkpt, *wC;
    mem_t ABv_mem, Cv_mem, abram_mem, chkpt_mem, wC_mem;

    obj_t*  iArray_s[6];
    obj_t** iArray;

    if( thread_am_ichief( thread ) ) {
        bli_obj_create_managed( dt, (b_alg_max / 30) * 32, 1, 0, 0, &ABv_s, &ABv_mem );
        bli_obj_create_managed( dt, (b_alg_max / 30) * 32, 1, 0, 0, &Cv_s, &Cv_mem );
        bli_obj_create_managed( dt, 1, bli_obj_width(*c), 0, 0, &wC_s, &wC_mem );
        bli_obj_create_managed( dt, 1, 1, 0, 0, &abram_s, &abram_mem );
        bli_obj_create_managed( dt, b_alg_max, bli_obj_width(*c), bli_obj_width(*c), 1, &chkpt_s, &chkpt_mem );
        bli_obj_scalar_apply_scalar( &BLIS_ZERO, &chkpt_s );
        bli_obj_init_pack( &a1_pack_s );

        iArray_s[0] = &ABv_s;
        iArray_s[1] = &Cv_s;
        iArray_s[2] = &wC_s;
        iArray_s[3] = &abram_s;
        iArray_s[4] = &chkpt_s;
        iArray_s[5] = &a1_pack_s;
    }
/*    chkpt = thread_ibroadcast( thread, &chkpt_s );
    ABv   = thread_ibroadcast( thread, &ABv_s );
    Cv    = thread_ibroadcast( thread, &Cv_s );
    abram = thread_ibroadcast( thread, &abram_s);
    wC    = thread_ibroadcast( thread, &wC_s );
    a1_pack = thread_ibroadcast( thread, &a1_pack_s );*/
    iArray = thread_ibroadcast( thread, &iArray_s );
     ABv = iArray[0];
     Cv = iArray[1];
     wC = iArray[2];
     abram = iArray[3];
     chkpt = iArray[4];
     a1_pack = iArray[5];

    gemm_thrinfo_t* hyperthread = gemm_thread_sub_gemm(thread);

	// Partition along the m dimension.
	for ( i = start; i < end; i += b_alg )
	{
		// Determine the current algorithmic blocksize.
		// NOTE: Use of a (for execution datatype) is intentional!
		// This causes the right blocksize to be used if c and a are
		// complex and b is real.
		b_alg = bli_determine_blocksize_f( i, end, a,
		                                   cntl_blocksize( cntl ) );
        
		// Acquire partitions for A1 and C1 and vectors that are allocated extra-long
		bli_acquire_mpart_t2b( BLIS_SUBPART1,
		                       i, b_alg, a, &a1 );
		bli_acquire_mpart_t2b( BLIS_SUBPART1,
  		                       i, b_alg, c, &c1 );
		bli_acquire_mpart_t2b( BLIS_SUBPART1,
  		                       0, b_alg, chkpt, &chkpt1 );
		bli_acquire_mpart_t2b( BLIS_SUBPART1,
  		                       0, (b_alg / 30) * 32, ABv, &ABv1 );
		bli_acquire_mpart_t2b( BLIS_SUBPART1,
  		                       0, (b_alg / 30) * 32, Cv, &Cv1 );
		bli_acquire_mpart_t2b( BLIS_SUBPART1,
  		                       0, b_alg, w, &w1 );
		
        if( gemm_thread_do_compute( hyperthread ) ) {
           if( thread_am_ichief( hyperthread ) ) {
                bli_setv( &BLIS_ZERO, &ABv1 ); 
                bli_setv( &BLIS_ZERO, &Cv1 ); 
                bli_setv( &BLIS_ZERO, wC ); 
           }

            // Initialize objects for packing A1 and C1.
            if( thread_am_ichief( thread ) ) {
                bli_packm_init( &a1, a1_pack,
                                cntl_sub_packm_a( cntl ) );
            }
            thread_ibarrier( hyperthread );
        
            start_t = rdtsc();
            // Pack A1 (if instructed).
            packa_micro_id[omp_get_thread_num()] = 0;
            bli_packm_gemv_int( &a1, a1_pack,
                           Bv, &ABv1, 1,
                           cntl_sub_packm_a( cntl ),
                           gemm_thread_sub_ipackm( hyperthread ) );
            pack_a_time += rdtsc() - start_t;

            thread_ibarrier( hyperthread ); //packing must be done before computation
            
            start_t = rdtsc();
            // Perform gemm subproblem.
            bli_gemm_ft_ker_var2( 
                          a1_pack,
                          b_pack,
                          &c1, &chkpt1,
                          v, &Cv1,
                          &w1, wC,
                          cntl_sub_gemm( cntl ),
                          gemm_thread_sub_gemm( hyperthread ) );
            macrokernel_time += rdtsc() - start_t;
            
            thread_ibarrier( hyperthread ); //Make sure all computation is done before we update.
            
            macro_id[omp_get_thread_num()]++;
            need_to_repack_b = 0;

            //Need to do some of this in parallel...
            if( thread_am_ichief( hyperthread ) ) { 
                start_t = rdtsc();
                bli_subv(&ABv1, &Cv1);
                clear_out_deadspace( &Cv1 );
                bli_normfv(&Cv1, abram);
                //bli_printv("Cv1", bli_obj_length(ABv1), "%5.5f", "");
                if( check_norm(abram) )
                {
                    //There is an error, nov detect location(s)
                //    printf("ERROR!\n");fflush(0);
                    obj_t wA, wAB;
                    mem_t wA_mem, wAB_mem;
                    bli_obj_create_managed( dt, 1, bli_obj_width_after_trans(a1), 0, 0, &wA, &wA_mem );
                    bli_obj_create_managed( dt, 1, bli_obj_width_after_trans(*b), 0, 0, &wAB, &wAB_mem );
                    bli_obj_induce_trans( a1 ); bli_gemv( &BLIS_ONE, &a1, &w1, &BLIS_ZERO, &wA ); bli_obj_induce_trans( a1 );
                    bli_obj_induce_trans( *b ); bli_gemv( &BLIS_ONE, b, &wA, &BLIS_ZERO, &wAB ); bli_obj_induce_trans( *b );
                    bli_subm( &wAB, wC );

                    dim_t offsetM = 0;
                    dim_t offsetN = 0;
                    dim_t offsetK = 0;
#ifdef LAZY
                    offsetM = bli_obj_row_offset( c1 );
                    offsetN = bli_obj_col_offset( c1 );
                    //offsetK = bli_obj_col_offset( a1 ); offsetk is always zero since we correct during this rank-k update
                    need_to_repack_b = scanErrors( &Cv1, wC, &c1, &chkpt1, offsetM, offsetN, offsetK, blocks, &n_blocks );
#else
                    need_to_repack_b = scanErrors( &Cv1, wC, &c1, &chkpt1, offsetM, offsetN, offsetK, blocks, n_blocks_greedy );
#endif

                    if( need_to_repack_b )
                        bli_setv( &BLIS_ZERO, &Bv_s ); 

                    //Free left sided vectors here
                    bli_mem_release( &wA_mem );
                    bli_mem_release( &wAB_mem );
                }
            }
#ifndef LAZY //Greedy recomputation here!

        obj_t scalar_a, scalar_b, beta;
        bli_obj_scalar_detach( c, &beta ); 
        bli_obj_scalar_detach( a, &scalar_a );
        bli_obj_scalar_detach( b, &scalar_b );
        bli_mulsc( &scalar_a, &scalar_b );

        thread_ibarrier( thread );
        recompute_blocks( &scalar_b, &a1, b, &beta, &c1,
                          blocks, *n_blocks_greedy, hyperthread );
        thread_ibarrier( thread );
        *n_blocks_greedy = 0;
#endif


#ifdef ENABLE_REPACK_B_ON_ERROR
            if( i == start )
            {
                need_to_repack_b = *((bool_t*) thread_obroadcast( thread, &need_to_repack_b ));
                if( need_to_repack_b ) {
                    //printf("REPACKING B\n");
                    bli_packm_gemv_int( b, b_pack,
                                   v, Bv, 0,
                                   cntl_sub_packm_b( cntl ),
                                   gemm_thread_sub_opackm( thread ) );
                }
            }
#endif
        }
        thread_ibarrier( thread ); //Make sure all computation is done before we update.
	}
#ifdef ENABLE_REPACK_B_ON_ERROR
        if( start >= end )
        {
            need_to_repack_b = *((bool_t*) thread_obroadcast( thread, &need_to_repack_b ));
            if( need_to_repack_b ) {
                bli_packm_gemv_int( b, b_pack,
                               v, Bv, 0,
                               cntl_sub_packm_b( cntl ),
                               gemm_thread_sub_opackm( thread ) );
                thread_obarrier( thread );
            }
        }
#endif

    // If any packing buffers were acquired within packm, release them back
	// to the memory manager.
    bli_mem_release( &local_Cv_mem );
    thread_obarrier( thread );
    if( thread_am_ochief( thread ) ){
	    bli_obj_release_pack( b_pack );
        bli_mem_release( &v_mem );
        bli_mem_release( &Bv_mem );
        bli_mem_release( &w_mem );
    }
    if( thread_am_ichief( thread ) ){
        bli_obj_release_pack( a1_pack );
        bli_mem_release( &ABv_mem );
        bli_mem_release( &Cv_mem );
        bli_mem_release( &abram_mem );
        bli_mem_release( &chkpt_mem );
        bli_mem_release( &wC_mem );
    }
}

