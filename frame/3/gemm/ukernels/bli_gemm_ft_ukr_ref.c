/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2014, The University of Texas at Austin

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas at Austin nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "blis.h"

void dgemm_ukernel_ft_ref( dim_t k,
                       double* restrict alpha,
                       double* restrict a,
                       double* restrict b,
                       double* restrict beta,
                       double* restrict c, inc_t rs_c, inc_t cs_c,
                       double* restrict chkpt, inc_t rs_chkpt, inc_t cs_chkpt,
                       double* restrict v, double* restrict Cv,
                       double* restrict w, double* restrict wC,
                       auxinfo_t* data )
{
    const dim_t mr = BLIS_DEFAULT_MR_D;
    const dim_t nr = BLIS_DEFAULT_NR_D;

    //Need to have a temporary buffer so we can perform checksum...
    double AB[BLIS_DEFAULT_MR_D * BLIS_DEFAULT_NR_D];
    double ZERO = 0.0;

    //Call microkernel
    BLIS_DGEMM_UKERNEL( k,
                   alpha, 
                   a, 
                   b,
                   &ZERO,
                   AB, 1, BLIS_DEFAULT_MR_D,
                   data );


    for( int i = 0; i < mr; i++ ){
        for( int j = 0; j < nr; j++ ){
                Cv[i] += AB[i*1 + j*BLIS_DEFAULT_MR_D] * v[j]; //Perform checksum
                wC[j] += w[i] * AB[i*1 + j*BLIS_DEFAULT_MR_D];
                chkpt[i*rs_chkpt + j*cs_chkpt] = c[i*rs_c + j*cs_c];
                c[i*rs_c + j*cs_c] = *beta * c[i*rs_c + j*cs_c] + AB[i*1 + j*BLIS_DEFAULT_MR_D];
        }
    }
}

void sgemm_ukernel_ft_ref( dim_t k,
                       float* restrict alpha,
                       float* restrict a,
                       float* restrict b,
                       float* restrict beta,
                       float* restrict c, inc_t rs_c, inc_t cs_c,
                       float* restrict chkpt, inc_t rs_chkpt, inc_t cs_chkpt,
                       float* restrict v, float* restrict Cv,
                       float* restrict w, float* restrict wC,
                       auxinfo_t* data )
{
}

void cgemm_ukernel_ft_ref( dim_t k,
                       scomplex* restrict alpha,
                       scomplex* restrict a,
                       scomplex* restrict b,
                       scomplex* restrict beta,
                       scomplex* restrict c, inc_t rs_c, inc_t cs_c,
                       scomplex* restrict chkpt, inc_t rs_chkpt, inc_t cs_chkpt,
                       scomplex* restrict v, scomplex* restrict Cv,
                       scomplex* restrict w, scomplex* restrict wC,
                       auxinfo_t* data )
{
}

void zgemm_ukernel_ft_ref( dim_t k,
                       dcomplex* restrict alpha,
                       dcomplex* restrict a,
                       dcomplex* restrict b,
                       dcomplex* restrict beta,
                       dcomplex* restrict c, inc_t rs_c, inc_t cs_c,
                       dcomplex* restrict chkpt, inc_t rs_chkpt, inc_t cs_chkpt,
                       dcomplex* restrict v, dcomplex* restrict Cv,
                       dcomplex* restrict w, dcomplex* restrict wC,
                       auxinfo_t* data )
{
}
