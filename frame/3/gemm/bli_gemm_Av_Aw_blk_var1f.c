/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2014, The University of Texas at Austin

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas at Austin nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "blis.h"

unsigned long long rdtsc();
void check_norm( obj_t* abram );
void bli_obj_create_managed( num_t dt, dim_t m, dim_t n, dim_t rs, dim_t cs, obj_t* obj, mem_t* mem );
void bli_obj_release_managed( obj_t* a );

void bli_set_feistv( obj_t* w )
{
    double* w_cast = (double*) bli_obj_buffer( *w );
    for( int i = 0; i < bli_obj_length( *w ); i++)
    {
        w_cast[i] = (double) i;
    }
}

void bli_gemm_Av_Aw_blk_var1f( obj_t*  a,
                         obj_t*  b,
                         obj_t*  c,
                         gemm_t* cntl,
                         gemm_thrinfo_t* thread )
{
    long long pack_b_time = 0;
    long long pack_a_time = 0;
    long long macrokernel_time = 0;
    long long update_c_time = 0;
    long long norm_time = 0;

    long long startall = rdtsc();

    //The s is for "lives on the stack"
    obj_t b_pack_s;
    obj_t a1_pack_s;

    obj_t a1, c1; 
    obj_t* a1_pack  = NULL;
    obj_t* b_pack   = NULL;

	dim_t i;
	dim_t b_alg;
	dim_t m_trans;

    //Setup checksum vectors for use by all threads
    num_t dt = bli_obj_datatype(*a);
    obj_t v_s, Bv_s;
    obj_t w_s, Bw_s;
    mem_t v_mem, Bv_mem;
    mem_t w_mem, Bw_mem;
    obj_t *v, *Bv;
    obj_t *w, *Bw;
    
    if( thread_am_ochief( thread ) ) { 
        bli_obj_create_managed( dt, bli_obj_width_after_trans(*b), 1, 0, 0, &v_s, &v_mem );
        bli_obj_create_managed( dt, bli_obj_length_after_trans(*b), 1, 0, 0, &Bv_s, &Bv_mem );
        bli_obj_create_managed( dt, bli_obj_width_after_trans(*b), 1, 0, 0, &w_s, &w_mem );
        bli_obj_create_managed( dt, bli_obj_length_after_trans(*b), 1, 0, 0, &Bw_s, &Bw_mem );
        bli_setv( &BLIS_ONE, &v_s ); 
        bli_setv( &BLIS_ZERO, &Bv_s ); 
        bli_set_feistv( &w_s );
        bli_setv( &BLIS_ZERO, &Bw_s ); 
    }
    v = thread_obroadcast( thread, &v_s );
    Bv = thread_obroadcast( thread, &Bv_s );
    w = thread_obroadcast( thread, &w_s );
    Bw = thread_obroadcast( thread, &Bw_s );

    
    if( thread_am_ochief( thread ) ) {
	    // Initialize object for packing B.
	    bli_obj_init_pack( &b_pack_s );
	    bli_packm_init( b, &b_pack_s,
	                    cntl_sub_packm_b( cntl ) );

        // Scale C by beta (if instructed).
        // Since scalm doesn't support multithreading yet, must be done by chief thread (ew)
        bli_scalm_int( &BLIS_ONE,
                       c,
                       cntl_sub_scalm( cntl ) );
    }
    b_pack = thread_obroadcast( thread, &b_pack_s );

	// Initialize objects passed into bli_packm_init for A and C
    if( thread_am_ichief( thread ) ) {
        bli_obj_init_pack( &a1_pack_s );
    }
    a1_pack = thread_ibroadcast( thread, &a1_pack_s );
    
	// Pack B (if instructed).
    long long start_t = rdtsc();
	bli_packm_Av_Aw_int( b, b_pack,
                   v, Bv,
                   w, Bw,
                   0,
	               cntl_sub_packm_b( cntl ),
                   gemm_thread_sub_opackm( thread ) );
    pack_b_time = rdtsc() - start_t;

	// Query dimension in partitioning direction.
	m_trans = bli_obj_length_after_trans( *a );
    dim_t start, end;
    bli_get_range( thread, 0, m_trans, 
                   bli_determine_reg_blocksize( a, cntl_blocksize( cntl ) ),
                   &start, &end );

    //Create temporary buffer for updating C and checksums
    obj_t c_tmp_s, c_tmp_1;
    obj_t *c_tmp;
    obj_t ABv_s, ABw_s, Cv_s, Cw_s, abram_s, macdonald_s;
    obj_t *ABv, *ABw, *Cv, *Cw, *abram, *macdonald;
    mem_t ABv_mem, ABw_mem, Cw_mem, Cv_mem, abram_mem, macdonald_mem, c_tmp_mem;

    //Determine maximum size for c_tmp
    dim_t b_alg_max = bli_determine_blocksize_f( 0, m_trans, a, cntl_blocksize( cntl ) );
    
    if( thread_am_ichief( thread ) ) {
        bli_obj_create_managed( dt, b_alg_max, bli_obj_width(*c), 0, 0, &c_tmp_s, &c_tmp_mem );
        bli_obj_scalar_apply_scalar( &BLIS_ZERO, &c_tmp_s );

        bli_obj_create_managed( dt, b_alg_max, 1, 0, 0, &ABv_s, &ABv_mem );
        bli_obj_create_managed( dt, b_alg_max, 1, 0, 0, &ABw_s, &ABw_mem );
        bli_obj_create_managed( dt, b_alg_max, 1, 0, 0, &Cw_s, &Cw_mem );
        bli_obj_create_managed( dt, b_alg_max, 1, 0, 0, &Cv_s, &Cv_mem );
        bli_obj_create_managed( dt, 1, 1, 0, 0, &abram_s, &abram_mem );
        bli_obj_create_managed( dt, 1, 1, 0, 0, &macdonald_s, &macdonald_mem );
    }
    c_tmp = thread_ibroadcast( thread, &c_tmp_s );
    ABv   = thread_ibroadcast( thread, &ABw_s );
    ABw   = thread_ibroadcast( thread, &ABw_s );
    Cv    = thread_ibroadcast( thread, &Cv_s );
    Cw    = thread_ibroadcast( thread, &Cw_s );
    abram = thread_ibroadcast( thread, &abram_s);
    macdonald = thread_ibroadcast( thread, &macdonald_s);


    //doublecheck
    /*
    obj_t Bw_copy, ABw_copy, Cw_copy;
    if( thread_am_ochief( thread ) ) {
        bli_obj_create( dt, bli_obj_length_after_trans(*b), 1, 0, 0, &Bw_copy );
        bli_setv( &BLIS_ZERO, &Bw_copy ); 

        //Doublecheck B*w checksum
        bli_gemv(&BLIS_ONE, b, w, &BLIS_ZERO, &Bw_copy);
        printf("db_len     %d\n", bli_obj_length(*Bw));
        bli_printv("db", Bw, "%5.5f", "");
        bli_printv("dbcopy", &Bw_copy, "%5.5f", "");
        bli_subv(Bw, &Bw_copy);
        bli_normfv(&Bw_copy, abram);
        bli_printm("packm gemv vs gemv", abram, "%5.2f", "");
    }
    if( thread_am_ichief( thread ) ){
        bli_obj_create( dt, b_alg_max, 1, 0, 0, &ABw_copy );
        bli_obj_create( dt, b_alg_max, 1, 0, 0, &Cw_copy );
    }
    */

	// Partition along the m dimension.
	for ( i = start; i < end; i += b_alg )
	{
       if( thread_am_ichief( thread ) ) {
           bli_setv( &BLIS_ZERO, ABv ); 
           bli_setv( &BLIS_ZERO, ABw ); 
           bli_setv( &BLIS_ZERO, Cv ); 
           bli_setv( &BLIS_ZERO, Cw ); 
       }

		// Determine the current algorithmic blocksize.
		// NOTE: Use of a (for execution datatype) is intentional!
		// This causes the right blocksize to be used if c and a are
		// complex and b is real.
		b_alg = bli_determine_blocksize_f( i, end, a,
		                                   cntl_blocksize( cntl ) );

		// Acquire partitions for A1 and C1.
		bli_acquire_mpart_t2b( BLIS_SUBPART1,
		                       i, b_alg, a, &a1 );
		bli_acquire_mpart_t2b( BLIS_SUBPART1,
  		                       i, b_alg, c, &c1 );
		bli_acquire_mpart_t2b( BLIS_SUBPART1,
  		                       0, b_alg, c_tmp, &c_tmp_1 );
		
        // Initialize objects for packing A1 and C1.
        if( thread_am_ichief( thread ) ) {
            bli_packm_init( &a1, a1_pack,
                            cntl_sub_packm_a( cntl ) );
        }
        thread_ibarrier( thread );
    
        start_t = rdtsc();
		// Pack A1 (if instructed).
		bli_packm_Av_Aw_int( &a1, a1_pack,
                       Bv, ABv,
                       Bw, ABw,
                       1,
		               cntl_sub_packm_a( cntl ),
                       gemm_thread_sub_ipackm( thread ) );
        pack_a_time += rdtsc() - start_t;


        start_t = rdtsc();
		// Perform gemm subproblem.
		bli_gemm_Av_Aw_ker_var2( 
		              a1_pack,
		              b_pack,
                      &c_tmp_1,
                      v, Cv,
                      w, Cw,
		              cntl_sub_gemm( cntl ),
                      gemm_thread_sub_gemm( thread ) );
        macrokernel_time += rdtsc() - start_t;

        //Doublecheck A * db checksum
        /*
        bli_gemv(&BLIS_ONE, b, &w, &BLIS_ZERO, &dbcopy);
        bli_gemv(&BLIS_ONE, &a1, &dbcopy, &BLIS_ZERO, &dabcopy);
        bli_printv("dabcopy", &dabcopy, "%5.5f", "");
        bli_printv("dab", &dab, "%5.5f", "");
        bli_printv("dc", &dc, "%5.5f", "");
        */ 
        //bli_subv(&dab, &dabcopy);
        //bli_normfv(&dabcopy, &abram);
       
       
        thread_ibarrier( thread );
        //Need to do some of this in parallel...
        if( thread_am_ichief( thread ) ) { 
            start_t = rdtsc();
            bli_subv(ABv, Cv);
            bli_normfv(Cv, macdonald);
            bli_subv(ABw, Cw);
            bli_normfv(Cw, abram);
            check_norm(abram); 
            norm_time += rdtsc() - start_t;
            
            //Now add Cc to C if the operation is good
            //if( abram < something ) {
            start_t = rdtsc();
                obj_t detached_beta; 
                bli_obj_scalar_detach( &c1, &detached_beta ); 
                bli_scalm( &detached_beta, &c1 );
                bli_addm( &c_tmp_1, &c1 );
            update_c_time += rdtsc() - start_t;
        }
        thread_ibarrier( thread );
	}

    //printf("pack B\tpack A\tmacrokernel\tupd C\tnorm\ttotal\n");
    //printf("%u\t%u\t%u\t%u\t%u\t%u\n", pack_b_time, pack_a_time, macrokernel_time, update_c_time, norm_time, rdtsc() - startall);
	
    // If any packing buffers were acquired within packm, release them back
	// to the memory manager.
    thread_obarrier( thread );
    if( thread_am_ochief( thread ) ){
	    bli_obj_release_pack( b_pack );
        bli_mem_release( &v_mem );
        bli_mem_release( &Bv_mem );
        bli_mem_release( &w_mem );
        bli_mem_release( &Bw_mem );
    }
    if( thread_am_ichief( thread ) ){
        bli_obj_release_pack( a1_pack );
        bli_mem_release( &ABv_mem );
        bli_mem_release( &Cv_mem );
        bli_mem_release( &macdonald_mem );
        bli_mem_release( &ABw_mem );
        bli_mem_release( &Cw_mem );
        bli_mem_release( &abram_mem );
        bli_mem_release( &c_tmp_mem );
    }
}

