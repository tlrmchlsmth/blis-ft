#define LAZY
#define MAX_NUM_BLOCKS 1000000
#define ENABLE_REPACK_B_ON_ERROR

//#define DO_ERR_PACKA
//#define DO_ERR_PACKB
//#define DO_ERR_C
extern bool_t do_err_packa;
extern bool_t do_err_packb;

#ifndef BLIS_ENABLE_OPENMP
#define omp_get_thread_num() 0
#endif

extern int macro_id[240]; //refers to which macro-kernel we are on
extern int packa_micro_id[240];
extern int num_a_err_locs;
extern int num_b_err_locs;
extern int num_c_err_locs;

extern int next_potential_A_err_index[240];
extern int next_potential_B_err_index[240];
extern int next_potential_C_err_index[240];
void bli_reset_err_locs();
extern int my_mpi_rank;
extern int error_tracker;

//Where to inject error
struct error_location_s
{
    int node_id;
    int thread_id;
    int macro_id;
    int micro_id;
};
typedef struct error_location_s error_location_t;
extern error_location_t A_err_locs[1000];
extern error_location_t B_err_locs[1000];
extern error_location_t C_err_locs[1000];

struct recompute_block_s
{
    dim_t m_coord;
    dim_t n_coord;
    dim_t k_coord;

    //All blocks are MR by KC by NR
//    dim_t m_len;
//    dim_t n_len;
//    dim_t k_len;
};
typedef struct recompute_block_s recompute_block_t;

extern recompute_block_t blocks[MAX_NUM_BLOCKS];
extern dim_t n_blocks;

void recompute_blocks( obj_t* alpha_obj, obj_t *a_obj, obj_t *b_obj, obj_t* beta_obj, obj_t *c_obj,
                       recompute_block_t *blocks, dim_t num_blocks, gemm_thrinfo_t* thread );
