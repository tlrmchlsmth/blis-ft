/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2014, The University of Texas at Austin

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas at Austin nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#define BLIS_SPACKM_GEMV_4XK_LEFT_KERNEL       bli_spackm_gemv_ref_4xk_left
#define BLIS_DPACKM_GEMV_4XK_LEFT_KERNEL       bli_dpackm_gemv_ref_4xk_left
#define BLIS_CPACKM_GEMV_4XK_LEFT_KERNEL       bli_cpackm_gemv_ref_4xk_left
#define BLIS_ZPACKM_GEMV_4XK_LEFT_KERNEL       bli_zpackm_gemv_ref_4xk_left

#define BLIS_SPACKM_GEMV_4XK_RIGHT_KERNEL       bli_spackm_gemv_ref_4xk_right
#define BLIS_DPACKM_GEMV_4XK_RIGHT_KERNEL       bli_dpackm_gemv_ref_4xk_right
#define BLIS_CPACKM_GEMV_4XK_RIGHT_KERNEL       bli_cpackm_gemv_ref_4xk_right
#define BLIS_ZPACKM_GEMV_4XK_RIGHT_KERNEL       bli_zpackm_gemv_ref_4xk_right

#define BLIS_SPACKM_GEMV_8XK_LEFT_KERNEL       bli_spackm_gemv_ref_8xk_left
#define BLIS_DPACKM_GEMV_8XK_LEFT_KERNEL       bli_dpackm_gemv_ref_8xk_left
#define BLIS_CPACKM_GEMV_8XK_LEFT_KERNEL       bli_cpackm_gemv_ref_8xk_left
#define BLIS_ZPACKM_GEMV_8XK_LEFT_KERNEL       bli_zpackm_gemv_ref_8xk_left

#define BLIS_SPACKM_GEMV_30XK_LEFT_KERNEL       bli_spackm_gemv_ref_30xk_left
#define BLIS_DPACKM_GEMV_30XK_LEFT_KERNEL       bli_dpackm_gemv_ref_30xk_left
#define BLIS_CPACKM_GEMV_30XK_LEFT_KERNEL       bli_cpackm_gemv_ref_30xk_left
#define BLIS_ZPACKM_GEMV_30XK_LEFT_KERNEL       bli_zpackm_gemv_ref_30xk_left

#define BLIS_SPACKM_GEMV_8XK_RIGHT_KERNEL       bli_spackm_gemv_ref_8xk_right
#define BLIS_DPACKM_GEMV_8XK_RIGHT_KERNEL       bli_dpackm_gemv_ref_8xk_right
#define BLIS_CPACKM_GEMV_8XK_RIGHT_KERNEL       bli_cpackm_gemv_ref_8xk_right
#define BLIS_ZPACKM_GEMV_8XK_RIGHT_KERNEL       bli_zpackm_gemv_ref_8xk_right

#include "bli_packm_gemv_int.h"

#include "bli_packm_gemv_blk_var1.h"

//#include "bli_packm_gemv_struc_cxk.h"
//#include "bli_packm_struc_cxk_rih.h"

#include "bli_packm_gemv_cxk_left.h"
#include "bli_packm_gemv_cxk_right.h"
//#include "bli_packm_cxk_rih.h"

