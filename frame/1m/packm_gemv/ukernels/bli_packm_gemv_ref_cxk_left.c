/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2014, The University of Texas at Austin

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas at Austin nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "blis.h"
#include <immintrin.h>
#include "ft_defs.h"

#undef  GENTFUNC
#define GENTFUNC( ctype, ch, varname ) \
\
void PASTEMAC(ch,varname)( \
                           conj_t  conja, \
                           dim_t   n, \
                           void*   kappa, \
                           void*   a, inc_t inca, inc_t lda, \
                           void*   p,             inc_t ldp, \
                           void*   w, \
                           void*   d_b  \
                         ) \
{ \
	ctype* restrict kappa_cast = kappa; \
	ctype* restrict alpha1     = a; \
	ctype* restrict pi1        = p; \
    ctype* restrict wcast        = w; \
    ctype* restrict dbcast       = d_b; \
\
	dim_t           n_iter     = n / 2; \
	dim_t           n_left     = n % 2; \
\
	if ( PASTEMAC(ch,eq1)( *kappa_cast ) ) \
	{ \
		if ( bli_is_conj( conja ) ) \
		{ \
            printf("packm_gemv conj area not implemented\n"); \
		} \
		else \
		{ \
			for ( ; n_iter != 0; --n_iter ) \
			{ \
				PASTEMAC2(ch,ch,copys)( *(alpha1 + 0*inca + 0*lda), *(pi1 + 0 + 0*ldp) ); \
				PASTEMAC2(ch,ch,copys)( *(alpha1 + 1*inca + 0*lda), *(pi1 + 1 + 0*ldp) ); \
				PASTEMAC2(ch,ch,copys)( *(alpha1 + 2*inca + 0*lda), *(pi1 + 2 + 0*ldp) ); \
				PASTEMAC2(ch,ch,copys)( *(alpha1 + 3*inca + 0*lda), *(pi1 + 3 + 0*ldp) ); \
\
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 0 + 0*ldp), wcast[0], dbcast[0]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 1 + 0*ldp), wcast[0], dbcast[1]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 2 + 0*ldp), wcast[0], dbcast[2]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 3 + 0*ldp), wcast[0], dbcast[3]); \
\
				PASTEMAC2(ch,ch,copys)( *(alpha1 + 0*inca + 1*lda), *(pi1 + 0 + 1*ldp) ); \
				PASTEMAC2(ch,ch,copys)( *(alpha1 + 1*inca + 1*lda), *(pi1 + 1 + 1*ldp) ); \
				PASTEMAC2(ch,ch,copys)( *(alpha1 + 2*inca + 1*lda), *(pi1 + 2 + 1*ldp) ); \
				PASTEMAC2(ch,ch,copys)( *(alpha1 + 3*inca + 1*lda), *(pi1 + 3 + 1*ldp) ); \
\
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 0 + 1*ldp), wcast[0], dbcast[0]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 1 + 1*ldp), wcast[0], dbcast[1]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 2 + 1*ldp), wcast[0], dbcast[2]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 3 + 1*ldp), wcast[0], dbcast[3]); \
\
				alpha1 += 2*lda; \
				pi1    += 2*ldp; \
                wcast      += 2;   \
			} \
\
			for ( ; n_left != 0; --n_left ) \
			{ \
				PASTEMAC2(ch,ch,copys)( *(alpha1 + 0*inca), *(pi1 + 0) ); \
				PASTEMAC2(ch,ch,copys)( *(alpha1 + 1*inca), *(pi1 + 1) ); \
				PASTEMAC2(ch,ch,copys)( *(alpha1 + 2*inca), *(pi1 + 2) ); \
				PASTEMAC2(ch,ch,copys)( *(alpha1 + 3*inca), *(pi1 + 3) ); \
\
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 0 + 0*ldp), wcast[0], dbcast[0]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 1 + 0*ldp), wcast[0], dbcast[1]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 2 + 0*ldp), wcast[0], dbcast[2]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 3 + 0*ldp), wcast[0], dbcast[3]); \
\
				alpha1 += lda; \
				pi1    += ldp; \
                wcast      += 1; \
			} \
		} \
	} \
	else \
	{ \
        printf("packm_gemv kappa != 1 not implemented\n"); \
	} \
}

GENTFUNC( float,    s, packm_gemv_ref_4xk_left )
//GENTFUNC( double,   d, packm_gemv_ref_4xk_left )
GENTFUNC( scomplex, c, packm_gemv_ref_4xk_left )
GENTFUNC( dcomplex, z, packm_gemv_ref_4xk_left )


#undef  GENTFUNC
#define GENTFUNC( ctype, ch, varname ) \
\
void PASTEMAC(ch,varname)( \
                           conj_t  conja, \
                           dim_t   n, \
                           void*   kappa, \
                           void*   a, inc_t inca, inc_t lda, \
                           void*   p,             inc_t ldp, \
                           void*   w, \
                           void*   d_b  \
                         ) \
{ \
	ctype* restrict kappa_cast = kappa; \
	ctype* restrict alpha1     = a; \
	ctype* restrict pi1        = p; \
    ctype* restrict wcast        = w; \
    ctype* restrict dbcast       = d_b; \
\
	dim_t           n_iter     = n / 2; \
	dim_t           n_left     = n % 2; \
\
	if ( PASTEMAC(ch,eq1)( *kappa_cast ) ) \
	{ \
		if ( bli_is_conj( conja ) ) \
		{ \
            printf("packm_gemv conj area not implemented\n"); \
		} \
		else \
		{ \
			for ( ; n_iter != 0; --n_iter ) \
			{ \
				PASTEMAC(ch,copys)( *(alpha1 + 0*inca + 0*lda), *(pi1 + 0 + 0*ldp) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 1*inca + 0*lda), *(pi1 + 1 + 0*ldp) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 2*inca + 0*lda), *(pi1 + 2 + 0*ldp) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 3*inca + 0*lda), *(pi1 + 3 + 0*ldp) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 4*inca + 0*lda), *(pi1 + 4 + 0*ldp) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 5*inca + 0*lda), *(pi1 + 5 + 0*ldp) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 6*inca + 0*lda), *(pi1 + 6 + 0*ldp) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 7*inca + 0*lda), *(pi1 + 7 + 0*ldp) ); \
\
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 0 + 0*ldp), wcast[0], dbcast[0]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 1 + 0*ldp), wcast[0], dbcast[1]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 2 + 0*ldp), wcast[0], dbcast[2]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 3 + 0*ldp), wcast[0], dbcast[3]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 4 + 0*ldp), wcast[0], dbcast[4]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 5 + 0*ldp), wcast[0], dbcast[5]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 6 + 0*ldp), wcast[0], dbcast[6]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 7 + 0*ldp), wcast[0], dbcast[7]); \
\
				PASTEMAC(ch,copys)( *(alpha1 + 0*inca + 1*lda), *(pi1 + 0 + 1*ldp) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 1*inca + 1*lda), *(pi1 + 1 + 1*ldp) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 2*inca + 1*lda), *(pi1 + 2 + 1*ldp) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 3*inca + 1*lda), *(pi1 + 3 + 1*ldp) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 4*inca + 1*lda), *(pi1 + 4 + 1*ldp) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 5*inca + 1*lda), *(pi1 + 5 + 1*ldp) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 6*inca + 1*lda), *(pi1 + 6 + 1*ldp) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 7*inca + 1*lda), *(pi1 + 7 + 1*ldp) ); \
\
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 0 + 1*ldp), wcast[1], dbcast[0]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 1 + 1*ldp), wcast[1], dbcast[1]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 2 + 1*ldp), wcast[1], dbcast[2]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 3 + 1*ldp), wcast[1], dbcast[3]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 4 + 1*ldp), wcast[1], dbcast[4]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 5 + 1*ldp), wcast[1], dbcast[5]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 6 + 1*ldp), wcast[1], dbcast[6]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 7 + 1*ldp), wcast[1], dbcast[7]); \
\
				alpha1 += 2*lda; \
				pi1    += 2*ldp; \
                wcast      += 2; \
			} \
\
			for ( ; n_left != 0; --n_left ) \
			{ \
				PASTEMAC(ch,copys)( *(alpha1 + 0*inca), *(pi1 + 0) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 1*inca), *(pi1 + 1) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 2*inca), *(pi1 + 2) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 3*inca), *(pi1 + 3) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 4*inca), *(pi1 + 4) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 5*inca), *(pi1 + 5) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 6*inca), *(pi1 + 6) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 7*inca), *(pi1 + 7) ); \
\
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 0 + 0*ldp), wcast[0], dbcast[0]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 1 + 0*ldp), wcast[0], dbcast[1]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 2 + 0*ldp), wcast[0], dbcast[2]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 3 + 0*ldp), wcast[0], dbcast[3]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 4 + 0*ldp), wcast[0], dbcast[4]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 5 + 0*ldp), wcast[0], dbcast[5]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 6 + 0*ldp), wcast[0], dbcast[6]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 7 + 0*ldp), wcast[0], dbcast[7]); \
\
				alpha1 += lda; \
				pi1    += ldp; \
                wcast      += 1; \
			} \
		} \
	} \
	else \
	{ \
        printf("packm_gemv kappa != 1 not implemented\n"); \
		if ( bli_is_conj( conja ) ) \
		{ \
			for ( ; n != 0; --n ) \
			{ \
				PASTEMAC(ch,scal2js)( *kappa_cast, *(alpha1 + 0*inca), *(pi1 + 0) ); \
				PASTEMAC(ch,scal2js)( *kappa_cast, *(alpha1 + 1*inca), *(pi1 + 1) ); \
				PASTEMAC(ch,scal2js)( *kappa_cast, *(alpha1 + 2*inca), *(pi1 + 2) ); \
				PASTEMAC(ch,scal2js)( *kappa_cast, *(alpha1 + 3*inca), *(pi1 + 3) ); \
				PASTEMAC(ch,scal2js)( *kappa_cast, *(alpha1 + 4*inca), *(pi1 + 4) ); \
				PASTEMAC(ch,scal2js)( *kappa_cast, *(alpha1 + 5*inca), *(pi1 + 5) ); \
				PASTEMAC(ch,scal2js)( *kappa_cast, *(alpha1 + 6*inca), *(pi1 + 6) ); \
				PASTEMAC(ch,scal2js)( *kappa_cast, *(alpha1 + 7*inca), *(pi1 + 7) ); \
\
				alpha1 += lda; \
				pi1    += ldp; \
			} \
		} \
		else \
		{ \
			for ( ; n != 0; --n ) \
			{ \
				PASTEMAC(ch,scal2s)( *kappa_cast, *(alpha1 + 0*inca), *(pi1 + 0) ); \
				PASTEMAC(ch,scal2s)( *kappa_cast, *(alpha1 + 1*inca), *(pi1 + 1) ); \
				PASTEMAC(ch,scal2s)( *kappa_cast, *(alpha1 + 2*inca), *(pi1 + 2) ); \
				PASTEMAC(ch,scal2s)( *kappa_cast, *(alpha1 + 3*inca), *(pi1 + 3) ); \
				PASTEMAC(ch,scal2s)( *kappa_cast, *(alpha1 + 4*inca), *(pi1 + 4) ); \
				PASTEMAC(ch,scal2s)( *kappa_cast, *(alpha1 + 5*inca), *(pi1 + 5) ); \
				PASTEMAC(ch,scal2s)( *kappa_cast, *(alpha1 + 6*inca), *(pi1 + 6) ); \
				PASTEMAC(ch,scal2s)( *kappa_cast, *(alpha1 + 7*inca), *(pi1 + 7) ); \
\
				alpha1 += lda; \
				pi1    += ldp; \
			} \
		} \
	} \
}

GENTFUNC( float,    s, packm_gemv_ref_8xk_left )
//GENTFUNC( double,   d, packm_gemv_ref_4xk_left )
GENTFUNC( scomplex, c, packm_gemv_ref_8xk_left )
GENTFUNC( dcomplex, z, packm_gemv_ref_8xk_left )


void bli_dpackm_gemv_ref_4xk_left(
                           conj_t  conja,
                           dim_t   n,
                           void*   kappa,
                           void*   a, inc_t inca, inc_t lda,
                           void*   p,             inc_t ldp,
                           void*   w,
                           void*   d_b 
                         ) 
{
	double* restrict kappa_cast = kappa;
	double* restrict alpha1     = a;
	double* restrict pi1        = p;
    double* restrict wcast        = w;
    double* restrict dbcast       = d_b;
 
	dim_t           n_iter     = n / 2;
	dim_t           n_left     = n % 2;
#ifndef KNC_BLIS_FT 
	if ( *kappa_cast == 1.0 )
	{
		if ( bli_is_conj( conja ) )
		{ 
            printf("packm_gemv conj area not implemented\n"); \
		}
		else 
		{ 
            __m256d dv = _mm256_load_pd( dbcast );

			for ( ; n_iter != 0; --n_iter ) \
			{ 
                __m256d wv = _mm256_broadcast_sd( wcast );
                __m256d av = _mm256_set_pd(*(alpha1 + 3*inca + 0*lda ),  
                                           *(alpha1 + 2*inca + 0*lda ),  
                                           *(alpha1 + 1*inca + 0*lda ),  
                                           *(alpha1 + 0*inca + 0*lda ));
                _mm256_store_pd( pi1 + 0 * ldp, av );
                __m256d dtmpv = _mm256_mul_pd( av, wv );
                dv = _mm256_add_pd( dv, dtmpv );

                wv = _mm256_broadcast_sd( wcast + 1 );
                av = _mm256_set_pd(*(alpha1 + 3*inca + 1*lda ),  
                                   *(alpha1 + 2*inca + 1*lda ),  
                                   *(alpha1 + 1*inca + 1*lda ),  
                                   *(alpha1 + 0*inca + 1*lda ));
                _mm256_store_pd( pi1 + 1 * ldp, av );
                dtmpv = _mm256_mul_pd( av, wv );
                dv = _mm256_add_pd( dv, dtmpv );
                
				alpha1 += 2*lda;
				pi1    += 2*ldp;
                wcast      += 2;
			} 
			for ( ; n_left != 0; --n_left )
			{ 
                __m256d wv = _mm256_broadcast_sd( wcast );
                __m256d av = _mm256_set_pd(*(alpha1 + 3*inca + 0*lda ),  
                                           *(alpha1 + 2*inca + 0*lda ),  
                                           *(alpha1 + 1*inca + 0*lda ),  
                                           *(alpha1 + 0*inca + 0*lda ));
                _mm256_store_pd( pi1 + 0 * ldp, av );
                __m256d dtmpv = _mm256_mul_pd( av, wv );
                dv = _mm256_add_pd( dv, dtmpv );

				alpha1 += lda;
				pi1    += ldp;
                wcast      += 1;
			} 
            _mm256_store_pd( dbcast, dv );
            #ifdef DO_ERR_PACKA
//            if( do_err_packa && omp_get_thread_num() == 0 ){
//                do_err_packa = 0;
//	            double* restrict pi1        = p;
//                __m256d reg = _mm256_set_pd(*(pi1) + 5.0, *(pi1+1), *(pi1+2), *(pi1+3)); 
//                _mm256_store_pd( pi1, reg );
//            }
            #endif
           int* eindex = &next_potential_A_err_index[omp_get_thread_num()]; 
           if( *eindex < num_a_err_locs ) { 
               error_location_t* loc = &A_err_locs[*eindex]; 
               if( loc->macro_id == macro_id[omp_get_thread_num()] && packa_micro_id[omp_get_thread_num()] == loc->micro_id) {
                    if(omp_get_thread_num() == loc->thread_id){ 
                        /*printf("injecting error at %d\t%d\n", macro_id[omp_get_thread_num()], j); */
                        double* restrict pi1        = p;
                        __m256d reg = _mm256_set_pd(*(pi1) + 5.0, *(pi1+1), *(pi1+2), *(pi1+3)); 
                        _mm256_store_pd( pi1, reg );
                    } 
                    *eindex++; 
               }
            } 
        }
	} 
	else 
	{ 
        printf("packm_gemv kappa != 1 not implemented\n"); 
	} 
    packa_micro_id[omp_get_thread_num()]++;
#endif
}

void bli_dpackm_gemv_ref_8xk_left(
                           conj_t  conja,
                           dim_t   n,
                           void*   kappa,
                           void*   a, inc_t inca, inc_t lda,
                           void*   p,             inc_t ldp,
                           void*   w,
                           void*   d_b 
                         ) 
{
	double* restrict kappa_cast = kappa;
	double* restrict alpha1     = a;
	double* restrict pi1        = p;
    double* restrict wcast        = w;
    double* restrict dbcast       = d_b;
 
	dim_t           n_iter     = n / 2;
	dim_t           n_left     = n % 2;
 
#ifndef KNC_BLIS_FT 
	if ( *kappa_cast == 1.0 )
	{
		if ( bli_is_conj( conja ) )
		{ 
            printf("packm_gemv conj area not implemented\n"); \
		}
		else 
		{ 

            __m256d dv1 = _mm256_load_pd( dbcast );
            __m256d dv2 = _mm256_load_pd( dbcast + 4 );

			for ( ; n_iter != 0; --n_iter ) \
			{ 
                __m256d wv = _mm256_broadcast_sd( wcast );
                __m256d av2 = _mm256_set_pd(*(alpha1 + 7*inca + 0*lda ),  
                                            *(alpha1 + 6*inca + 0*lda ),  
                                            *(alpha1 + 5*inca + 0*lda ),  
                                            *(alpha1 + 4*inca + 0*lda ));
                __m256d av1 = _mm256_set_pd(*(alpha1 + 3*inca + 0*lda ),  
                                            *(alpha1 + 2*inca + 0*lda ),  
                                            *(alpha1 + 1*inca + 0*lda ),  
                                            *(alpha1 + 0*inca + 0*lda ));
                _mm256_store_pd( pi1 + 0 * ldp, av1 );
                _mm256_store_pd( pi1 + 0 * ldp + 4, av2 );
                __m256d dtmpv1 = _mm256_mul_pd( av1, wv );
                __m256d dtmpv2 = _mm256_mul_pd( av2, wv );
                dv1 = _mm256_add_pd( dv1, dtmpv1 );
                dv2 = _mm256_add_pd( dv2, dtmpv2 );


                wv = _mm256_broadcast_sd( wcast + 1 );
                av2 = _mm256_set_pd(*(alpha1 + 7*inca + 1*lda ),  
                                    *(alpha1 + 6*inca + 1*lda ),  
                                    *(alpha1 + 5*inca + 1*lda ),  
                                    *(alpha1 + 4*inca + 1*lda ));
                av1 = _mm256_set_pd(*(alpha1 + 3*inca + 1*lda ),  
                                    *(alpha1 + 2*inca + 1*lda ),  
                                    *(alpha1 + 1*inca + 1*lda ),  
                                    *(alpha1 + 0*inca + 1*lda ));
                _mm256_store_pd( pi1 + 1 * ldp, av1 );
                _mm256_store_pd( pi1 + 1 * ldp + 4, av2 );
                dtmpv1 = _mm256_mul_pd( av1, wv );
                dtmpv2 = _mm256_mul_pd( av2, wv );
                dv1 = _mm256_add_pd( dv1, dtmpv1 );
                dv2 = _mm256_add_pd( dv2, dtmpv2 );
                
				alpha1 += 2*lda;
				pi1    += 2*ldp;
                wcast      += 2;
			} 
			for ( ; n_left != 0; --n_left )
			{ 
                __m256d wv = _mm256_broadcast_sd( wcast );
                __m256d av2 = _mm256_set_pd(*(alpha1 + 7*inca + 0*lda ),  
                                    *(alpha1 + 6*inca + 0*lda ),  
                                    *(alpha1 + 5*inca + 0*lda ),  
                                    *(alpha1 + 4*inca + 0*lda ));
                __m256d av1 = _mm256_set_pd(*(alpha1 + 3*inca + 0*lda ),  
                                    *(alpha1 + 2*inca + 0*lda ),  
                                    *(alpha1 + 1*inca + 0*lda ),  
                                    *(alpha1 + 0*inca + 0*lda ));
                _mm256_store_pd( pi1 + 0 * ldp, av1 );
                _mm256_store_pd( pi1 + 0 * ldp + 4, av2 );
                __m256d dtmpv1 = _mm256_mul_pd( av1, wv );
                __m256d dtmpv2 = _mm256_mul_pd( av2, wv );
                dv1 = _mm256_add_pd( dv1, dtmpv1 );
                dv2 = _mm256_add_pd( dv2, dtmpv2 );

				alpha1 += lda;
				pi1    += ldp;
                wcast      += 1;
			}
            _mm256_store_pd( dbcast, dv1 );
            _mm256_store_pd( dbcast+4, dv2 );

           /* #ifdef DO_ERR_PACKA
            if( do_err_packa && omp_get_thread_num() == 0 ){
                do_err_packa = 0;
	            double* restrict p_cast        = p;
                __m256d reg = _mm256_set_pd(p_cast[3], p_cast[2], p_cast[1], p_cast[0] + 5.0 );
                _mm256_store_pd( p_cast, reg );
            }
            #endif*/
           int thread_id = omp_get_thread_num();
           int* eindex = &next_potential_A_err_index[thread_id]; 
           error_location_t* loc = &A_err_locs[*eindex]; 
           while( *eindex < num_a_err_locs && loc->macro_id == macro_id[thread_id] && loc->micro_id == packa_micro_id[thread_id]) {
                //printf("%d\t%d\n", loc->macro_id, macro_id[thread_id]);
                if(thread_id == loc->thread_id ){ 
                    //printf("injecting error at %d\t%d\t%d\n", omp_get_thread_num(), macro_id[omp_get_thread_num()],
                    //packa_micro_id[omp_get_thread_num()]);
                    double* restrict pi1        = p;
                    __m256d reg = _mm256_set_pd(*(pi1) + 5.0, *(pi1+1), *(pi1+2), *(pi1+3)); 
                    _mm256_store_pd( pi1, reg );
                    _Pragma("omp atomic")
                        error_tracker++;

                } 
                (*eindex)++; 
                loc = &A_err_locs[*eindex]; 
           }
        }
	} 
	else 
	{ 
        printf("packm_gemv kappa != 1 not implemented\n"); 
	}
    packa_micro_id[omp_get_thread_num()]++;
    #endif
}

void printVecReg( __m512d reg )
{
    double boyz[8];
    _mm512_store_pd( boyz,  reg );
    for(int i = 0; i < 8; i++)
        printf("%f\t", boyz[i]);
    printf("\n");
}
void printVecRegi( __m512i reg )
{
    int boyz[16];
    _mm512_store_epi32( boyz,  reg );
    for(int i = 0; i < 16; i++)
        printf("%d\t", boyz[i]);
    printf("\n");
}

#define CHECKSUM
void bli_dpackm_gemv_ref_30xk_left(
                           conj_t  conja,
                           dim_t   n,
                           void*   kappa,
                           volatile void*   a, inc_t inca, inc_t lda,
                           void*   p,             inc_t ldp,
                           void*   w_in,
                           void*   Aw_in 
                         ) 
{
    volatile double* a_cast = a;
    double* p_cast = p;
    double* w_cast = w_in;

	double* restrict kappa_cast = kappa;
	volatile double* restrict alpha1     = a;
	double* restrict pi1;
    double* restrict w;
    double* restrict Aw        = Aw_in;
 
	dim_t           n_iter     = n ;
	dim_t           n_left     = 0 ;
    __declspec(aligned(64)) int balls[16] = {0, 1*inca, 2*inca, 3*inca, 4*inca, 5*inca, 6*inca, 7*inca,
                                             0, 1*inca, 2*inca, 3*inca, 4*inca, 5*inca, 6*inca, 7*inca };
    __m512i gthrindex = _mm512_load_epi32( balls );
	
    if ( *kappa_cast == 1.0 )
	{
		if ( bli_is_conj( conja ) )
            printf("packm_gemv conj area not implemented\n");
		else 
		{
#ifdef CHECKSUM
            __m512d a0to7w   = _mm512_load_pd( Aw );
            __m512d a8to15w  = _mm512_load_pd( Aw + 8 );
            __m512d a16to23w = _mm512_load_pd( Aw + 16 );
            __m512d a24to29w = _mm512_load_pd( Aw + 24 );
#endif
            __mmask16 mask = _mm512_int2mask( 0x3F );
            
            //Each iteration loads 30 elements of A, multiplies with 1 element of w, and
            //consequently updates 30 elements of Aw
			for ( int i = 0; i < n; i+=8 )
			{
                for( int ii = 0; ii < 8; ii++ )
                {
                    if( i + ii >= n ) break;
                    
				alpha1 = &a_cast[(ii + i)*lda];
				pi1    = &p_cast[(ii + i)*ldp];
                w      = &w_cast[ii + i];

                //printf("counter %d, inca %d, lda %d\n", counter, inca, lda); fflush(0); 
                //Load elements of A
                __m512d a0to7   = _mm512_i32logather_pd ( gthrindex, alpha1 +  0*inca, 8 );
                __m512d a8to15  = _mm512_i32logather_pd ( gthrindex, alpha1 +  8*inca, 8 );
                __m512d a16to23 = _mm512_i32logather_pd ( gthrindex, alpha1 + 16*inca, 8 );
                //__m512d a24to29 = _mm512_i32logather_pd ( gthrindex, alpha1 + 24*inca, 8 );
                __m512d a24to29 = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
                a24to29 = _mm512_mask_i32logather_pd ( a24to29, mask, gthrindex, alpha1 + 24*inca, 8 );


                
                //Store elements to A_pack
                _mm512_store_pd( pi1,  a0to7 );
                _mm512_store_pd( pi1 + 8,  a8to15 );
                _mm512_store_pd( pi1 + 16, a16to23 );
                //_mm512_store_pd( pi1 + 24, a24to29 );
                _mm512_mask_store_pd( pi1 + 24, mask, a24to29 );
               
#ifdef CHECKSUM
                a0to7   = _mm512_i32logather_pd ( gthrindex, alpha1 +  0*inca, 8 );
                a8to15  = _mm512_i32logather_pd ( gthrindex, alpha1 +  8*inca, 8 );
                a16to23 = _mm512_i32logather_pd ( gthrindex, alpha1 + 16*inca, 8 );
                a24to29 = _mm512_mask_i32logather_pd ( a24to29, mask, gthrindex, alpha1 + 24*inca, 8 );

                //Multiply all elements of A with same element of w
                __m512d wbcast= _mm512_extload_pd ( w, _MM_UPCONV_PD_NONE, _MM_BROADCAST_1X8, 0);
                a0to7w   = _mm512_fmadd_pd( a0to7, wbcast, a0to7w );
                a8to15w  = _mm512_fmadd_pd( a8to15, wbcast, a8to15w );
                a16to23w = _mm512_fmadd_pd( a16to23, wbcast, a16to23w );
                a24to29w = _mm512_fmadd_pd( a24to29, wbcast, a24to29w );
#endif                
                }
			} 
#ifdef CHECKSUM
            _mm512_storenr_pd( Aw, a0to7w );
            _mm512_storenr_pd( Aw + 8,  a8to15w );
            _mm512_storenr_pd( Aw + 16, a16to23w );
            _mm512_storenr_pd( Aw + 24, a24to29w );
#endif            
            #ifdef DO_ERR_PACKA
//            if( do_err_packa && omp_get_thread_num() == 0 ){
//                do_err_packa = 0;
//	            double* restrict pi1        = p;
//                __m256d reg = _mm256_set_pd(*(pi1) + 5.0, *(pi1+1), *(pi1+2), *(pi1+3)); 
//                _mm256_store_pd( pi1, reg );
//            }
            #endif
/*           int* eindex = &next_potential_A_err_index[omp_get_thread_num()]; 
           if( *eindex < num_a_err_locs ) { 
               error_location_t* loc = &A_err_locs[*eindex]; 
               if( loc->macro_id == macro_id[omp_get_thread_num()] && packa_micro_id[omp_get_thread_num()] == loc->micro_id) {
                    if(omp_get_thread_num() == loc->thread_id){ 
            //            double* restrict pi1        = p;
             //           __m256d reg = _mm256_set_pd(*(pi1) + 5.0, *(pi1+1), *(pi1+2), *(pi1+3)); 
              //          _mm256_store_pd( pi1, reg );
                    } 
             //       *eindex++; 
               }
            }*/ 
        }
	} 
	else 
	{ 
        printf("packm_gemv kappa != 1 not implemented\n"); 
	} 
//    packa_micro_id[omp_get_thread_num()]++;
}

GENTFUNC( float,    s, packm_gemv_ref_30xk_left )
GENTFUNC( scomplex, c, packm_gemv_ref_30xk_left )
GENTFUNC( dcomplex, z, packm_gemv_ref_30xk_left )
