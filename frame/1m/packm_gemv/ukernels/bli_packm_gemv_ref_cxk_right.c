/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2014, The University of Texas at Austin

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas at Austin nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "blis.h"
#include <immintrin.h>
#include "ft_defs.h"

#undef  GENTFUNC
#define GENTFUNC( ctype, ch, varname ) \
\
void PASTEMAC(ch,varname)( \
                           conj_t  conja, \
                           dim_t   n, \
                           void*   kappa, \
                           void*   a, inc_t inca, inc_t lda, \
                           void*   p,             inc_t ldp, \
                           void*   w,  \
                           void*   d_b, \
                           dim_t   t_id, dim_t num_threads \
                         ) \
{ \
    ctype* restrict kappa_cast = kappa; \
    ctype* restrict wcast        = w; \
    ctype* restrict acast = a; \
    ctype* restrict pcast = p; \
    ctype* restrict dcast = d_b; \
\
	if ( PASTEMAC(ch,eq1)( *kappa_cast ) ) \
	{ \
		if ( bli_is_conj( conja ) ) \
		{ \
            printf("NOT SUPPORTED\n"); \
		} \
		else \
		{ \
			for ( int index = t_id; index < n; index += num_threads ) \
			{ \
\
				ctype* alpha1 = &acast[index * lda]; \
				ctype* pi1    = &pcast[index * ldp]; \
                ctype* d      = &dcast[index]; \
\
				PASTEMAC2(ch,ch,copys)( *(alpha1 + 0*inca), *(pi1 + 0) ); \
				PASTEMAC2(ch,ch,copys)( *(alpha1 + 1*inca), *(pi1 + 1) ); \
				PASTEMAC2(ch,ch,copys)( *(alpha1 + 2*inca), *(pi1 + 2) ); \
				PASTEMAC2(ch,ch,copys)( *(alpha1 + 3*inca), *(pi1 + 3) ); \
\
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 0 + 0*ldp), wcast[0], d[0]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 1 + 0*ldp), wcast[1], d[0]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 2 + 0*ldp), wcast[2], d[0]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 3 + 0*ldp), wcast[3], d[0]); \
			} \
		} \
	} \
	else \
	{ \
            printf("NOT SUPPORTED\n"); \
	} \
}

GENTFUNC( float,    s, packm_gemv_ref_4xk_right )
GENTFUNC( scomplex, c, packm_gemv_ref_4xk_right )
GENTFUNC( dcomplex, z, packm_gemv_ref_4xk_right )


#undef  GENTFUNC
#define GENTFUNC( ctype, ch, varname ) \
\
void PASTEMAC(ch,varname)( \
                           conj_t  conja, \
                           dim_t   n, \
                           void*   kappa, \
                           void*   a, inc_t inca, inc_t lda, \
                           void*   p,             inc_t ldp, \
                           void*   w,  \
                           void*   d_b, \
                           dim_t   t_id, dim_t num_threads \
                         ) \
{ \
    ctype* restrict kappa_cast = kappa; \
    ctype* restrict wcast        = w; \
    ctype* restrict acast = a; \
    ctype* restrict pcast = p; \
    ctype* restrict dcast = d_b; \
\
	if ( PASTEMAC(ch,eq1)( *kappa_cast ) ) \
	{ \
		if ( bli_is_conj( conja ) ) \
		{ \
            printf("NOT SUPPORTED\n"); \
		} \
		else \
		{ \
			for ( int index = t_id; index < n; index += num_threads ) \
			{ \
				ctype* alpha1 = &acast[index * lda]; \
				ctype* pi1    = &pcast[index * ldp]; \
                ctype* d      = &dcast[index]; \
\
				PASTEMAC(ch,copys)( *(alpha1 + 0*inca), *(pi1 + 0) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 1*inca), *(pi1 + 1) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 2*inca), *(pi1 + 2) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 3*inca), *(pi1 + 3) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 4*inca), *(pi1 + 4) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 5*inca), *(pi1 + 5) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 6*inca), *(pi1 + 6) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 7*inca), *(pi1 + 7) ); \
\
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 0 + 0*ldp), wcast[0], d[0]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 1 + 0*ldp), wcast[1], d[0]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 2 + 0*ldp), wcast[2], d[0]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 3 + 0*ldp), wcast[3], d[0]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 4 + 0*ldp), wcast[4], d[0]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 5 + 0*ldp), wcast[5], d[0]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 6 + 0*ldp), wcast[6], d[0]); \
                PASTEMAC3(ch,ch,ch,axpys)(*(pi1 + 7 + 0*ldp), wcast[7], d[0]); \
			} \
		} \
	} \
	else \
	{ \
        printf("NOT SUPPORTED\n"); \
	} \
}

GENTFUNC( float,    s, packm_gemv_ref_8xk_right )
GENTFUNC( scomplex, c, packm_gemv_ref_8xk_right )
GENTFUNC( dcomplex, z, packm_gemv_ref_8xk_right )

void bli_dpackm_gemv_ref_4xk_right(
                           conj_t  conja,
                           dim_t   n,  
                           void*   kappa,
                           void*   a, inc_t inca, inc_t lda,
                           void*   p,             inc_t ldp,
                           void*   w,  
                           void*   d_b,
                           dim_t   t_id, dim_t num_threads
                         )   
{
    double* restrict kappa_cast = kappa;
    double* restrict wcast        = w;
    double* restrict acast = a;
    double* restrict pcast = p;
    double* restrict dcast = d_b;
 
#ifndef KNC_BLIS_FT 
    if ( *kappa_cast == 1.0 )
    {   
        if ( bli_is_conj( conja ) ) 
        {   
            printf("packm_gemv conj area not implemented\n"); \
        }   
        else 
        {   
            __m256d wv = _mm256_load_pd( wcast );
            for ( int index = t_id; index < n; index += num_threads )
            {
				double* alpha1 = &acast[index * lda];
				double* pi1    = &pcast[index * ldp];
                double* d      = &dcast[index];

                __m256d dv = _mm256_set_pd( d[0], 0.0, 0.0, 0.0 );
                __m256d av = _mm256_set_pd(alpha1[3*inca + 0*lda],  
                                           alpha1[2*inca + 0*lda],  
                                           alpha1[1*inca + 0*lda],  
                                           alpha1[0*inca + 0*lda]); 
                _mm256_store_pd( pi1 + 0 * ldp, av );
                __m256d dtmpv = _mm256_mul_pd( av, wv );
                dv = _mm256_add_pd( dv, dtmpv );
                __m256d temp = _mm256_hadd_pd( dv, dv );
                __m128d hi128 = _mm256_extractf128_pd( temp, 1 );
                __m128d tmplo128 = _mm256_extractf128_pd( temp, 0 );
                __m128d dotproduct = _mm_add_pd( tmplo128, hi128 );
                _mm_storel_pd( d, dotproduct );
            }
            #ifdef DO_ERR_PACKB
            if( do_err_packb && omp_get_thread_num() == 0 ){
                do_err_packb = 0;
                double* restrict p_cast        = p;
                __m256d reg = _mm256_set_pd(p_cast[3], p_cast[2], p_cast[1], p_cast[0] + 5.0 );
                _mm256_store_pd( p_cast, reg );
            }   
            #endif
        }
    }
    else
    {
        printf("packm_gemv kappa != 1 not implemented\n");
    }
#endif
}

#define CHECKSUM
void bli_dpackm_gemv_ref_8xk_right(
                           conj_t  conja,
                           dim_t   n,
                           void*   kappa,
                           volatile void*   a, inc_t inca, inc_t lda,
                           void*   p,             inc_t ldp,
                           void*   win,
                           void*   wAin,
                           dim_t   t_id, dim_t num_threads
                         )
{
    double* restrict kappa_cast = kappa;
    double* restrict wptr        = win;
    double* restrict wAptr = wAin;
    volatile double* restrict acast = a;
    double* restrict pcast = p;

    if ( *kappa_cast == 1.0 )
    {
        if ( bli_is_conj( conja ) )
        {
            printf("packm_gemv conj area not implemented\n"); \
        }
        else
        {
            int balls[16] = {0, 1*inca, 2*inca, 3*inca, 4*inca, 5*inca, 6*inca, 7*inca,
                    0, 1*inca, 2*inca, 3*inca, 4*inca, 5*inca, 6*inca, 7*inca };
            __m512i gthrindex = _mm512_load_epi32( balls );

#ifdef CHECKSUM                 
            __m512d w   = _mm512_load_pd( wptr );
#endif
            for ( int index = t_id; index < n; index += num_threads )
            {
				double* alpha1 = &acast[index * lda];
				double* pi1    = &pcast[index * ldp];
                double* d      = &wAptr[index];
                
                 __m512d a0to7  = _mm512_i32logather_pd ( gthrindex, alpha1, 8 );
                 _mm512_store_pd( pi1,  a0to7 );
                 
//                 __m512d dv = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
//                 __mmask16 masko = _mm512_int2mask( 0x01 );
//                 dv = _mm512_mask_load_pd( dv, masko, d  );
#ifdef CHECKSUM                 
                 a0to7  = _mm512_i32logather_pd ( gthrindex, alpha1, 8 );
                 __m512d dv = _mm512_mul_pd( a0to7, w );
                 double dotprod = _mm512_reduce_add_pd( dv ); 

                 d[0] += dotprod;
#endif
            }
        }
        #ifdef DO_ERR_PACKB
        if( do_err_packb && num_times == 1 && omp_get_thread_num() == 0 ){
            do_err_packb = 0;
            double* restrict pi1        = p;
            //__m256d reg = _mm256_set_pd(*(pi1) + 5.0, *(pi1+1), *(pi1+2), *(pi1+3)); 
            //_mm256_store_pd( pi1, reg );
        }
        #endif
    }
    else
    { \
        printf("packm_gemv kappa != 1 not implemented\n"); \
    }
}
