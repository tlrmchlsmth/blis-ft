/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2014, The University of Texas at Austin

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas at Austin nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "blis.h"

#define FUNCPTR_T packm_gemv_fp

typedef void (*FUNCPTR_T)(
                           struc_t strucc,
                           doff_t  diagoffc,
                           diag_t  diagc,
                           uplo_t  uploc,
                           trans_t transc,
                           pack_t  schema,
                           bool_t  invdiag,
                           bool_t  revifup,
                           bool_t  reviflo,
                           dim_t   m,
                           dim_t   n,
                           dim_t   m_max,
                           dim_t   n_max,
                           void*   kappa,
                           void*   c, inc_t rs_c, inc_t cs_c,
                           void*   p, inc_t rs_p, inc_t cs_p,
                                      dim_t pd_p, inc_t ps_p,
                           void*   w,
                           void*   d_b,
                           bool_t  leftMul,
                           packm_thrinfo_t* thread
                         );

static FUNCPTR_T GENARRAY(ftypes, packm_gemv_blk_var1);

void bli_packm_gemv_blk_var1( obj_t*   c,
                         obj_t*   p,
                         obj_t*   w,
                         obj_t*   d_b,
                         bool_t  leftMul,
                         packm_thrinfo_t* t )
{
	num_t     dt_cp      = bli_obj_datatype( *c );

	struc_t   strucc     = bli_obj_struc( *c );
	doff_t    diagoffc   = bli_obj_diag_offset( *c );
	diag_t    diagc      = bli_obj_diag( *c );
	uplo_t    uploc      = bli_obj_uplo( *c );
	trans_t   transc     = bli_obj_conjtrans_status( *c );
	pack_t    schema     = bli_obj_pack_schema( *p );
	bool_t    invdiag    = bli_obj_has_inverted_diag( *p );
	bool_t    revifup    = bli_obj_is_pack_rev_if_upper( *p );
	bool_t    reviflo    = bli_obj_is_pack_rev_if_lower( *p );

	dim_t     m_p        = bli_obj_length( *p );
	dim_t     n_p        = bli_obj_width( *p );
	dim_t     m_max_p    = bli_obj_padded_length( *p );
	dim_t     n_max_p    = bli_obj_padded_width( *p );

	void*     buf_c      = bli_obj_buffer_at_off( *c );
	inc_t     rs_c       = bli_obj_row_stride( *c );
	inc_t     cs_c       = bli_obj_col_stride( *c );

	void*     buf_p      = bli_obj_buffer_at_off( *p );
	inc_t     rs_p       = bli_obj_row_stride( *p );
	inc_t     cs_p       = bli_obj_col_stride( *p );
	dim_t     pd_p       = bli_obj_panel_dim( *p );
	inc_t     ps_p       = bli_obj_panel_stride( *p );

    void*     buf_w      = bli_obj_buffer_at_off( *w );
    void*     buf_d_b      = bli_obj_buffer_at_off( *d_b );

	void*     buf_kappa;

	FUNCPTR_T f;

	// This variant assumes that the micro-kernel will always apply the
	// alpha scalar of the higher-level operation. Thus, we use BLIS_ONE
	// for kappa so that the underlying packm implementation does not
	// scale during packing.
	buf_kappa = bli_obj_buffer_for_const( dt_cp, BLIS_ONE );

	// Index into the type combination array to extract the correct
	// function pointer.
	f = ftypes[dt_cp];

	// Invoke the function.
	f( strucc,
	   diagoffc,
	   diagc,
	   uploc,
	   transc,
	   schema,
	   invdiag,
	   revifup,
	   reviflo,
	   m_p,
	   n_p,
	   m_max_p,
	   n_max_p,
	   buf_kappa,
	   buf_c, rs_c, cs_c,
	   buf_p, rs_p, cs_p,
	          pd_p, ps_p,
       buf_w,
       buf_d_b,
       leftMul,
	   t );
}


#undef  GENTFUNC
#define GENTFUNC( ctype, ch, varname, kertype ) \
\
void PASTEMAC(ch,varname)( \
                           struc_t strucc, \
                           doff_t  diagoffc, \
                           diag_t  diagc, \
                           uplo_t  uploc, \
                           trans_t transc, \
                           pack_t  schema, \
                           bool_t  invdiag, \
                           bool_t  revifup, \
                           bool_t  reviflo, \
                           dim_t   m, \
                           dim_t   n, \
                           dim_t   m_max, \
                           dim_t   n_max, \
                           void*   kappa, \
                           void*   c, inc_t rs_c, inc_t cs_c, \
                           void*   p, inc_t rs_p, inc_t cs_p, \
                                      dim_t pd_p, inc_t ps_p, \
                           void*   w,   \
                           void*   d_b, \
                           bool_t leftMul, \
                           packm_thrinfo_t* thread \
                         ) \
{ \
	ctype* restrict kappa_cast = kappa; \
	ctype* restrict c_cast     = c; \
	ctype* restrict p_cast     = p; \
    ctype* restrict dbcast     = d_b; \
    ctype* restrict wcast      = w; \
\
	dim_t           iter_dim; \
	dim_t           panel_len; \
	dim_t           panel_dim; \
	inc_t           vs_c; \
	inc_t           ldc; \
	conj_t          conjc; \
	bool_t          row_stored; \
	bool_t          col_stored; \
\
	ctype* restrict c_use; \
	ctype* restrict p_use; \
	doff_t          diagoffp_i; \
\
	/* Extract the conjugation bit from the transposition argument. */ \
	conjc = bli_extract_conj( transc ); \
\
	/* If c needs a transposition, induce it so that we can more simply
	   express the remaining parameters and code. */ \
	if ( bli_does_trans( transc ) ) \
	{ \
		bli_swap_incs( rs_c, cs_c ); \
		bli_negate_diag_offset( diagoffc ); \
		bli_toggle_uplo( uploc ); \
		bli_toggle_trans( transc ); \
	} \
\
	/* Create flags to incidate row or column storage. Note that the
	   schema bit that encodes row or column is describing the form of
	   micro-panel, not the storage in the micro-panel. Hence the
	   mismatch in "row" and "column" semantics. */ \
	row_stored = bli_is_col_packed( schema ); \
	col_stored = bli_is_row_packed( schema ); \
\
	/* If the row storage flag indicates row storage, then we are packing
	   to column panels; otherwise, if the strides indicate column storage,
	   we are packing to row panels. */ \
    int panel_stride; \
	if ( row_stored ) \
	{ \
		/* Prepare to pack to row-stored column panels. */ \
		iter_dim       = n; \
		panel_len  = m; \
		panel_dim  = pd_p; \
		panel_stride  = rs_p; \
		ldc            = rs_c; \
		vs_c           = cs_c; \
	} \
	else /* if ( col_stored ) */ \
	{ \
		/* Prepare to pack to column-stored row panels. */ \
		iter_dim       = m; \
		panel_len = n; \
		panel_dim  = pd_p; \
		panel_stride  = cs_p; \
		ldc            = cs_c; \
		vs_c           = rs_c; \
	} \
\
	/* Compute the total number of iterations we'll need. */ \
	dim_t num_iter = iter_dim / panel_dim + ( iter_dim % panel_dim ? 1 : 0 ); \
\
    dim_t start_index = 0; \
    dim_t index_inc   = 1; \
    if( leftMul ){ \
        start_index = thread_id( thread ); \
        index_inc   = thread_num_threads( thread ); \
    } \
\
    int i; \
	for ( i = start_index; i < num_iter; i += index_inc ) \
	{ \
		ctype* restrict c_begin     = c_cast   + i * panel_dim * vs_c;\
        ctype* restrict p_begin     = p_cast   + i * ps_p; \
        ctype* restrict w_begin; \
        ctype* restrict db_begin; \
\
        if ( leftMul ) {\
            w_begin     = wcast; \
            db_begin    = dbcast + i * panel_stride; \
\
            PASTEMAC(ch,packm_gemv_cxk_left)( \
                                  conjc, \
                                  panel_dim, \
                                  panel_len, \
                                  kappa_cast, \
                                  c_begin, vs_c, ldc, \
                                  p_begin, panel_stride, \
                                  w_begin, \
                                  db_begin ); \
        } \
        else {\
            w_begin     = wcast + i * panel_dim; \
            db_begin    = dbcast; \
\
            PASTEMAC(ch,packm_gemv_cxk_right)( \
                                 conjc, \
                                 panel_dim, \
                                 panel_len, \
                                 kappa_cast, \
                                 c_begin, vs_c, ldc, \
                                 p_begin, panel_stride, \
                                 w_begin, \
                                 db_begin,\
                                 thread ); \
        } \
	} \
}
INSERT_GENTFUNC_BASIC( packm_gemv_blk_var1, packm_ker_t )
