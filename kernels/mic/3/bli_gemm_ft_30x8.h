void bli_dgemm_ft_30x8(
                    dim_t            k,
                    double* restrict alpha,
                    double* restrict a,
                    double* restrict b,
                    double* restrict beta,
                    double* restrict c, inc_t rs_c, inc_t cs_c,
                    double* restrict chkpt, inc_t rs_chkpt, inc_t cs_chkpt,
                    double* restrict v, double* restrict   Cv, 
                    double* restrict w, double* restrict   wC, 
                    auxinfo_t*       data
                  );

