#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <assert.h>
#include <values.h>
#include <mpi.h>
#include <string.h>
#include <math.h>
#include <vector>
#include <omp.h>
#include <unistd.h>
#include <mkl.h>
#include <mkl_trans.h>
#include <sys/time.h>
#include <blis.h>
#include <ft_defs.h>

#define INJECT
//#define INJECT_C_WORST
#define INJECT_C_BEST
//#define INJECT_A_WORST
//#define INJECT_A_BEST

#ifdef INJECT
void bli_reset_err_locs()
{
    for(int i = 0; i < 16; i++){
        macro_id[i] = 0;
        next_potential_A_err_index[i] = 0;
        next_potential_B_err_index[i] = 0;
        next_potential_C_err_index[i] = 0;
    }   
    num_a_err_locs = 0;
    num_b_err_locs = 0;
    num_c_err_locs = 0;
    error_tracker = 0;
}
#endif

#ifdef INJECT_C_WORST
void setWorstCaseC( int numProcs, int numErrs, int myrank )
{
    if(myrank != 0)
        return;

    int maxErrsPerThread = 336 * (int)sqrt(numProcs);
    int threadsToErr = floor((double)numErrs / (double)maxErrsPerThread);
    int leftover = numErrs - threadsToErr * maxErrsPerThread;
    int nleftovers = 0;
    for( int i = 0; i < maxErrsPerThread; i++) {
        for( int j = 0; j < threadsToErr; j++){
            //printf("inject err at %d\t%d\n", j, i);
            C_err_locs[num_c_err_locs].node_id  = 0;
            C_err_locs[num_c_err_locs].thread_id = j;
            C_err_locs[num_c_err_locs].macro_id = i;
            C_err_locs[num_c_err_locs].micro_id = 0;
            num_c_err_locs++;
            if(num_c_err_locs >= numErrs)
                return;
        }
        if(nleftovers < leftover){
            //printf("inject err at %d\t%d\n", threadsToErr, i);
            C_err_locs[num_c_err_locs].node_id  = 0;
            C_err_locs[num_c_err_locs].thread_id = threadsToErr;
            C_err_locs[num_c_err_locs].macro_id = i;
            C_err_locs[num_c_err_locs].micro_id = 0;
            num_c_err_locs++;
            nleftovers ++;
            if(num_c_err_locs >= numErrs)
                return;
        }
    }
}
/*void setWorstCaseC( int numProcs, int num_ranks )
{
    for( int i = 0; i < numProcs; i++)
    {
        C_err_locs[i].node_id  = 0;
        C_err_locs[i].thread_id = 0;
        C_err_locs[i].macro_id = 0;
        C_err_locs[i].micro_id = i;
    }   
    num_c_err_locs = numProcs;
}*/
#endif
#ifdef INJECT_C_BEST
void setBestCaseC( int numProcs, int numErrs, int myrank )
{
    //local
    num_c_err_locs = 0;
    int glob_num_c_err_locs = 0;
    int errsPerProc = ceil((double) numErrs / (double) numProcs );
    int errsPerThread = ceil((double) errsPerProc / 16.0);
    for( int k = 0; k < errsPerThread; k++){
        for( int j = 0; j < 16; j++){
            for( int i = 0; i < numProcs; i++){
                if( i == myrank ){
                    //printf("insert error at %d\t%d\t%d\n", i,j,k); 
                    C_err_locs[num_c_err_locs].node_id  = i;
                    C_err_locs[num_c_err_locs].thread_id = j;
                    C_err_locs[num_c_err_locs].macro_id = k;
                    C_err_locs[num_c_err_locs].micro_id = 0;
                    num_c_err_locs++;
                }
                glob_num_c_err_locs++;
                if(glob_num_c_err_locs >= numErrs)
                    return;
            }
        }
    }
}
#endif
#ifdef INJECT_A_WORST
void setWorstCaseA( int numProcs, int numErrs, int myrank )
{   
    if(myrank != 0)
        return;

    int maxErrsPerThread = 336 * (int)sqrt(numProcs);
    int threadsToErr = floor((double)numErrs / (double)maxErrsPerThread);
    int leftover = numErrs - threadsToErr * maxErrsPerThread;
    int nleftovers = 0;
    for( int i = 0; i < maxErrsPerThread; i++) {
        for( int j = 0; j < threadsToErr; j++){
            A_err_locs[num_a_err_locs].node_id  = 0;
            A_err_locs[num_a_err_locs].thread_id = j;
            A_err_locs[num_a_err_locs].macro_id = i;
            A_err_locs[num_a_err_locs].micro_id = 0;
            num_a_err_locs++;
            if(num_a_err_locs >= numErrs)
                return;
        }
        if(nleftovers < leftover){
            A_err_locs[num_a_err_locs].node_id  = 0;
            A_err_locs[num_a_err_locs].thread_id = threadsToErr;
            A_err_locs[num_a_err_locs].macro_id = i;
            A_err_locs[num_a_err_locs].micro_id = 0;
            num_a_err_locs++;
            nleftovers ++;
            if(num_a_err_locs >= numErrs)
                return;
        }
    }   
}
#endif
#ifdef INJECT_A_BEST
void setBestCaseA( int numProcs, int numErrs, int myrank )
{
    //local
    num_a_err_locs = 0;

    int glob_num_a_err_locs = 0;
    int errsPerProc = ceil((double) numErrs / (double) numProcs );
    int errsPerThread = ceil((double) errsPerProc / 16.0);
    //printf("errs per proc %d\t errs per thread %d\n", errsPerProc, errsPerThread);
    for( int k = 0; k < errsPerThread; k++){
        for( int j = 0; j < 16; j++){
            for( int i = 0; i < numProcs; i++){
                if( i == myrank ){
                    //printf("inserting error at rank %d thread %d place %d\n", i, j, k);
                    A_err_locs[num_a_err_locs].node_id  = i;
                    A_err_locs[num_a_err_locs].thread_id = j;
                    A_err_locs[num_a_err_locs].macro_id = k;
                    A_err_locs[num_a_err_locs].micro_id = 0;
                    num_a_err_locs++;
                }
                glob_num_a_err_locs++;
                if(glob_num_a_err_locs >= numErrs){
                    //printf("num err locs %d\t%d\n", num_a_err_locs, glob_num_a_err_locs);
                    return;
                }
            }
        }
    }
}
#endif


#define D(x) ((double)(x))
#define min(a,b) (((a)>(b))?(b):(a))
#define max(a,b) (((a)<(b))?(b):(a))

static double time_in_seconds (void)
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return (double)tv.tv_sec + (double)tv.tv_usec / 1000000.0;
}

static double randData(double low, double high){
    double t = (double)rand() / (double)RAND_MAX;
    return (1.0 - t) * low + t * high;
}

#define RAND01() randData(0.0, 1.0)

#define P(mat,i,j,lda) ((mat)+(i)+(lda)*(j))
void copyMat(int m, int n, double *From, int ldfrom, double *To, int ldto)
{
  #if defined(PERF)
  double t0=time_in_seconds();
  #endif
  #pragma omp parallel for
  for(int c=0; c < n; c++)
   for(int r=0; r < m; r++)
     *P(To, r, c, ldto)=*P(From, r, c, ldfrom);
  #if defined(PERF)
  double t1=time_in_seconds();
  printf("copy[%d x %d]=%.2lf GB/s\n", m,n, D(16*m*n*2)/(t1-t0)/1e9);
  #endif
}




using namespace std;

#ifdef __cplusplus
extern "C" {
#endif
int numroc_(int *n, int *nb, int *iproc, int *isrcprocs, int *nprocs); //error: unresolved external symbols
#ifdef __cplusplus
}
#endif

inline int get_num_threads()
{
  int nthreads_;
  #pragma omp parallel
  #pragma omp master
  {
    nthreads_=omp_get_num_threads( );
  }
  return nthreads_;
}


#define MIN(a, b)    ((a) < (b) ? (a) : (b))
#define MAX(a, b)    ((a) > (b) ? (a) : (b))


static void usage (char *call)
{
    printf ("Usage: %s <matrix size> "
            "<#nodes per row> "
            "<#nodes per column>"
            "<#stats=0|1>"
            "<#overlap=0|1>\n", call);
}


// ring broadcast (pipelined)
static void ring_bcast (double *buf, int count, MPI_Datatype type, int root, MPI_Comm comm)
{
    int me;
    int np;
    MPI_Status status;

    MPI_Comm_rank (comm, &me);
    MPI_Comm_size (comm, &np);
    if (me != root)
    {
        MPI_Recv (buf, count, type, (me - 1 + np) % np, MPI_ANY_TAG, comm,
                  &status);
    }
    if ((me + 1) % np != root)
    {
        MPI_Send (buf, count, type, (me + 1) % np, 0, comm);
    }
}

#define bcast ring_bcast
#define bcast MPI_Bcast


typedef struct 
{
  int ii, 
      jj, 
      kk, 
      iwrk,
      icurcol,
      icurrow;
} Iter;
static void my_pdgemm (int n, int nb,
                       double *A, double *B, double *C,
                       int nrows, int ncols, int ldx,
                       int *nr, int *nc,
                       MPI_Comm comm_row, MPI_Comm comm_col,
                       double *work1, double *work2, int ldw1, 
                       int overlap, int stats)
{
    int myrank_row;
    int myrank_col;
    int myrow;
    int mycol;

    // get row and col communicator
    MPI_Comm_rank (comm_row, &myrank_row);
    MPI_Comm_rank (comm_col, &myrank_col);
    myrow = myrank_col;
    mycol = myrank_row;

   
    double tot_dgemm_pernode=0, tot_time_pernode=0, tot_bcst_pernode=0, tot_gflops_pernode=0;
    tot_time_pernode=time_in_seconds();

    int nthreads=get_num_threads();
    // zeros C
    memset (C, 0, sizeof(double) * nrows * ldx);
    int ndgemms=0;
    if(overlap==1)
    {
       assert(0);
       // create linear iteration space
       int icurrow = 0;
       int icurcol = 0;
       int ii=0, jj = 0;
       int iwrk = 0;
       vector <Iter> iter(0);
       for (int kk = 0; kk < n; kk += iwrk)
       {
           iwrk = MIN (nb, nr[icurrow] - ii);
           iwrk = MIN (iwrk, nc[icurcol] - jj);
           Iter it={ii, jj, kk, iwrk, icurcol, icurrow};
           iter.push_back(it);
    
           ii += iwrk;
           jj += iwrk;
           if (jj >= nc[icurcol])
           {
               icurcol++;
               jj = 0;
           }
           if (ii >= nr[icurrow])
           {
               icurrow++;
               ii = 0;
           }
       }

       // software pipeline   
       double *work1_1 = work1, *work1_2 = work1 + nrows * ldw1;
       double *work2_1 = work2, *work2_2 = work2 + nb * ldx;
       int niterations = (int)(iter.size());
       omp_set_nested(1);
       omp_set_dynamic(0);
       int nthreads_bct=1, nthreads_gem=nthreads-nthreads_bct;
       for(int i=-1; i < niterations; i++)
       {
         
          int bcstsize=0;
          double ts_dgemm, te_dgemm, ts_bcst, te_bcst;
          double ts=time_in_seconds();
          #pragma omp parallel num_threads(2)
          {
            
            // int nthreads=omp_get_num_threads();
            int tid=omp_get_thread_num();
            if(tid == 0)
            {
              if(i >= 0)
              {
                ts_dgemm=time_in_seconds();
                int iwrk=iter[i].iwrk,
//                mkl_set_num_threads(nthreads_gem);
                omp_set_num_threads(nthreads_gem);
//                mkl_set_dynamic(0);
                /*cblas_dgemm (CblasRowMajor, CblasNoTrans, CblasNoTrans, nrows, ncols,
                             iwrk, 1.0, work1_1, ldw1, work2_1, ldx, 1.0, C, ldx);*/
                obj_t a, b, c;
                bli_obj_create_with_attached_buffer( BLIS_DOUBLE, nrows, iwrk, work1_1, ldw1, 1, &a);
                bli_obj_create_with_attached_buffer( BLIS_DOUBLE, iwrk, ncols, work2_1, ldx, 1, &b);
                bli_obj_create_with_attached_buffer( BLIS_DOUBLE, nrows, ncols, C, ldx, 1, &c);
                bli_gemm( &BLIS_ONE, &a, &b, &BLIS_ONE, &c );

                te_dgemm=time_in_seconds();
                ndgemms++;
                tot_gflops_pernode += 2.0*D(nrows)*D(ncols)*D(iwrk)/(te_dgemm-ts_dgemm);
                tot_dgemm_pernode+=te_dgemm-ts_dgemm;
              }
            }
            else
            {
              if(i < niterations - 1)
              {
                ts_bcst=time_in_seconds();
                int ii=iter[i+1].ii,
                    jj=iter[i+1].jj,
                    iwrk=iter[i+1].iwrk,
                    icurcol=iter[i+1].icurcol,
                    icurrow=iter[i+1].icurrow;

//                omp_set_num_threads(nthreads_bct);

                if (mycol == icurcol)
                {
                    // dlacpy_ ("General", &iwrk, &nrows, &A[jj], &ldx, work1_2, &ldw1);
                    copyMat(iwrk, nrows, &A[jj], ldx, work1_2, ldw1);
                }
                if (myrow == icurrow)
                {
                    // dlacpy_ ("General", &ncols, &iwrk, &B[ii * ldx], &ldx, work2_2, &ldx);
                    copyMat(ncols, iwrk, &B[ii * ldx], ldx, work2_2, ldx);
                }
                bcast (work1_2, nrows * ldw1, MPI_DOUBLE, icurcol, comm_row);
                bcast (work2_2, ldx * iwrk, MPI_DOUBLE, icurrow, comm_col);
                bcstsize=(nrows * ldw1+ldx * iwrk)*8;
                te_bcst=time_in_seconds();
              }
             }
          }
          double te=time_in_seconds();
          double perf=2.0*D(nrows)*D(ncols)*D(iwrk)/(te_dgemm-ts_dgemm);
          if(stats)
            printf("wt overlap nt=%2d(%2d+%2d) total=%9.3lfs dgemm=%9.3lfs (%4.1lf Gflops) bcst=%9.3lfs size=%.3lfMb\n", 
                   nthreads, nthreads_gem, nthreads_bct, 
                   te-ts, te_dgemm-ts_dgemm, perf/1e9, te_bcst-ts_bcst, D(bcstsize)/D(1024*1024));
          double *tmp;
          tmp=work1_1; work1_1=work1_2; work1_2=tmp;
          tmp=work2_1; work2_1=work2_2; work2_2=tmp;
       }
    }
    else
    {
       // main loop
       int icurrow = 0;
       int icurcol = 0;
       int ii=0, jj = 0;
       int iwrk = 0;
       int i=0, bcstsize=0;;
       for (int kk = 0; kk < n; kk += iwrk)
       {
           iwrk = MIN (nb, nr[icurrow] - ii);
           iwrk = MIN (iwrk, nc[icurcol] - jj);

           double ts=time_in_seconds();
           double ts_bcst=time_in_seconds();
           if (mycol == icurcol)
           {
               // dlacpy_ ("General", &iwrk, &nrows, &A[jj], &ldx, work1, &ldw1);
               copyMat(iwrk, nrows, &A[jj], ldx, work1, ldw1);
           }
           if (myrow == icurrow)
           {
               // dlacpy_ ("General", &ncols, &iwrk, &B[ii * ldx], &ldx, work2, &ldx);
               copyMat(ncols, iwrk, &B[ii * ldx], ldx, work2, ldx);
           }
           double t0=time_in_seconds();
           bcast (work1, nrows * ldw1, MPI_DOUBLE, icurcol, comm_row);
           bcast (work2, ldx * iwrk, MPI_DOUBLE, icurrow, comm_col);
           bcstsize=(nrows * ldw1+ldx * iwrk)*8;
           double t1=time_in_seconds();
           // printf("size=%d bcs=%lfs\n", nrows*ldw1+ldx*iwrk, t1-t0);
           double te_bcst=time_in_seconds();
           tot_bcst_pernode+=te_bcst-ts_bcst;

           double ts_dgemm=time_in_seconds();
           //mkl_set_num_threads(nthreads);
           //cblas_dgemm (CblasRowMajor, CblasNoTrans, CblasNoTrans, nrows, ncols,
           //             iwrk, 1.0, work1, ldw1, work2, ldx, 1.0, C, ldx);
                obj_t a, b, c;
                bli_obj_create_with_attached_buffer( BLIS_DOUBLE, nrows, iwrk, work1, ldw1, 1, &a);
                bli_obj_create_with_attached_buffer( BLIS_DOUBLE, iwrk, ncols, work2, ldx, 1, &b);
                bli_obj_create_with_attached_buffer( BLIS_DOUBLE, nrows, ncols, C, ldx, 1, &c);
                //printf("%d\n", omp_get_thread_num());
                bli_gemm( &BLIS_ONE, &a, &b, &BLIS_ONE, &c );
           double te_dgemm=time_in_seconds();
           ndgemms++;
           tot_dgemm_pernode+=te_dgemm-ts_dgemm;
           tot_gflops_pernode += 2.0*D(nrows)*D(ncols)*D(iwrk)/(te_dgemm-ts_dgemm);

           double te=time_in_seconds();
           double perf=2.0*D(nrows)*D(ncols)*D(iwrk)/(te_dgemm-ts_dgemm);
           if(stats)
             printf("no overlap nt=%2d(%2d+%2d) total=%9.3lfs dgemm=%9.3lfs (%4.1lf Gflops) bcst=%9.3lfs size=%.3lfMb\n",
                    nthreads, nthreads, nthreads,
                    te-ts, te_dgemm-ts_dgemm, perf/1e9, te_bcst-ts_bcst, D(bcstsize)/D(1024*1024));
           ii += iwrk;
           jj += iwrk;
           if (jj >= nc[icurcol])
           {
               icurcol++;
               jj = 0;
           }
           if (ii >= nr[icurrow])
           {
               icurrow++;
               ii = 0;
           }
           
           i++;
       }
   }
   tot_time_pernode=time_in_seconds()-tot_time_pernode;

   double tot_dgemm=0, tot_bcst=0, tot_gflops=0, tot_time=0;
   MPI_Reduce (&tot_dgemm_pernode, &tot_dgemm, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
   MPI_Reduce (&tot_bcst_pernode, &tot_bcst, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
   MPI_Reduce (&tot_gflops_pernode, &tot_gflops, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
   MPI_Reduce (&tot_time_pernode, &tot_time, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

#if 0
   int myrank;
   MPI_Comm_rank (MPI_COMM_WORLD, &myrank);
   if(myrank==0)
   {
     int nprocs;
     MPI_Comm_size (MPI_COMM_WORLD, &nprocs);
     double perf=tot_gflops/D(ndgemms)/nprocs;
     printf ("overlap=%d nb=%d tot_time/node=%.3lf secs dgemm/node=%.3lf secs (%.1lf Gflops/node) bcst/node=%.3lf secs\n",
             overlap, nb, tot_time/nprocs, tot_dgemm/nprocs, perf/1e9, tot_bcst/nprocs);
 
   }
#endif
}

void PrintMat(int n, double *M, int ldx, char *s)
{
    printf("%s\n", s);
    for(int i=0; i < n; i++)
      for(int j=0; j < n; j++)
        printf("[%d %d] %lf\n", i, j, M[i*ldx+j]);
}

void gatherMatrix(int nrows, int ncols, double *From, int ldx, double *To, int nbf,
                  int *nr, int *nc, int myrank, int nprocs, int nprows, int npcols)
{
   MPI_Barrier(MPI_COMM_WORLD);
   if (myrank)
   {
     MPI_Send (&ncols, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
     MPI_Send (&nrows, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
     MPI_Send (&ldx, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
     MPI_Send (From, nrows*ldx, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
     // printf("Sending %d %d %d from (%d %d)\n", ncols, nrows, ldx, myrow, mycol);
   }
   else
   {
     double *tmp=new double[nbf*nbf];
     int startrow = 0, startcol=0;
     copyMat(nrows, ncols, From, ldx, &To[startrow*nbf+startcol], nbf);
     // PrintMat(nrows, &To[startrow*nbf+startcol], nbf, "myrank=0");
     if(nprocs>1)
     {
       for(int pr=0; pr < nprows; pr++)
       {
          startcol=0;
          for(int pc=0; pc < npcols; pc++)
          {
            if(pc==0 && pr==0) {
              startcol += nc[pr];
              continue;
            }

            int from=pr*npcols+pc;
            int nrows_, ncols_, ldx_;
            MPI_Status status;
            MPI_Recv(&ncols_, 1, MPI_INT, from, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            // printf("Resv %d %d %d at (%d %d)\n", ncols_, nrows_, ldx_, myrow, mycol);

            MPI_Recv(&nrows_, 1, MPI_INT, from, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            MPI_Recv(&ldx_, 1, MPI_INT, from, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            MPI_Recv(tmp, nrows_*ldx_, MPI_DOUBLE, from, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            // printf("Received from pr=%d pc=%d copying to (%d %d) of Glibal nbf=%d\n", pr, pc, startrow, startcol, nbf);
            // PrintMat(nrows_, tmp, ldx_, "tmp");

            copyMat(nrows_, ncols_, tmp, ldx_, &To[startrow*nbf+startcol], nbf);
            // PrintMat(nrows_, &To[startrow*nbf+startcol], nbf, "tmp");
            startcol += nc[pr];
          }
          startrow += nr[pr];
       }
     }
     delete [] tmp;
   }

#if 0
   MPI_Barrier(MPI_COMM_WORLD);
   if(myrank==0)
     PrintMat(nbf, &To[0], nbf, "tmp");
   MPI_Barrier(MPI_COMM_WORLD);
#endif
}

int main (int argc, char **argv)
{    
    // init MPI
    int myrank;
    int nprocs;
    int overlap;
    int stats;
    MPI_Init (&argc, &argv);
    MPI_Comm_rank (MPI_COMM_WORLD, &myrank);
    MPI_Comm_size (MPI_COMM_WORLD, &nprocs);

    bli_init();
#if 0
    char hostname[1024]; 
    gethostname (hostname, 1024);  
    printf ("Rank %d of %d running on node %s\n", myrank, nprocs, hostname);
#endif
    
#if 0
    float a[1024];
    #pragma omp parallel for
    for (int i = 0; i < 1024; i++)
      a[i]=0;
    MPI_Finalize ();
    return 0;
#endif

    // input parameters and load basis set
    int nprows;
    int npcols;
    int nbf;
    if (myrank == 0)
    {
        if (argc != 6)
        {
            usage (argv[0]);
            MPI_Finalize ();
            exit (0);
        }
        
        // init parameters
        nbf = atoi (argv[1]);
        nprows = atoi (argv[2]);
        npcols = atoi (argv[3]);
        overlap = atoi (argv[4]);
        stats = atoi (argv[5]);
        assert (nprows * npcols == nprocs);
        assert (nbf > 0);
    }
    MPI_Bcast (&nprows, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast (&npcols, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast (&nbf, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast (&overlap, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast (&stats, 1, MPI_INT, 0, MPI_COMM_WORLD);
    
    // initialize communicators    
    int rowid = myrank / npcols;
    int colid = myrank % npcols;
    MPI_Comm comm_col;
    MPI_Comm comm_row;
    MPI_Comm_split (MPI_COMM_WORLD, rowid, colid, &(comm_row));
    MPI_Comm_split (MPI_COMM_WORLD, colid, rowid, &(comm_col));
    int nb = MIN(nbf/nprows, nbf/npcols);
    int mycol;
    int myrow;
    int izero = 0;
    MPI_Comm_rank (comm_row, &mycol);
    MPI_Comm_rank (comm_col, &myrow);
    int nrows = numroc_ (&nbf, &nb, &myrow, &izero, &nprows);
    int ncols = numroc_ (&nbf, &nb, &mycol, &izero, &npcols);    
    
#if 0
    MPI_Barrier(MPI_COMM_WORLD);
    for(int r=0; r < nprocs; r++)
    {
      if(r==myrank)
        printf("nbf=%d (myrow=%d mycol=%d) (nrows=%d ncols=%d) nb=%d\n", nbf, myrow, mycol, nrows, ncols, nb);
      MPI_Barrier(MPI_COMM_WORLD);
    }
    exit(0);
#endif
 
    // positions of partitions
    int *nr = (int *)malloc (sizeof(int) * nprows);
    int *nc = (int *)malloc (sizeof(int) * npcols);
    assert (nr != NULL);
    assert (nc != NULL);   
    // get nr and sr
    MPI_Allgather (&nrows, 1, MPI_INT, nr, 1, MPI_INT, comm_col);
    int startrow = 0;
    for (int i = 0; i < myrow; i++)
    {
        startrow += nr[i];
    }
    const int endrow = startrow + nrows - 1;    
    // get nc and sc
    MPI_Allgather (&ncols, 1, MPI_INT, nc, 1, MPI_INT, comm_row);
    int startcol = 0;
    for (int i = 0; i < mycol; i++)
    {
        startcol += nc[i];
    }
    const int endcol = startcol + ncols - 1;
    // for matrix trace
    const int start = MAX (startcol, startrow);
    const int tr_len = MIN (endcol, endrow) - start + 1;
    const int tr_scol = start - startcol;
    const int tr_srow = start - startrow;
    // create local arrays
    #define ALIGNSIZE 8
    int ldx = (ncols + ALIGNSIZE - 1)/ALIGNSIZE * ALIGNSIZE;
    const int meshsize = nrows * ldx;
    double *A_block = (double *)mkl_malloc (meshsize * sizeof(double), 64);
    assert (A_block != NULL);
    double *B_block = (double *)mkl_malloc (meshsize * sizeof(double), 64);
    assert (B_block != NULL);
    double *C_block = (double *)mkl_malloc (meshsize * sizeof(double), 64);
    assert (C_block != NULL);

    // working space for purification
    const int ldw1 = (nb + ALIGNSIZE - 1)/ALIGNSIZE * ALIGNSIZE;
    // times 2 for overlap
    double *work1 = (double *)mkl_malloc (2 * nrows * ldw1 * sizeof(double), 64);
    assert (work1 != NULL);
    double *work2 = (double *)mkl_malloc (2 * nb * ldx * sizeof(double), 64);
    assert (work2 != NULL);

    // D is symmetric matrix
    #pragma omp parallel for
    for (int i = 0; i < nrows; i++)
    {
        for (int j = 0; j < ncols; j++)
        {
            A_block[i * ldx + j] = RAND01()/(double)nbf;
            B_block[i * ldx + j] = RAND01()/(double)nbf;
            C_block[i * ldx + j] = 0.0;
        }
    }
 
    MPI_Barrier(MPI_COMM_WORLD);

    {
      for(int r=0; r < nprocs; r++)
      {
         MPI_Barrier(MPI_COMM_WORLD);
         if(r==myrank) {
           // printf("myrank=%d\n", myrank);
           // PrintMat(nrows, D_block, ldx, "D_block");
         }
         MPI_Barrier(MPI_COMM_WORLD);
      }
    }
#if 0
    // benchmark dgemm
    MPI_Barrier(MPI_COMM_WORLD);
    // if(myrank == 0)
    {
      int n=min(nb, 2000);
      /*double *A=(double *)_mm_malloc(sizeof(A[0])*n*n, 64);
      double *B=(double *)_mm_malloc(sizeof(A[0])*n*n, 64);
      double *C=(double *)_mm_malloc(sizeof(A[0])*n*n, 64);*/
      /*for(int i=0; i < n*n; i++)
      {
         A[i]=RAND01();
         B[i]=RAND01();
         C[i]=RAND01();
      }*/
      obj_t a, b, c;
/*      bli_obj_create_with_attached_buffer( BLIS_DOUBLE, n, n, A, 1, n, &a);
      bli_obj_create_with_attached_buffer( BLIS_DOUBLE, n, n, B, 1, n, &b);
      bli_obj_create_with_attached_buffer( BLIS_DOUBLE, n, n, C, 1, n, &c);*/
      bli_obj_create( BLIS_DOUBLE, n, n, 0, 0, &a);
      bli_obj_create( BLIS_DOUBLE, n, n, 0, 0, &b);
      bli_obj_create( BLIS_DOUBLE, n, n, 0, 0, &c);

      bli_randm( &a );
      bli_randm( &b );
      bli_randm( &c );

      int ntsaved=get_num_threads(), nthreads=get_num_threads()-1;
#if 0
      mkl_set_num_threads(nthreads);
      omp_set_num_threads(nthreads);
#endif
      double mintime=DBL_MAX;
      for(int it=0; it < 4; it++)
      {
        double t0=time_in_seconds();
        //printf("A, %d\n", omp_get_num_threads());
        bli_gemm( &BLIS_ONE, &a, &b, &BLIS_ONE, &c );
        //printf("B\n");
        double t1=time_in_seconds();
       // bli_randm( &c );
        mintime=min(mintime, t1-t0);
      }
      printf("^^myrank=%d %d x%d dgemm on %3d threads gets %.2lf Gflops^^\n", myrank, n, n, nthreads, D(2*n)*D(n)*D(n)/mintime/1e9);
/*      _mm_free(A);
      _mm_free(B);
      _mm_free(C);*/
//      omp_set_num_threads(ntsaved);
    }
    MPI_Barrier(MPI_COMM_WORLD);
#endif

    // main loop
    double t1, t2;
    struct timeval tv1, tv2;
    double flops = 2.0 * nbf * nbf * nbf;
    double timedgemm = 0.0;

    #define ITERS 5
    t1 = time_in_seconds();
    MPI_Barrier(MPI_COMM_WORLD);
    for (int iter = 0; iter < ITERS; iter++)
    {     
        // D2 = D*D;
        // D3 = D2*D;
#ifdef INJECT
        bli_reset_err_locs();
#endif
#ifdef INJECT_C_WORST
        setWorstCaseC( nprocs, 250*(nbf/4000) * (nbf/4000) * (nbf/4000), myrank );
#endif
#ifdef INJECT_C_BEST
        setBestCaseC( nprocs, 250*(nbf/4000) * (nbf/4000) * (nbf/4000), myrank );
#endif
#ifdef INJECT_A_WORST
        setWorstCaseA( nprocs, 250*(nbf/4000) * (nbf/4000) * (nbf/4000), myrank );
#endif
#ifdef INJECT_A_BEST
        setBestCaseA( nprocs, 250*(nbf/4000) * (nbf/4000) * (nbf/4000), myrank );
#endif
        gettimeofday (&tv1, NULL);
        my_pdgemm (nbf, nb, A_block, B_block, C_block, nrows, ncols, ldx,
                   nr, nc, comm_row, comm_col, work1, work2, ldw1, overlap, stats);
#ifdef INJECT
        printf("Errors tracked= %d\trank = %d\n",error_tracker,myrank);
#endif
    }
    MPI_Barrier(MPI_COMM_WORLD);
    t2 = time_in_seconds();
    timedgemm=t2-t1;
    if (myrank == 0)
    {
        printf ("%d x %d dgemm time %.3lf secs, %.3lf GFlops\n",
                nbf, nbf, timedgemm, ITERS*flops/(timedgemm)/1e9);
    }
   
    MPI_Finalize ();
    
    return 0;
}
