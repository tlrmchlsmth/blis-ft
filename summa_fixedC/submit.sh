#!/bin/bash
#SBATCH -J  4node          # job name
#SBATCH -o 4node.o%j       # output and error file name (%j expands to jobID)
#SBATCH -N 4 -n 4
#SBATCH -p development       # queue (partition) -- normal, development, etc.
#SBATCH -t 00:05:00         # run time (hh:mm:ss) - 1.5 hours

export BLIS_JC_NT=8
export BLIS_IC_NT=2
export KMP_AFFINITY=granularity=fine,compact,1,0

echo "Base"
ibrun ./base.exe 8000 2 2 0 0 # run the MPI executable named a.out
echo "Detect"
ibrun ./detect.exe 8000 2 2 0 0 # run the MPI executable named a.out
echo "A_worst"
ibrun ./a_worst.exe 8000 2 2 0 0 # run the MPI executable named a.out
echo "A_best"
ibrun ./a_best.exe 8000 2 2 0 0 # run the MPI executable named a.out
