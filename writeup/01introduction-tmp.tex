{\color{red} Enrique: I skip the first two paragraphs as Misha may want to rewrite them}

As High-Performance Computing (HPC) systems evolve, the number of components which comprise these systems increases.
At the same time the mean time to failure (MTTI) of each of these components will not improve enough to compensate for this increase.
As the result, probability of the of failure of an individual component increases.
Soft errors are transient fails that do not damage the hardware and are caused by high-energy particle incidence.
These energetic particles can  deposit charge in a transistor,
which when sufficiently large may force a nonconducting transistor to become inadvertently conducting for a short period of time,
thus causing unwarranted bit flips in computation, control and data.
Soft faults in chip circuitry pose challenges to application developer,
because they can produce incorrect results. Tera-scale systems were already vulnerable to soft errors,
with ASCI Q experiencing 26.1 CPU failures per week~\cite{Michalak05predictingthe}. For Peta-scale system softerror rate increased:
for Titan system, MTBF for double-bit errors occurring in register file and device memory is seven 
days~\cite{ http://on-demand.gputechconf.com/gtc/2015/presentation/S5566-James-Rogers.pdf}.
For Sequoa system, when accounting for instruction cache, data cache, and GPR and FPR register files,
the mean time between correctable errors is predicted to be 1.5 days ~\cite{Cher:2014:USE:2683593.2683658}.
As HPC systems move into era of Exascale, the number of system components will increase up to a 1000-fold.
As the result, the mean time between errors will further increase and hence resilience becomes a fundamental concern ~\cite{Bergman08exascalecomputing}.

The vast and growing number of components in 
today's CMOS digital circuits %, dictated by Moore's Law, 
has turned {\em reliability} together with cooling limitations (also known as the {\em power wall}) into two major
computer architecture challenges that scientists and engineers have to face~\cite{Dur15,Luc14}. 
Indeed, there exists a direct connection between these two hurdles: on the one hand, the mean time between failures (MTBF) of circuits
decays as the temperature grows. % (see, e.g.~\cite{Fat07}). 
%which results in the hardware becoming less reliable. 
On the other hand, 
the benefits of cramming more cores into a chip have increased the appeal of operating the circuits at very low voltage (near--threshold voltage computing, NTVC)
in order to stay within the power budget~\cite{Kar13}. The hope in this approach is that the higher degree of concurrency will compensate the degradation of the frequency that 
is associated with lowering the voltage. Unfortunately, NTVC is also more prone to errors.

To tackle the aforementioned problems, current hardware systems feature 
a variety of sophisticated error recovery mechanisms 
at multiple levels%~\cite{Zha12}
, with a well-known example being 
% For example, most commercial processors include 
the ECC (error correction code) bits present in the cache memories of most commercial processors. 
%({\bf Misha: other examples?})
However, these solutions also consume a considerable amount of energy, 
with the corresponding negative impact on the power budget, and
the temperature--reliability duo.

The multiplication of two dense matrices (\gemm) 
plays a key role in scientific and engineering applications, as many complex codes are built on top of numerical linear algebra libraries 
(e.g., LAPACK, {\tt libflame}, ScaLAPACK, Elemental, etc.),
which internally cast a large fraction of its computations in terms of \gemm. 
%This is recognized by most hardware vendors, and currently 
%there exist highly optimized implementations of this operation
%for almost any processor architecture (e.g., \gemm\ is available in libraries such as Intel's MKL, AMD's ACML, IBM's ESSL, and NVIDIA's CUBLAS). 
%
\rvdgFromTo{}{Moreover, techniques designed for \gemm\ can often be
  easily generalized to other matrix operations.}
Developing a high-performance fault-tolerant \gemm\ is therefore a crucial first step towards creating efficient fault-tolerant
linear algebra libraries and, in consequence, more 
reliable scientific and engineering applications.

In this paper we revisit \gemm\ with the goal of
embedding a software layer for fault tolerance into \gemm\ that can tolerate multiple errors
while retaining high performance. 
We consider {\em silent data corruption}~\cite{Bron08,Muk05} only, which does not abort the program execution
but yields an incorrect result of the computation if not properly addressed. 
Furthermore, %we assume these errors occur 
%when the operands are in the processor datapath (e.g., in the floating point units, 
%or FPUs, the reordering buffer, the register bank, etc.), 
%they only affect the results of the floating-point arithmetic operations,
we assume that these errors manifest themselves in incorrect results of floating point 
computation%\footnote{Data corruption 
%during \gemm\ affecting the integer arithmetic, e.g., to calculate a memory address may
%result in loading/storing an incorrect data/result floating-point number. 
%Thus, they will likely trigger an error in
%in the result, and they can be expected to be detected by the same fault tolerance mechanism. 
%Data corruption in the branch logic can be expected to produce a hard error.}
.
As such they can originate anywhere in processor datapath; e.g., in
the register file, floating point units (FPUs), reorder buffer, front-end of the pipeline,
or cache controller. \rvdgFromTo{W}{In this initial study, w}e further assume that errors in the cache memory are solved via conventional ECC.
%and that errors in the cache memory are solved via conventional ECC bits.

We make the following original contributions with respect to
previous work~\cite{Hua84,Gun01,Wu11}:
\begin{itemize}
\item Our fault tolerance approach relies on the blocked \gemm\ algorithm 
      underlying \rvdgFromTo{}{the BLAS-like Library Instantiation
        Software (}BLIS\rvdgFromTo{}{)}~\cite{BLIS1}, which ``recursively'' decomposes this kernel into a number of 
      smaller \gemm\ suboperations. 
      Error recovery is applied ``on-line'' (i.e., at execution time) to
      each suboperation, tolerating multiple errors per suboperation as well as
      errors in multiple suboperations. Error detection is based on 
      the use of left and right checksum vectors~\cite{Hua84,Gun01}. 
      For error correction, we simply recompute (a part of) the faulty
      suboperation \rvdgFromTo{}{(checkpoint and restart)}.
\item Our fault tolerance mechanism can be applied 
      at any of the layers exposed in BLIS \gemm, which 
      operates with smaller matrix multiplication suboperations of different 
      granularity at each layer. In this manner, we gain detailed control 
      on the workspace requirements, the error detection overhead, and the error correction cost, 
      which then offers a means to adapt the mechanism to a target
      MTBF.
\rvdgFromTo{}{We discuss the pros and cons of injecting fault
  tolerance at the different layers.}
%\item We extend the fault tolerance mechanism to operate also in a multi-threaded execution of
%      \gemm, which is crucial for this approach to be practical on current multicore processors.
\item We demonstrate practical high performance using a fault-tolerant version of BLIS
      on an \ixeon, with the error recovery layer integrated into the
      BLIS macro-kernel (also known as the ``inner kernel'' in GotoBLAS~\cite{Goto}).
      Our implementation for this architecture overlaps the data transfers 
      between the cache memory levels intrinsic to BLIS 
      with the computations due to error detection.
      \rvdgFromTo{}{This reduces additional memory movements that
        would be encountered otherwise.}
\item
\rvdgFromTo{Finally, we augment our fault-tolerant implementation with a simple technique
      that tolerates errors below a certain threshold, thus turning the original fault-tolerant
      mechanism into a technique for {\em approximate computing}, which trades off numerical
      accuracy for performance.}{We investigate the impact of %the
      adding the proposed fault-tolerance mechanism into multi-threaded and
      distributed memory parallel implementations of \gemm~\cite{BLIS3,SUMMA}.}
%\item {\bf For the future: None of the previous works has addressed multi-threading. 
      %In BLIS, if we want to parallelize certain loops, this requires having to synchronize
      %concurrent updates to the checksum vectors by multiple threads. We can deal with this
      %by each thread computing a separate checksum and performing a reduction in the end.}
\end{itemize}

In this paper, we will consider the ``extended'' form of \gemm,
\rvdgFromTo{$C \mathrel{+}= A\cdot B$}{$C \mathrel{+}= A B$}, where
$C \in \R^{m \times n}$, 
$A \in \R^{m \times k}$ and
$B \in \R^{k \times n}$, though in some cases we will simplify it to the more ``basic''
case \rvdgFromTo{$C = A\cdot B$}{$C = A B$}.
Hereafter, we will define the problem dimension using the triple $(m,n,k)$.

{\color{red} The rest of the paper is organized as follows...}
