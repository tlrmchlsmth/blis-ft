% no -g option given; defaulting to "input.general" for parameters filename.
% no -o option given; defaulting to "input.operations" for operations filename.
% 
% --- BLIS library info -------------------------------------
% 
% version string               0.1.6
% 
% --- BLIS config header ---
% 
% integer type size (bits)     64
% # of floating-point types    4
% maximum type size            16
% 
% maximum number of threads    16
% 
% SIMD alignment (bytes)       32
% 
% stack memory allocation        
%   address alignment (bytes)  32
% 
% dynamic memory allocation      
%   address alignment          32
%   stride alignment           64
% 
% contiguous memory allocation   
%   # of mc x kc blocks        16
%   # of kc x nc blocks        8
%   # of mc x nc blocks        200
%   block address alignment    4096
%   max preload byte offset    128
%   actual pool sizes (bytes)    
%     for mc x kc blocks of A  4821120
%     for kc x nc panels of B  101482624
%     for mc x nc panels of C  1888256128
% 
% BLAS compatibility layer       
%   enabled?                   1
%   integer type size (bits)   64
% 
% --- BLIS kernel header ---
% 
% floating-point types           s       d       c       z 
%   sizes (bytes)                4       8       8      16
% 
% level-3 def cache blkszes      s       d       c       z 
%   m dimension                128      96     128      96
%   k dimension                384     256     192     128
%   n dimension               4096    4096    4096    4096
% 
% level-3 max cache blkszes      s       d       c       z 
%   m dimension                128      96     128      96
%   k dimension                384     256     192     128
%   n dimension               4096    4096    4096    4096
% 
% level-3 register blocksizes    s       d       c       z 
%   m dimension                  8       8       8       8
%   n dimension                  8       4       8       4
% 
% level-3 pack register blksz    s       d       c       z 
%   m dimension                  8       8       8       8
%   n dimension                  8       4       8       4
% 
% micro-panel alignment (bytes)  s       d       c       z 
%   A (left matrix)              4       8       8      16
%   B (right matrix)             4       8       8      16
% 
% level-2 cache blocksizes       s       d       c       z 
%   m dimension               1000    1000    1000    1000
%   n dimension               1000    1000    1000    1000
% 
% level-1f fusing factors        s       d       c       z 
%   default                      8       4       4       2
%   axpyf                        8       4       4       2
%   dotxf                        8       4       4       2
%   dotxaxpyf                    8       4       4       2
% 
% micro-kernel types             s       d       c       z
%   gemm                    optmzd  optmzd  virt4m  virt4m
%   gemmtrsm_l              refnce  refnce  virt4m  virt4m
%   gemmtrsm_u              refnce  refnce  virt4m  virt4m
%   trsm_l                  refnce  refnce  virt4m  virt4m
%   trsm_u                  refnce  refnce  virt4m  virt4m
% 
% --- BLIS implementation details ---
% 
% level-3 implementations        s       d       c       z
%   gemm                    native  native      4m      4m
%   hemm                    native  native      4m      4m
%   herk                    native  native      4m      4m
%   her2k                   native  native      4m      4m
%   symm                    native  native      4m      4m
%   syrk                    native  native      4m      4m
%   syr2k                   native  native      4m      4m
%   trmm                    native  native      4m      4m
%   trmm3                   native  native      4m      4m
%   trsm                    native  native      4m      4m
% 

% 
% --- BLIS test suite parameters ----------------------------
% 
% num repeats per experiment   5
% num matrix storage schemes   1
% storage[ matrix ]            c
% num vector storage schemes   1
% storage[ vector ]            c
% mix all storage schemes?     0
% general stride spacing       64
% num datatypes                1
% datatype[0]                  2 (d)
% problem size: first to test  64
% problem size: max to test    4096
% problem size increment       64
% enable 3mh?                  0
% enable 3m?                   0
% enable 4mh?                  0
% enable 4m?                   1
% error-checking level         1
% reaction to failure          i
% output in matlab format?     0
% output to stdout AND files?  0
% 

% 
% --- Section overrides ---
% 
% Utility operations           0
% Level-1v operations          0
% Level-1m operations          0
% Level-1f operations          0
% Level-2 operations           0
% Level-3 micro-kernels        0
% Level-3 operations           1
% 

% --- gemm ---
% 
% test gemm seq front-end?    1
% gemm m n k                  -1 -1 256
% gemm operand params         nn
% 

% blis_<dt><oper>_<params>_<storage>           m     n     k   gflops   resid      result
blis_dgemm_nn_ccc                             64    64   256   11.523   9.79e-17   PASS
blis_dgemm_nn_ccc                            128   128   256   38.836   1.73e-16   PASS
blis_dgemm_nn_ccc                            192   192   256   73.156   2.02e-16   PASS
blis_dgemm_nn_ccc                            256   256   256  104.858   2.29e-16   PASS
blis_dgemm_nn_ccc                            320   320   256  133.747   2.87e-16   PASS
blis_dgemm_nn_ccc                            384   384   256  161.319   2.91e-16   PASS
blis_dgemm_nn_ccc                            448   448   256  179.651   2.97e-16   PASS
blis_dgemm_nn_ccc                            512   512   256  193.676   3.09e-16   PASS
blis_dgemm_nn_ccc                            576   576   256  212.869   3.93e-16   PASS
blis_dgemm_nn_ccc                            640   640   256  224.055   3.91e-16   PASS
blis_dgemm_nn_ccc                            704   704   256  237.821   4.14e-16   PASS
blis_dgemm_nn_ccc                            768   768   256  243.934   4.38e-16   PASS
blis_dgemm_nn_ccc                            832   832   256  249.943   4.28e-16   PASS
blis_dgemm_nn_ccc                            896   896   256  257.706   4.28e-16   PASS
blis_dgemm_nn_ccc                            960   960   256  259.691   5.03e-16   PASS
blis_dgemm_nn_ccc                           1024  1024   256  257.739   4.94e-16   PASS
blis_dgemm_nn_ccc                           1088  1088   256  264.662   5.01e-16   PASS
blis_dgemm_nn_ccc                           1152  1152   256  264.594   5.23e-16   PASS
blis_dgemm_nn_ccc                           1216  1216   256  267.139   5.78e-16   PASS
blis_dgemm_nn_ccc                           1280  1280   256  268.178   5.56e-16   PASS
blis_dgemm_nn_ccc                           1344  1344   256  274.516   5.80e-16   PASS
blis_dgemm_nn_ccc                           1408  1408   256  274.256   6.13e-16   PASS
blis_dgemm_nn_ccc                           1472  1472   256  278.183   5.81e-16   PASS
blis_dgemm_nn_ccc                           1536  1536   256  277.309   5.99e-16   PASS
blis_dgemm_nn_ccc                           1600  1600   256  282.544   6.42e-16   PASS
blis_dgemm_nn_ccc                           1664  1664   256  284.445   6.53e-16   PASS
blis_dgemm_nn_ccc                           1728  1728   256  286.565   6.67e-16   PASS
blis_dgemm_nn_ccc                           1792  1792   256  288.450   6.77e-16   PASS
blis_dgemm_nn_ccc                           1856  1856   256  291.473   6.44e-16   PASS
blis_dgemm_nn_ccc                           1920  1920   256  292.218   6.80e-16   PASS
blis_dgemm_nn_ccc                           1984  1984   256  288.651   6.91e-16   PASS
blis_dgemm_nn_ccc                           2048  2048   256  285.912   6.69e-16   PASS
blis_dgemm_nn_ccc                           2112  2112   256  295.293   7.28e-16   PASS
blis_dgemm_nn_ccc                           2176  2176   256  293.749   7.18e-16   PASS
blis_dgemm_nn_ccc                           2240  2240   256  293.400   7.26e-16   PASS
blis_dgemm_nn_ccc                           2304  2304   256  295.489   7.39e-16   PASS
blis_dgemm_nn_ccc                           2368  2368   256  298.876   8.17e-16   PASS
blis_dgemm_nn_ccc                           2432  2432   256  299.356   7.55e-16   PASS
blis_dgemm_nn_ccc                           2496  2496   256  299.003   8.22e-16   PASS
blis_dgemm_nn_ccc                           2560  2560   256  297.100   8.12e-16   PASS
blis_dgemm_nn_ccc                           2624  2624   256  301.928   7.76e-16   PASS
blis_dgemm_nn_ccc                           2688  2688   256  300.958   8.18e-16   PASS
blis_dgemm_nn_ccc                           2752  2752   256  301.386   8.04e-16   PASS
blis_dgemm_nn_ccc                           2816  2816   256  302.630   8.40e-16   PASS
blis_dgemm_nn_ccc                           2880  2880   256  304.316   8.70e-16   PASS
blis_dgemm_nn_ccc                           2944  2944   256  304.904   8.70e-16   PASS
blis_dgemm_nn_ccc                           3008  3008   256  304.157   8.60e-16   PASS
blis_dgemm_nn_ccc                           3072  3072   256  296.869   8.79e-16   PASS
blis_dgemm_nn_ccc                           3136  3136   256  308.269   8.88e-16   PASS
blis_dgemm_nn_ccc                           3200  3200   256  308.532   8.87e-16   PASS
blis_dgemm_nn_ccc                           3264  3264   256  307.775   9.29e-16   PASS
blis_dgemm_nn_ccc                           3328  3328   256  307.405   9.09e-16   PASS
blis_dgemm_nn_ccc                           3392  3392   256  308.198   9.11e-16   PASS
blis_dgemm_nn_ccc                           3456  3456   256  308.526   9.34e-16   PASS
blis_dgemm_nn_ccc                           3520  3520   256  309.684   9.31e-16   PASS
blis_dgemm_nn_ccc                           3584  3584   256  303.534   9.88e-16   PASS
blis_dgemm_nn_ccc                           3648  3648   256  309.360   9.34e-16   PASS
blis_dgemm_nn_ccc                           3712  3712   256  310.634   9.67e-16   PASS
blis_dgemm_nn_ccc                           3776  3776   256  308.012   1.00e-15   PASS
blis_dgemm_nn_ccc                           3840  3840   256  308.606   1.00e-15   PASS
blis_dgemm_nn_ccc                           3904  3904   256  311.617   1.01e-15   PASS
blis_dgemm_nn_ccc                           3968  3968   256  309.853   1.00e-15   PASS
blis_dgemm_nn_ccc                           4032  4032   256  307.439   1.01e-15   PASS
blis_dgemm_nn_ccc                           4096  4096   256  259.789   9.96e-16   PASS

