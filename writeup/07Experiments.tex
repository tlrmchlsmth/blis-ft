\label{sec:results}

\NoShow{
In this section we evaluate the overheads of both error detection
and error correction introduced by both the
proposed mechanisms when added 
to the BLIS framework. We target a single core, a multicore processor,
and a manycore architecture.
}

\input fig_three

We report results on a dual socket system with two 
eight core Sandybridge (SNB) processors as well as 
an Intel Xeon Phi Knight's Corner (KNC) coprocessor.

%The peak performance of each core of the \ixeon\ is 28 (double-precision) GFLOPS in
%turbo mode (employed for single core experiments) and 21.6 GFLOPS in
%non-turbo mode (employed for multicore experiments), making it very
%difficult to judge what the theoretical peak is `for a given experiment.  
%For easy evaluation, all of our
%graphs show performance in terms of GFLOPS/core.

% \subsection{Implementation}
\vspace{0.05in}
\noindent{\bf Implementation.}
The described mechanisms were added to the BLIS framework, % version 0.1.6. 
using modified versions of the standard micro-kernels and 
the blocking parameters reported in~\cite{BLIS2}.
As mentioned, the standard implementations of BLIS
are highly competitive.

The following decisions were made regarding the
implementation of the detection and correction mechanism:
(1) Our implementation handles the extended \gemm\ operation using the approach that checkpoints $C$
before updating it, 
as described at the end of Section~\ref{sec:extended},
because that approach requires less bandwidth from main memory when no error occurs;
(2)
The implementation always uses the lazy left checksum approach because performance results
suggest that the cost of error detection outweighs the cost of error correction in most cases;
(3)
\NoShow{
We experiment with the tradeoffs between the lazy and greedy recomputation,
and we will default to the lazy update for its better load balancing.
}
We swap Loops~4 and~5 to place the lazy recomputation outside of the
outermost loop over the $n$ dimension for load balancing reasons.
\NoShow{
 and
(4)
Similarly, we experiment with the tradeoffs between repacking $B_c$
when it is detected that an element in it has been corrupted,
and we will default to repacking $B_c$ in order to prevent errors from propagating.
}%
With these decisions, the algorithm we use
for our experiments 
%(when not evaluating the tradeoffs between these decisions)
is shown in Fig.~\ref{fig:blis_ftgemm2}.
We note that we did not take the precaution mentioned in Section~\ref{sec:practical}
to prevent false negatives for the \ixeon,
but for the KNC, $A$ and $B$ were read from the assumed to be protected L1 cache twice.
This had very little impact on performance.

%\subsection{
\vspace{0.05in}
\noindent{\bf How to read the graphs.}
The tops of the graphs represent theoretical peak performance.
In some graphs we use problem sizes with fixed $k$, varying $m$ and $n$,
where $ k $ is the algorithmic block size, $ k_c $.
Such \gemm\ cases are often encountered in, for example, LAPACK implementations.
Furthermore, our fault tolerant mechanisms occur entirely within each rank-k update,
so the costs of the fault-tolerance do not change significantly once $k$ is greater than or equal to $ k_c =256 $
(see Fig.~\ref{tab:balance}),
making it more interesting to see how the costs of performing the checksums change with varying $m$ and $n$.
The rate of computation in GFLOPS uses the standard $ 2 m n k $ flop count.

\renewcommand{\ixeon}{SNB}
%\subsection{
\vspace{0.05in}
\noindent{\bf Experimental results for a conventional CPU.}
We now examine the overheads of our fault-tolerant BLIS implementation on {\ixeon}.
We first examine the costs associated with  error detection
when no errors occur.
% \input fig_detect
In Fig.~\ref{fig:detect} we report the overhead
of the error detection mechanism (labeled with ``BLIS-FT'') when no errors are
introduced, both for a single core and for two \ixeon\  processors with eight
cores each.
These overheads result from the computation of the checksum vectors 
and the checkpointing of $C$.
This shows that the overhead of fault-tolerance is in the $10\%$ range
for both the single and multi-threaded cases, once the problem size is
reasonably large.
Fig.~\ref{fig:detect} also breaks those costs down further,
showing the overhead if either the checksum vectors are computed
or the checkpointing of $C$ is performed.
This graph demonstrates that these two costs involved in error detection
are roughly equal.
Furthermore, this is the case both in the single-threaded case
and the multi-threaded case, so performing the checkpointing of $C$
is not causing the \gemm\ operation to be bandwidth limited, at least
with $ 16 $ threads.
%\footnote{%
%We note that an earlier implementation, which computed a contribution
%to $ \hat{C} $, checked it for correctness, and then added it to $ C $
%showed signs of being bandwidth limited.}

%\paragraph{Cost of the error detection and correction}

\input fig_three_B

We will now examine the effects of performance when one or more errors are introduced
into various stages of the computation.
In Fig.~\ref{fig:threeB}~(left), we illustrate the impact on performance when 
between one and ten errors are artificially introduced into matrix $C$ 
and then detected and corrected, with a single thread.
These errors were injected in such a way that there were no
false positives in recomputation.
\NoShow{
The problem sizes here are quite small, and in fact only a single macro-kernel is executed.
}
The performance impact for recomputation is relatively low even for small matrices.
The gap between the curve with no errors and the curve with a single introduced error
is greater than the gap between the single error curve and the 5 error curve,
and it is still greater than the gap between the single error cuve and the 10 error curve.
That this is true, even though the 10 error curve has ten times more work to do during rollback and recomputation, suggests that 
other costs that are associated with detecting an error can be more costly than recomputation.
The largest such cost is the left checksum.


In Fig.~\ref{fig:threeB}~(middle), we report the performance impact when a
single error is artificially introduced during a packing routine,
and then detected and corrected after that error propagates, corrupting potentially
hundreds of elements of $C$.
If an error is introduced into $A_c$, this will corrupt $min(n, n_c)$ elements
of $C$ that must be rolled back and recomputed.
In this case, an entire row of $C$ will be corrupted, since $n_c = 4096$, the maximum size for $n$ in this experiment.
This is illustrated in the curve labeled ``FT, 1 error in $A_c$''.
Next, if an error is introduced in $B_c$, this could potentially corrupt an entire column of $C$.
This is illustrated in the curve labeled ``FT, 1 error in $B_c$''.
Now the amount of recomputation that must occur changes with the problem size.
Furthermore, it will cause errors in many macro-kernels,
and each time a macro-kernel with error is performed,
the left checksum must be performed and the locations of these errors must be determined and 
recorded since the recomputation is performed lazily.
We believe it is these operations that account for the difference in performance between the lines labeled
``FT, 1 error in $A_c$'' and ``FT, 1 error in $B_c$'',
even though at the right-most datapoint in the graph they have the same amount of rollback and recomputation.
It is possible to detect if an error has occurred in $B_c$ after a single macro-kernel execution,
and then repack $B_c$ if this has happened.
In this case, a maximum of $m_c = 96$ elements may be corrupted.
This is illustrated in the curve labeled ``FT, 1 error in $B_c$, with repack''.
A large performance advantage is seen due to this optimization of repacking $B_c$, 
as fewer errors must be corrected, despite the extra time spent in repacking $B_c$.
It may be possible to repack only select elements of $B_c$ if an error is detected in it,
but in this experiment, we repacked the entire $B_c$.

In Fig.~\ref{fig:threeB}~(right),
we report performance when introducing errors into $B_c$ using both lazy and greedy recomputation.  Here we do not repack $B_C$ to demonstrate the case where many errors are encountered by one or only a few of the threads.
In this experiment, we parallelized Loop~5 with eight threads, and Loop~3 with two threads.
Thus, $B_c$ will be shared by each pair of threads parallelizing Loop~3,
and corrupt a single column of $C$.
With greedy recomputation, only one pair of threads will perform the recomputation of that corrupted column,
while the other threads wait.
In contrast,  with lazy recomputation all eight pairs of threads will perform that recomputation, yielding much better performance.
This demonstrates that lazy recomputation can be very effective in reducing the load imabalance that may
occur when few threads encounter many errors.

%The greedy version applies correcting as soon as an error is detected, potentially
%taking advantage of the fact that the data needed to perform the recomputation
%are still in the fast parts of the memory hierarchy.
%The lazy version instead
%delays correction.
%This implies that the data needed to perform the recomputation
%may no longer be in cache,
%but better load balance can be achieved while correcting,
%since multiple threads can collaborate to correct multiple errors.
%Either greedy or lazy with repack refers to an implementation that also repacks 
%$B_c$ when it is detected that some element in it may have been corrupted,
%thus preventing future corruptions in $C$. 
%However this prevention of errors is done at the expense of load imbalance,
%as some threads will have to wait as others repack their corrupted $B_c$.

\input fig_knc
% \subsection{
\vspace{0.05in}
\noindent{\bf Experimental results for a many-core coprocessor.}
We now examine the overheads of our fault-tolerant BLIS implementation on KNC.
In Fig.~\ref{fig:knc}, we show the regular BLIS implementation of \gemm, alongside a prototype fault-tolerant implementation.
In both implementations, we used 60 cores%
\footnote{
Our KNC has 61 cores, however 60 cores are usually employed in practice.
}, parallelizing Loop~3 with 15 threads, and Loop~2 with 16 threads.
(Each core must use four hyperthreads for high performance.)
This graph demonstrates that our error detection mechanisms scale to a many-core architecture.
We break down performance further and show that the overhead of performing checksums is relatively minor,
and the overhead of performing checkpointing is even smaller.
It is interesting that performing checkpointing can have such a low overhead on an architecture with so many threads,
despite the extra bandwidth to main memory that this entails.

It was important to use streaming store instructions such as 
\texttt{vmovnrapd} during the checkpointing to attain high performance.
The curve ``FT neither'' is the prototype fault-tolerant implementation of \gemm with neither checkpointing
nor checksums performed.
The fact that this curve is much slower than the normal BLIS implementation suggests that there are inefficiencies
in our foult-tolerant prototype (e.g. extra barriers and memory allocations) that can be removed, 
and thus performance can be closer to that of the regular implementation, especially for smaller problem sizes.

In this graph, we also report the performance exhibited when we introduce errors during the execution of a single micro-kernel.
In our experiment, we corrupted an $8 \times 1$ row of a single micro-tile of $C$ during computation.
Our prototype implementation performs recomputation on the granularity of a micro-kernel, 
and so a single $8 \times 30 \times 240$ matrix multiplication was performed for the recomputation.
This recomputation was performed by a single thread, and it was implemented with an inefficient triply nested loop,
so our implementation of it can still be improved.
However this was an extremely small amount of computation in the context of a $14400 \times 14400 \times 240$ \gemm.
This suggests that while our lazy recomputation solution to load imbalance problems works in the context of 
a multi-core system where one core encounters many errors,
it does not help load imbalance problems in the context of a many-core system where only one error is encountered.
This indicates that we should use dynamic parallelism to solve this load imbalance.

%\subsection{Affect on distributed parallel GEMM}
%
%\input fig_multinode
%In Figure~\ref{fig:multinode} we report how performance is affected
%when a distributed memory implementation of matrix-matrix
%multiplication (a simple implementation of the SUMMA
%algorithm~\cite{SUMMA}) locally uses our
%fault tolerant \dgemm.  For this experiment, we increase the number of
%errors that are introduced to be proportional to the runtime and
%number of cores (in other words, the number of errors per second per
%core is held constant).
%In the best case, the errors are equally distributed among the nodes.
%In the worst case, the errors occur all on one core of a single node, thus creating
%maximal load imbalance.  
%(However, at most one error per call to a
%macro-kernel is created, meaning that for the worst case errors on
%more than one core will occur if there are more than the number of calls to
%the macro-kernel on one core.) 
%When injecting errors into $A$ when packing, the rate of errors was taken to equal
%approximately one error per core per second.
%When injecting errors into $C$ during the execution of the micro-kernel, 
%the error rate was approximately five errors per core per second.
%For practical reasons when injecting errors, the number of errors was actually based on the problem size and the number of cores used.
%The total number of errors was equal to $C P \frac{N^3}{4000^3}$, where $C$ is a constant, $P$ is the number of nodes, and $N$ is the problem size.
%The constant 50 was chosen to correspond to roughly one error per core per second when injecting errors into $A$,
%and 250 was chosen to correspond to roughly five errors per core per second when injecting errors into $C$.
%
%We notice that except for the worst
%case when errors are introduced in $ A $ and $ C $ at quite high rates, the impact on performance is
%negligible beyond the overhead of the mechanism when no errors are encountered.
