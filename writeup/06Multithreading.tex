\rvdgFromTo{The paper~\cite{BLIS3} investigated which loops to parallelize given a cache hierarchy.
It also argued that it is beneficial to parallelize multiple loops,
and to parallelize at least one loop that iterates along the $m$ dimension of $C$
and one that iterates along the $n$ dimension of $C$.
This paper will employ the same strategy, 
but there are a couple of wrinkles created by the addition of fault
tolerance.}
{Much like the structure of the BLIS implementation allows one to
  systematically reason about where to insert a mechanism for fault
  tolerance, it also allows one to systematically reason about how to
  add thread-level parallelism, as discussed in~\cite{BLIS3}.
In that paper, it is also demonstrated that, for many-core architectures, it is beneficial to parallelize multiple loops,
including at least one loop that iterates along the $m$ dimension of the problem
and one that iterates along its $n$ dimension.
We now discuss how to merge these ideas with
the proposed mechanisms for fault-tolerance. This creates
constraints on where parallelism can be introduced
because the presence of checksum \gemv\ operations introduces dependencies between iterations of some loops.
}

% \subsection{
\vspace{0.05in}
\noindent
{\bf Loops to parallelize.}
Let us revisit Fig.~\ref{fig:gotoblas_gemm}.
%In~\cite{BLIS3} it is argued that 
Parallelizing Loop~4 is not desirable~\cite{BLIS3}, since then partial results must be computed, stored, and
summed.
For this reason, hereafter we focus on the other
loops,
taking into account that
%\rvdgFromTo{D}{Now, d}
dependencies may appear if iterations of a loop
share a checksum vector:

\noindent{\bf Loop~5.}
\rvdgFromTo{Notice that f}{F}or Loop~5, all the fault tolerance happens within it:
\rvdgFromTo{The iterations}{each iteration} of the loop 
\rvdgFromTo{basically call}{calls} fault tolerant operations.  Thus,
this loop can be trivially parallelized.

\noindent{\bf Loop~3.} 
If Loop~3
\rvdgFromTo{. If this loop}{} is parallelized,
then all the threads 
compute the checksum $d_b := -B_c w$ during the operation that packs $B_c$.
This packing is performed in parallel
by the BLIS framework.
%If it is parallelized, then
Therefore, it is important that the fused pack and \gemv\ operation are parallelized along rows;
if the operation is instead parallelized along columns, each thread will update all of the entries of $d_b$,
requiring either a reduction or mutex synchronization.
Now, each iteration of Loop~3 uses different parts of 
$A$, different parts of $C$, and performs its own checksums using parts of $A$ and $C$.
Furthermore each iteration performs its own independent recomputation of $C$ if an error has occurred.
This means that no other race conditions are introducing during the update of checksum vectors,
except that, if Loop~3 is parallelized,
each thread must have its own independent checksum vectors $d$ and $e^T$.

\noindent{\bf Loop~2.}
\rvdgFromTo{Next, consider i}{I}f Loop~2 is parallelized,
then all threads collaborate in the concurrent packing of $B_c$, as discussed for Loop~3, as well as 
%Also when parallelizing Loop~2,
the operations that pack the $m_c \times k_c$ block of $A_c$
and perform the checksum operations
$d := A_c d_b$ and $e_a^T := -v^T A_c$.
Notice that there are two dimensions along which the loops iterate: $m_c$ and $k_c$.
Each iteration of the loop along the $m_c$ dimension updates the entire vector $e_a^T$,
and each iteration of the loop along the $k_c$ dimension updates the entire vector $d$.
Thus, there are no independent dimensions over which to parallelize this operation,
and either a reduction or a mutex is required for updating either $e_a^T$ or $d$, 
depending on which loop is parallelized.
Because of this, one ancillary benefit of computing the left checksum only if an error is detected 
by the right checksum is that the synchronization when updating $e_a^T$ can be avoided.
If only the right checksum is computed during the packing of $A_c$, then the loop that iterates over $m_c$ can be parallelized.

Next, notice that each iteration of Loop~2 updates the entire vector $d$ inside of the micro-kernel.
Thus, the iterations of this loop are still dependent, and either a mutex or a reduction must be employed when updating $d$.
There are no other potential race conditions when updating checksum vectors,
as each iteration of Loop~2 updates different parts of $e^T$ inside of the micro-kernel.
Furthermore, each iteration of Loop~2 updates a different part of $C$.
When parallelizing Loop~2, notice that each thread parallelizes the recomputation of $C$ if an error has occurred.

\noindent{\bf Loop~1.}
If Loop~1 is parallelized,
all thread will participate in the concurrent packing of $B_c$ and associated operations, as discussed in the bullet point on Loop~3.
Also, all thread will collaborate to pack of $A_c$ and associated operations
and recompute $C$, as discussed in the bullet on Loop~2.

In addition, each iteration of this loop updates the entire checksum vector $e^T$,
introducing potential race conditions that must be avoided,
but this is not an issue if the left checksums are performed lazily.

\noindent{\bf Summary.}
Loop~5 can always be trivially parallelized,
and Loop~3 can be parallelized as long as care is taken when packing $B_c$.
When parallelizing Loop~2, care must be taken to avoid race conditions when updating $d$ inside of the micro-kernel,
and care must be taken to avoid race conditions when packing $A_c$.
When parallelizing Loop~1, care must be taken to avoid race conditions when updating $e^T$ inside of the micro-kernel,
but this is not an issue when the lazy left checksum optimization is employed.

% \subsection{
\vspace{0.05in}
\noindent
{\bf Load imbalance caused by error correction.}
Delays and load imbalance among threads may occur if one or more threads
encounter errors and those threads recompute while other threads have to wait.
%From the text highlighted in green in Fig.~\ref{fig:blis_ftgemm},
If the recomputation is done as soon as errors are detected
this happens within Loop~3 of our algorithm,
at the end of each iteration.
If the lazy left checksum is computed, this operation will also take place within Loop~3,
just before the recomputation is performed.
If loops 3, 4, or 5 are parallelized and, for example,  a single error occurs,
then one thread will perform the recomputation, while the remaining threads will have to wait for it to finish.
On the other hand, if loops~1 or~2 are parallelized, even if only a
single thread encounters an error, all the threads parallelizing those loops will participate in the recomputation.

\rvdgFromTo{\bf Lazy recomputation}{}
It is desirable to parallelize loops~3 and~5, and to do so without introducing
load imbalance upon error recovery overhead.
To this end, we introduce the {\em lazy recomputation} of the parts of $C$ that were calculated incorrectly.
The goal of the lazy recomputation is to ``push'' the recomputation to the outer loops, 
instead of performing it inside Loop~3.
Thus the goal is to delay performing the recomputation of parts of $C \mathrel{+}= AB$,
so that more threads can participate in the recomputation.
The advantage of this approach is better load balancing.
In contrast to lazy recomputation, we will refer to the scheme that performs recomputation as soon as errors are detected
as the {\em greedy recomputation}.

There are a couple of weaknesses to this lazy recomputation approach.
First, the lazy left checksum, the identification of where the errors happened, the repacking of $B_c$,
and the roll-back of the $C$ to $\check C$ are still performed immediately upon encountering an error
and this is done inside of Loop~3.
Thus there is still some load imbalance that may occur, since these operations will
not be performed in cooperation by all threads and some load imbalance will still occur.
%; if for example only Loop~3 and/or Loop~5 are parallelized,
%then all the threads which did not encounter an error will have to wait for these operations to be performed
%by the remaining threads.  However, the threads in the former group,
%will not have to wait for those in the latter group to perform any recomputation.
%the parts of the matrix $\hat C$ that were calculated incorrectly are identified,
%selectively updating $C$ with the parts of $\hat C$ that were calculated correctly,
%and then recomputing the corrupted parts of $\hat C$ later, when more threads can participate.
Second, a disadvantage of the lazy recomputation is that putting off the recomputations for later means that they must be performed cold.
That is, when the error is first detected, $A_c$ is still in the L2 cache,
and $B_c$ is still in the L3 cache (However it is possible that one or both of $A_c$ or $B_c$ has corrupted elements
and must be repacked).
By performing the lazy recomputation later, $A$ or $B$ will no longer be packed and will no longer be in cache,
so the recomputation will be less efficient. 


%It is desirable to parallelize Loop~5 without introducing load imbalance,
%so to do this, one can move the lazy recomputation to outside of Loop~4.

% \subsection{
\vspace{0.05in}
\noindent
{\bf Swapping the two outermost loops.}
If the lazy recomputation is performed after Loop~3, 
then it is possible to remove any load unbalance caused by recomputation that may arise when parallelizing Loop~3,
but this load imbalance will still occur when Loop~5 is parallelized instead. 
Thus it is desireable to perform the lazy recomputation outside of Loop~5,
however this means that the recomputation will be done across multiple rank-$k$ updates.
This introduces a couple of small challenges.
First, the recomputations from different rank-$k$ udpates will happen concurrently,
leading to potential race conditions if an element of $C$ must be recomputed across multiple rank-$k$ updates.
Second, BLIS scales $C$ by $\beta$ inside of the micro-kernel,
so the first rank-$k$ update uses the $\beta$ passed in by the user invoking \gemm,
and subsequent rank-$k$ updates use $\beta=1$. 
This can create difficulties because $C$ must first be scaled by $\beta$ before it is updated,
and each rank-$k$ update uses a different $\beta$.

There are several solutions to the above problems but we believe the simplest is to swap loops 4 and 5.
Proceeding in this manner, we can parallelize the outermost loop over the $n$ dimension,
while performing the lazy recomputation after it has finished,
all while doing all of this within a single rank-$k$ update to avoid the above issues.
This yields an algorithm that slightly deviates from the one used in the GotoBLAS approach~\cite{Goto},
but the resulting algorithm is still one that belongs to the family of high performance algorithms that follows a locally
optimal cache blocking scheme investigated in~\cite{GunH01}.
The main difference is that
the next panel of $B$ to work on is the one next to the current panel of $B$ rather than the one below.
We do not expect this to have an appreciable impact on performance.

%when we perform the lazy recomputation outside of Loop~4 (which used to be Loop~5),
%but still inside of Loop~5 (which used to be Loop~4),
%the lazy recomputation only works on a single rank-$k$ at a time, and the 
%race conditions and issues regarding $\beta$ are solved.
%Furthermore, all threads can participate in the recomputation,
%because it is undesirable to parallelize the loop in the $k$ dimension anyway.

%While swapping these loops yields an algorithm that is slightly different from the one used in the GotoBLAS approach~\cite{Goto},
%this formulation is still one of the members of the family of high performance algorithms that follows a locally
%optimal cache blocking scheme investigated in~\cite{GunH01}.
%The main difference is that%, when performing a rank-$k$ update, 
%the next panel of $B$ to work on is the one next to the current panel of $B$ instead of the one below.
%This should have negligible impact on performance.%, because although accessing $B$ by columns (in a traditional column-major storage scheme)
%is more favorable than accessing $B$ by rows,
%the amount of time between accessing the current panel of $B$ and the next is so great as to obviate this spatial locality.
