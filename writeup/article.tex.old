%\documentclass{sigplanconf}[10pt]
\documentclass{sig-alternate}
%\setlength{\textheight}{8.75in}
%\setlength{\textwidth}{6.5in}
%\setlength{\oddsidemargin}{0in}
%\setlength{\evensidemargin}{0in}
%\setlength{\topmargin}{-0.5in}

%\usepackage{amsmath}
\usepackage{comment}
\usepackage{tikz}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing,patterns,positioning}
\usepackage{amssymb}
\usepackage{ifthen}
\usepackage{color}
\usepackage{colortbl}
\usepackage{rotating}
\usepackage{multirow}
\usepackage{multicol}
\usepackage{url}

\usepackage{pgfplots}

\usepackage{wrapfig}
\usepackage{subfigure}
\usepackage{array}
\usepackage{framed}
\usepackage{multirow}


\usepackage{epsfig}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{algorithmicx}
%\usepackage{amsthm}

\usepackage[normalem]{ulem}

%\usepackage{program}
\usepackage{ifthen}
\usepackage{boxedminipage}
\usepackage{fancyvrb}

\usepackage{soul}
\usepackage{graphicx}
\usepackage{graphics}
\usepackage{moreverb}
\usepackage{rotating}
\usepackage{enumerate}

\usetikzlibrary{arrows}

%\usepackage{flushend}
\usepackage{xspace}

\newcommand{\bigo}{\mathcal{O}}
\newcommand{\libflame}{{\tt libflame} }
\newcommand{\libflamens}{{\tt libflame}}
\newcommand{\conjs}{\bar}
\newcommand{\conjm}{\overline}

\newcommand{\trans}{{\tt trans} }
\newcommand{\transns}{{\tt trans}}

\newcommand{\axpyv}{{\sc axpyv} }
\newcommand{\axpyvns}{{\sc axpyv}}
\newcommand{\copyv}{{\sc copyv} }
\newcommand{\copyvns}{{\sc copyv}}
\newcommand{\dotv}{{\sc dotv} }
\newcommand{\dotvns}{{\sc dotv}}
\newcommand{\dotxv}{{\sc dotxv} }
\newcommand{\dotxvns}{{\sc dotxv}}
\newcommand{\invertv}{{\sc invertv} }
\newcommand{\invertvns}{{\sc invertv}}
\newcommand{\setv}{{\sc setv} }
\newcommand{\setvns}{{\sc setv}}
\newcommand{\scalv}{{\sc scalv} }
\newcommand{\scalvns}{{\sc scalv}}
\newcommand{\scaltv}{{\sc scal2v} }
\newcommand{\scaltvns}{{\sc scal2v}}

\newcommand{\axpym}{{\sc axpym} }
\newcommand{\axpymns}{{\sc axpym}}
\newcommand{\copym}{{\sc copym} }
\newcommand{\copymns}{{\sc copym}}
\newcommand{\scalm}{{\sc scalm} }
\newcommand{\scalmns}{{\sc scalm}}
\newcommand{\scaltm}{{\sc scal2m} }
\newcommand{\scaltmns}{{\sc scal2m}}
\newcommand{\setm}{{\sc setm} }
\newcommand{\setmns}{{\sc setm}}

\newcommand{\axpytv}{{\sc axpy2v}}
\newcommand{\axpytvns}{{\sc axpy2v}}
\newcommand{\dotaxpyv}{{\sc dotaxpyv}}
\newcommand{\dotaxpyvns}{{\sc dotaxpyv}}
\newcommand{\axpyf}{{\sc axpyf}}
\newcommand{\axpyfns}{{\sc axpyf}}
\newcommand{\dotxf}{{\sc dotxf}}
\newcommand{\dotxfns}{{\sc dotxf}}
\newcommand{\dotxaxpyf}{{\sc dotxaxpyf}}
\newcommand{\dotxaxpyfns}{{\sc dotxaxpyf}}

\newcommand{\gemv}{{\sc gemv}\xspace}
\newcommand{\gemvns}{{\sc gemv}}
\newcommand{\ger}{{\sc ger} }
\newcommand{\gerns}{{\sc ger}}
\newcommand{\hemv}{{\sc hemv}}
\newcommand{\hemvns}{{\sc hemv}}
\newcommand{\her}{{\sc her}}
\newcommand{\herns}{{\sc her}}
\newcommand{\hert}{{\sc her2}}
\newcommand{\hertns}{{\sc her2}}
\newcommand{\symv}{{\sc symv}}
\newcommand{\symvns}{{\sc symv}}
\newcommand{\syr}{{\sc syr}}
\newcommand{\syrns}{{\sc syr}}
\newcommand{\syrt}{{\sc syr2}}
\newcommand{\syrtns}{{\sc syr2}}
\newcommand{\trmv}{{\sc trmv}}
\newcommand{\trmvns}{{\sc trmv}}
\newcommand{\trsv}{{\sc trsv}}
\newcommand{\trsvns}{{\sc trsv}}
\newcommand{\trmvth}{{\sc trmv3}}
\newcommand{\trmvthns}{{\sc trmv3}}
\newcommand{\trsvth}{{\sc trsv3}}
\newcommand{\trsvthns}{{\sc trsv3}}

\newcommand{\gemmns}{{\sc gemm}}
\newcommand{\gemm}{{\sc gemm}\xspace}
\newcommand{\dgemm}{{\sc dgemm}}
\newcommand{\hemm}{{\sc hemm}}
\newcommand{\herk}{{\sc herk}}
\newcommand{\hertk}{{\sc her2k}}
\newcommand{\symm}{{\sc symm}}
\newcommand{\syrk}{{\sc syrk}}
\newcommand{\syrtk}{{\sc syr2k}}
\newcommand{\trmm}{{\sc trmm}}
\newcommand{\trmmth}{{\sc trmm3}}
\newcommand{\trsm}{{\sc trsm}}

\definecolor{darkred}{rgb}{0.44,0,0}
\definecolor{darkgreen}{rgb}{0,0.44,0}
\definecolor{darkblue}{rgb}{0,0,0.44}
\definecolor{enrique}{rgb}{0,0,0}

\newcommand{\xeon}{Intel Xeon E5-2680\xspace}
\newcommand{\ixeon}{Intel Xeon E5-2680\xspace}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Jc}{{\cal J}_c}
\newcommand{\Pc}{{\cal P}_c}
\newcommand{\Ic}{{\cal I}_c}
\newcommand{\Jr}{{\cal J}_r}
\renewcommand{\Pr}{{\cal P}_r}
\newcommand{\Ir}{{\cal I}_r}
\newcommand{\Ap}{A_{p}}
\newcommand{\Bp}{B_{p}}
\newcommand{\Cp}{C_{p}}
\newcommand{\Chp}{\hat{C}_{p}}
\newcommand{\Ccp}{\check{C}_{p}}

\newcommand{\tabbox}[2]{\parbox{#1}{\vspace{3pt}#2\vspace{3pt}}}
\newcommand{\tabboxsm}[2]{\parbox{#1}{\vspace{1.5pt}#2\vspace{1.5pt}}}
\newcommand{\tabboxmed}[2]{\parbox{#1}{\vspace{2.2pt}#2\vspace{2.2pt}}}

\newcommand{\cj}{{\small \textsc{cj}}}
\newcommand{\ct}{{\small \textsc{ct}}}
%\newcommand{\mod}{\textsc{mod}}
%\newcommand{\vdim}{\textsc{dim}}

\newcommand{\amd}{{AMD A10}}
\newcommand{\sbr}{{Intel Sandy Bridge}}
\newcommand{\bgq}{{BG/Q PowerPC A2}}
\newcommand{\loongson}{{Loongson 3A}}
\newcommand{\arm}{{ARM Cortex-A9}}
\newcommand{\xeonphi}{{Intel Xeon Phi}}
\newcommand{\tidsp}{{Texas Instruments C6678 DSP}}

%\newcommand{\rvdgFromTo}[2]{\sout{#1} {\color{darkgreen}#2}}
\newcommand{\rvdgFromTo}[2]{#2}

\newenvironment{proto}
{\begin{minipage}{9.5cm}}
{\vspace{1pt}
\end{minipage}}

\newtheorem{mydef}{Definition}
\newtheorem{mytheorem}{Theorem}

\input{flatex}
\input{macros}

%\newcommand{\plusequals}{+\!\!=}
\newcommand{\plusequals}{\mathrel{+}=}

\date{
\today
}

\usepackage{listings}
\usepackage{xcolor}
\usepackage{fancyvrb}
\fvset{tabsize=2}
\definecolor{mygreen}{rgb}{0,0.6,0}
\lstset{ %
language=C,                % choose the language of the code
basicstyle=\footnotesize,       % the size of the fonts that are used for the code
commentstyle=\color{mygreen},
keywordstyle=\color{blue},
%numbers=left,                   % where to put the line-numbers
%numberstyle=\footnotesize,      % the size of the fonts that are used for the line-numbers
%stepnumber=1,                   % the step between two line-numbers. If it is 1 each line will be numbered
%numbersep=5pt,                  % how far the line-numbers are from the code
backgroundcolor=\color{white},  % choose the background color. You must add \usepackage{color}
showspaces=false,               % show spaces adding particular underscores
showstringspaces=false,         % underline spaces within strings
frame=single,           % adds a frame around the code
tabsize=1,          % sets default tabsize to 2 spaces
columns=fixed,
captionpos=b,           % sets the caption-position to bottom
breaklines=true,        % sets automatic line breaking
breakatwhitespace=true,    % sets if automatic breaks should only happen at whitespace
escapeinside={\%*}{*)}          % if you want to add a comment within your code
}            

\pgfplotsset{every axis/.append style={
        scaled y ticks = false, 
        scaled x ticks = false, 
        y tick label style={/pgf/number format/.cd, fixed, fixed zerofill,
                            int detect,1000 sep={\;},precision=3},
        x tick label style={/pgf/number format/.cd, fixed, fixed zerofill,
                            int detect, 1000 sep={},precision=3}
    }
}

\special{papersize=8.5in,11in}
\setlength{\pdfpageheight}{\paperheight}
\setlength{\pdfpagewidth}{\paperwidth}

\conferenceinfo{CONF 'yy}{Month d--d, 20yy, City, ST, Country} 
\copyrightyear{20yy} 
\copyrightdata{978-1-nnnn-nnnn-n/yy/mm} 
\doi{nnnnnnn.nnnnnnn}

% Uncomment one of the following two, if you are not going for the 
% traditional copyright transfer agreement.

%\exclusivelicense                % ACM gets exclusive license to publish, 
                                  % you retain copyright

%\permissiontopublish             % ACM gets nonexclusive license to publish
                                  % (paid open-access papers, 
                                  % short abstracts)

\titlebanner{banner above paper title}        % These are ignored unless
\preprintfooter{short description of paper}   % 'preprint' option specified.

\title{Detecting and Correcting Error within a High-performance Matrix Multiplication}
%\subtitle{Subtitle Text, if any}

\authorinfo{Tyler M. Smith \\ Robert A. van de Geijn}
           {The University of Texas at Austin}
%           {tms@cs.utexas.edu}
           {\{tms,rvdg\}@cs.utexas.edu}
\authorinfo{Mikhail Smelyanskiy}
           {Intel Parallel Computing Lab}
           {mikhail.smelyanskiy@intel.com}
\authorinfo{Enrique S. Quintana-Ort\'{i}}
           {Universidad Jaime I}
           {quintana@uji.es}


\begin{document}

\maketitle

\begin{abstract}
\input 00abstract
\end{abstract}

\input body

\subsection*{Acknowledgments}

This material is based upon work supported by the National Science
Foundation under Award No. ACI-1148125/1340293.
Access to the Stampede supercomputer administered by TACC
is gratefully acknowledged.
This work was carried out while E. S. Quintana-Ort\'{i} was visiting
UT-Austin with support from an ICES  J.T. Oden Faculty Fellowship.
E. S. Quintana-Ort\'{i} was also supported by project CICYT TIN2014-53495-R.

\bibliographystyle{siam}
\bibliography{biblio}

\end{document}
