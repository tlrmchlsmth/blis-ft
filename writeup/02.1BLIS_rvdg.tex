\input{blis_gemm_new}


BLIS implements \gemm\ as three external loops involving
two packing routines around a macro-kernel that computes
the suboperation $C_c \mathrel{+}= A_c B_c$, of size $(m_c,n_c,k_c)$; 
see Fig.~\ref{fig:gotoblas_gemm} and the 
\rvdgFromTo{loops~4-6}{loops~3--5} there.
Note that $A_c,B_c$ correspond to actual buffers involved in data copies,
while $C_c \equiv C(\Ic,\Jc)$ is just a notation artifact,
introduced to ease the presentation of the algorithm.

Internally, the macro-kernel consists of two additional loops around a micro-kernel
that computes
\[
C_c({\cal I}_r,{\cal J}_r) \mathrel{+}= A_c({\cal I}_r,0:k_c-1)~ %\cdot
B_c(0:k_c-1,{\cal J}_r),
\] 
of size $(m_r,n_r,k_c)$; see again Fig.~\ref{fig:gotoblas_gemm} and the loops~1 and~2 there. 
\rvdgFromTo{Finally, t}{T}he BLIS micro-kernel is typically implemented
using assembly code or with vector intrinsics, 
as a loop around a
rank--1 (i.e., outer product) update;  see loop~0 in Fig.~\ref{fig:gotoblas_gemm}.
\rvdgFromTo{On the other hand,
t}{T}he remaining five loops are implemented in C.%
\rvdgFromTo{; see~\cite{BLIS1} for details}{\footnotemark[1]}

The performance of the BLIS implementation strongly depends on that of the 
micro-kernel plus the selected blocking parameters
$m_c$, $n_c$, $k_c$, $m_r$ and $n_r$.
An appropriate choice of these values:
{\em i)} yields a \rvdgFromTo{}{near-}perfect overlap of communication (data fetching into the FPUs) with computation; 
{\color{enrique}
{\em ii)} loads $B_c$ into the L3 cache (if there is one) when this block is packed; 
{\em iii)} loads $B_c$ into the L1 cache in micro-panels of $n_r$ columns (say $B_r$) from inside the micro-kernel;
{\em iv)} loads $A_c$ into the L2 cache when packed; 
%and from there streams the contents of this block into the processor registers from within the micro-kernel;
{\em v)} and, from the micro-kernel, 
         loads/stores $C$ from/to the main memory into/from the file register,
         and streams $B_r/A_c$ from the L1/L2 cache into the FPUs; see~\cite{BLIS1}.
}
In practice, the BLIS implementations have been demonstrated to deliver performance
that rivals those of proprietary libraries
such as Intel MKL~\cite{MKL}, AMD AMCL~\cite{acml}, and IBM ESSL~\cite{essl},\rvdgFromTo{}{ as well open source libraries
like ATLAS~\cite{Wha01} and OpenBLAS~\cite{xianyi2012model},} 
on a wide variety of modern computer architectures\rvdgFromTo{; see,
e.g.,}{}~\cite{BLIS2,BLIS3}.
\rvdgFromTo{}{Since all modern implementations of \gemm\ are based on the same
general approach pioneered by the GotoBLAS~\cite{Goto}, the insights
in this paper can be applied to any of these.}

A key observation in~\cite{Gun01} is that,
for high-performance implementations of \gemm,
the cost moving data is $ \mathcal{O}( mn+mk+kn )$, the cost of checking the correctness is $ \mathcal{O}( mn+mk+kn
) $, and both overheads can be potentially amortized over $ \mathcal{O}( mnk ) $ computation.
%{\color{red} Enrique: shall we mention how we measure these costs? flops vs memops. Checking correctness costs refer both to memops and flops, 
%but given that the former are more expensive, we
%can relate them to memops. 
%A second concern: how does the reader know that the cost of checking the correctness is that? We do not introduce the mechanism till
%section 3.1. We can solve this by putting a reference to previous papers. Minor note: If you define/utilize ``flops'' here, drop the definition in section 4.1.}
Therefore,
there is the possibility of incorporating error checking into the
data movements, much of which is explicitly exposed as packing
into contiguous memory.
The question now becomes how to achieve this.
