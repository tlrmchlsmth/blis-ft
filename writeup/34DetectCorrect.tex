%\section{Introducing Fault Tolerance and Approximate Computing into \gemm}
\label{sec:ftgemm}

%{\bf Pending: Open this section by explaining the type of errors that we can deal with.
%Include a brief summary in the introduction, and indicate that in the description of
%the organization of the paper.\\
%We can assume that the memory and caches are reliable and error only occur in the
%register bank or the FPUs. For the future, we can allow errors in the caches. We should then
%adapt the error correction mechanism to flush the cache that was detected to be erroneous.
%May be this can be related to the left and right checksum vectors.
%For example: if we detect two consecutive errors on the left checksum vector with $A_c$ we
%should flush that block from the L2 cache.}

% \subsection{Checksum vectors for fault tolerance}

We open this section with a brief review of the two-sided checksum-based method for \gemm
introduced in~\cite{Hua84}, and the technique formulated in~\cite{Tur00,Gun01}
to detect soft errors and distinguish them from those intrinsic to the use of finite precision arithmetic.
%to detect errors other than those intrinsic to the use of finite precision
%arithmetic.

Assume for the moment that we are interested in computing the basic product $C := A B$.
Consider next the augmented matrices
\[
A^* \!=\! 
\left(
\renewcommand{\arraystretch}{1.2}
\renewcommand{\arraycolsep}{1pt}
\begin{array}{c}
A\\ \hline
v^T A
\end{array}
\right),
B^* \!=\! 
\left(
\begin{array}{c|c}
B & B w
\end{array}
\right),
{C}^* \!=\! 
\left(
\renewcommand{\arraystretch}{1.2}
\begin{array}{c|c}
{C}    & {C}w \\ \hline
v^T{C} & v^T {C} w
\end{array}
\right),
\]
where
$v^T$ and $w$ are respectively row and column vectors with
$m$ and $n$ random components. 
%and the ``hat'' superscript ``$\hat{~~}$'' on $C$ is used to distinguish the computed matrix from the sought-after result.
In the absence of errors and in exact arithmetic, 
the result then satisfies ${C}^* = A^* B^*$, and a simple
mechanism to detect an error is to verify whether
\begin{equation}
\begin{array}{r@{\hspace{2pt}}c@{\hspace{2pt}}l@{\hspace{2pt}}l@{\hspace{2pt}}ll}
  \| d  \|_{\infty}  &=& \|{C} w   - A (B  w)\|_{\infty}  &> &0 & \mbox{\rm or}\\
  \| e^T\|_{\infty}  &=& \|v^T  {C} - (v^T A) B\|_{\infty} &>& 0.
\end{array}
\label{eqn:checksum}
\end{equation}
Here, $d$ and $e^T$ are referred to 
as the {\em left and right checksum vectors} respectively, yielding a two-sided error
detection method.
Note that in case $w$ is orthogonal to one of the rows of $B$
or $v^T$ is orthogonal to one of the columns of $A$,
a one-sided error detection method is not guaranteed to detect an error if $A$ or $B$ is corrupted.
%depending on which one-sided error detection method is used and which matrix the error occurs.
However we consider this unlikely to occur in practice,
and we assume that this will not occur.

In a real scenario, round-off error occurs and the previous criteria are modified in
to declare an error if
\begin{equation}
\begin{array}{r@{\hspace{2pt}}c@{\hspace{2pt}}lcl}
  \| d \|_{\infty} &>& \tau  \| A \|_{\infty}  \| B \|_{\infty} ~~\mbox{\rm or}\\ [0.01in]
  \| e^T \|_{\infty} &>& \tau  \| A \|_{\infty}  \| B \|_{\infty},
\end{array}
\label{eqn:norm}
\end{equation}
where $\tau = k \, u$, and $u$ denotes the unit round-off of the machine~\cite{Tur00,Gun01},
and $k$ is the inner dimension of the matrix multiplication.


\input fig_err_loc
{\color{enrique}
The location of the errors may be determined by inspecting the entries of $d$ and $e^T$
Concretely, if the $j$-th entry of vector $d$ satisfies $| d_j | > \tau  \| A \|_{\infty}  \| B \|_{\infty} = \rho$, there is a
significant error somewhere in 
the $ j^{th} $ column of $C$.
Similarly if $ |e^T_i| > \rho$, there is an error somewhere in the $ i^{th} $ row of $C$.
Then, each error will occur at some combination of $i$ and $j$ coordinates,
but there may be false positives, as illustrated in Figure~\ref{fig:err_loc}.}


%An alternate method to detect the location of errors was proposed in~\cite{Wu11}.
%This method is to use two right multiplies.
%The first with a checksum vector of all 
%{\color{blue}
%The above discusses detecting the existence of an error, add here a discussion
%about detecting location of an error.
%You can do it with a left multiply and a right multiply.
%You can also do it with 2 right multiplies~\cite{Wu11}.
%}

%\subsection{}
%
%\rvdgFromTo{{Approximate computing}
%The criteria in~(\ref{eqn:norm}) for error detection are based
%on classical numerical analysis theory~\cite{GVL}.
%However, for applications with a higher tolerance for errors,
%we can easily tune (i.e., raise) the threshold. This in turn
%leads to a version of \gemm for approximate computing
%that trades off accuracy for performance, as errors which do not ``propagate''
%beyond a certain level do not force the application of error correction,
%therefore saving time.
%}{}
%
%\rvdgFromTo{Hereafter we will refer to fault tolerance, but all the insights and results 
%also apply to a case where approximate computing is the goal.
%}{I suggest we combine Sections 3 and 4, and maybe even 5.}

% \subsection{
\vspace{0.05in}
\noindent
{\bf Handling $C \mathrel{+}= A B$.} \label{subsec:extended}
When performing the ``basic'' operation $C := AB$,
the checksum detection scheme introduced in the previous section is sufficient.
In this case, those parts of $C$ that may be corrupted can be simply recomputed.
However in case of the more complicated operation $C \mathrel{+}= A B$,
two additional problems arise.
First the checksum approach simply does not apply to this operation;
it only applies to the basic operation.
Secondly, if a corrupted (intermediate) result is added to some element of the matrix $C$,
then it may be impossible to recover that element of $C$.
We present two methods to deal with these issues.

First, one can perform the operation $\hat{C} := AB$,
using checksum vectors to test the correctness of this operation.
If no error was detected, $\hat{C}$ is next added to the original matrix $C$.
A second possibility is to checkpoint (i.e., copy) $C$ as $\check{C} := C$.
Next, perform $T := AB$ and $C \mathrel{+}= T$ with errors detected during the computation of $T := AB$.
If errors occur, $C$ can be rolled back by performing $C := \check{C}$, and restarting the operation.
%This may seem more complicated than the first option,
%however 
%in Section{\color{red}???} we will explore how to fuse the operations $\check{C} = C$, $T = AB$, and $C \mathrel{+}= T$.
%%but notice the operations $\check{C} = C$, $T = AB$, and $C \mathrel{+}= T$ have the opportunity to be fused
%%such that $C$ is only brought into memory once, and $T$ exists only in registers.
%{\color{red} Enrique: is this explanation of the second possibility correct? As it is explained, this will require two
%workspaces: one for $\check{C}$ and one for $T$, when one is sufficient.
%Shouldn't this be: $\check{C} = C$, $C = AB$ and$C \mathrel{+}= \check{C}$? There may be some performance reason for doing it
%in that way that I don't know, though.}
  


% \subsection{
\vspace{0.05in}
\noindent
{\bf Comparison with conventional ABFT.}
Traditional approaches for ABFT often involve estimating 
the difference between a corrupted result from an operation
and the correctly computed result, and then subtracting that difference
from the corrupted result to yield the correct result.
We chose a simpler checkpoint and restart approach 
for two reasons.
First, we have concerns that subtracting the estimated error from the computed result
may give rise to numerical stability issues, mainly due to catastrophic cancellation.
Second, in such schemes, it is necessary to determine exactly both what the errors are 
and where they occurred.
Such schemes may either fall apart or require extra checksums to account for 
multiple errors occurring in the same row or column of the result,
for instance when an error occurs in $A_c$ or $B_c$ and propagates respectively corrupting several elements in a row or column of $C$.
%{\color{red} like, e.g. when an error occurs in $A_c$ or $B_c$ and propagates respectively corrupting several elements in a row or column of $C$.
%Obviously this can't be here since we don't talk about corrupting $A_c$ or $B_c$ until later.~~~~~~
%Enrique: Actually, we already mentioned that scenario in section 2.2.}
In contrast, our approach can tolerate multiple errors in the same row or column of $C$.
At worst, these lead to false positives, and require the recomputation of uncorrupted elements of $C$.
%{\color{red} This section obviously needs more work but I'm tired.~~~~~~
%Enrique: I did some polishing, but I don't know how deep you want to go.}

%\subsection{Identifying Where the Error Occured, and What Was Affected}
