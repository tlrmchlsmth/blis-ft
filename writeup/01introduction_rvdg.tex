Algorithm-based fault tolerance (ABFT) is an application-specific
approach that takes advantage of specialized properties of the
application to embed error detection and correction within the underlying
algorithm. For example, matrix operations can take advantage of ABFT
by verifying a pre-proved checksum relationship at the end of the
calculation~\cite{Hua84}. These ideas have been
further extended to tolerate fail-stop failures in distributed
environments without checkpointing~\cite{chen2008algorithm}. 
%In~\cite{Wu11},
%on-line error recovery is integrated within matrix multiplication by detecting
%errors after individual blocked outer-product computations of the result. 

Matrix multiplication of two dense matrices (\gemm)
plays a key role in scientific and engineering applications, as many complex codes are built on top of linear 
algebra libraries (e.g., LAPACK~\cite{LAPACK3}, {\tt libflame}~\cite{CiSE09},
ScaLAPACK~\cite{ScaLAPACK:guide}, Elemental~\cite{Poulson:2012:ENF},
to name a few) that internally cast a large fraction of their computations in terms of \gemm. 
\rvdgFromTo{}{Moreover, techniques designed for \gemm\ can often be easily generalized to other matrix operations.}
Developing a high-performance fault-tolerant \gemm\ is therefore a crucial first step towards creating efficient fault-tolerant
linear algebra libraries and, in consequence, more 
reliable scientific and engineering applications.


In~\cite{Wu11},
on-line error recovery was integrated within matrix multiplication by detecting
errors after individual blocked outer-product computations of the result. 
The work in~\cite{Gun01} expanded upon the original ABFT paper~\cite{Hua84} and provided the key insights that underly the current work:  To implement a high-performance \gemm\ one must amortize the movement of $ O( n^2 ) $ data (the matrices) over $ O(n^3) $ computation.  Checking the integrity of the data similarly requires $ O( n^2 ) $ operations.  
Concretely, our work demonstrated that, for the existing architectures and the best known algorithm for \gemm\ at that time,
high performance could be maintained.
The current paper reexamines the results from~\cite{Gun01} for the significantly more advanced modern multicore and many-core architectures that did not exist in 2001.  While the algorithm for implementing \gemm\ from~\cite{GunH01} that underlied the work in~\cite{Gun01} was then state-of-the-art, shortly afterwards Goto introduced the techniques that are  at the core of most recent high-performance implementations, including those incorporated in GotoBLAS~\cite{Goto}, OpenBLAS~\cite{xianyi2012model}, Intel's MKL~\cite{MKL}, AMD's ACML~\cite{acml}, and IBM's ESSL~\cite{essl}.  Since this approach exposes many more levels of blocking, a careful study of where ABFT can be added to these implementations is in order.
In addition, the BLAS-like Library Instantiation Software (BLIS)~\cite{BLIS1,BLIS2,BLIS3} framework now provides a convenient infrastruction with which to evaluate and analyze insights.

In short, the current work provides a thorough examination of how to incorporate ABFT into practical implementations of \gemm\ for modern multicore and many-core architectures, supporting applications that can execute on Exascale architectures by providing fault-tolerance at the node level and demonstrating high performance.
For this purpose, we revisit \gemm\ with the goal of providing
a software layer that can tolerate multiple errors
while retaining high performance. 
We consider {\em silent data corruption}~\cite{Bron08,Muk05} only, which does not abort the program execution
but may yield an incorrect result of the computation if not properly addressed. 
Furthermore, %we assume these errors occur 
%when the operands are in the processor datapath (e.g., in the floating point units, 
%or FPUs, the reordering buffer, the register bank, etc.), 
%they only affect the results of the floating-point arithmetic operations,
we assume that these errors manifest themselves in incorrect results of floating point 
computation%\footnote{Data corruption 
%during \gemm\ affecting the integer arithmetic, e.g., to calculate a memory address may
%result in loading/storing an incorrect data/result floating-point number. 
%Thus, they will likely trigger an error in
%in the result, and they can be expected to be detected by the same fault tolerance mechanism. 
%Data corruption in the branch logic can be expected to produce a hard error.}
.
As such they can originate anywhere in the processor datapath; e.g., in
the register file, floating point units (FPUs), reorder buffer, front-end of the pipeline,
or cache controller. In this initial study, we finally assume that errors in the L1 cache memory or below are solved via conventional 
error-correcting code (ECC) memory.

In the remainder of the paper, we will consider the ``extended'' form of \gemm,
\rvdgFromTo{$C \mathrel{+}= A\cdot B$}{$C \mathrel{+}= A B$}, where
$C \in \R^{m \times n}$, 
$A \in \R^{m \times k}$ and
$B \in \R^{k \times n}$, though in some cases we will simplify it to the more ``basic''
case \rvdgFromTo{$C = A\cdot B$}{$C = A B$}.
Hereafter, we will define the problem dimension using the triple $(m,n,k)$.

%%% {\color{red} The rest of the paper is organized as follows...}

