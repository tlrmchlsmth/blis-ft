% I think we're good on this now
%{\color{red} Notice: there is some confusion as to the numbering of
%  the loops.  4-6 should be 3-5.}

\input{blis_ftgemm2}

\vspace{0.05in}
\noindent
{\bf Focusing on the Third Loop.}
%\subsection{Low-cost fault tolerance for the \ixeon}
Whether the overhead costs in Fig.~\ref{tab:balance} are acceptable or not strongly depends
on the problem dimensions as well as the concrete values of the five BLIS blocking parameters.
Consider for instance the \ixeon target processor (the Intel Xeon Phi Knight's corner has a similar story):
$(m_c, n_c, k_c) = (96,4096,256)$ and
%$m_c = 96$,
%$n_c = 4,096$,
%$k_c = 256$, and
$(m_r, n_r) = (8,4)$
This particular value of $n_c$ likely turns 
the checksum approach at the two outermost Loops 
\rvdgFromTo{(Loops~6 and~5)}{(Loops~5 and~4)} too expensive from the point of view of workspace, as they both require
storage for $m \times n_c = m \times 4,096$ numbers.
Furthermore, the small values of $m_r$ and $n_r$ make the detection of errors
inside the innermost three Loops 
\rvdgFromTo{(Loops~2, 1, and~0)}{(Loops~2, 1, and~0)} relatively expensive due
to the high overhead
\rvdgFromTo{}{for detection}
they would incur. 
Therefore, 
the only reasonable choice for this particular architecture 
is to apply the error recovery mechanism
inside the macro-kernel (Loop~3), that is, when
$C_c \mathrel{+}= A_c B_c$ is computed. This choice balances a moderate workspace 
($96 \times 4,096$ numbers), with reasonable error correction cost, and 
a low relative overhead for error detection:
${\cal O}_d(m_c,n_c,k_c) = (4m_cn_c+5m_ck_c+5k_cn_c)/(2m_cn_ck_c) \leq 5/m_c = 5/192 \approx 
2.6\%$. 
%{\color{red} Enrique: comparison of flops for computation of macro-kernel vs memops for detection of error. Anything to say?} 
Note that the memory operations (memops) are more costly than the flops when performing the checksums.
As we detail next, this cost can be reduced if, e.g.,
we reuse the matrix norms for $A_c,B_c$ or the results of certain \gemv\ products.

Figure~\ref{fig:blis_ftgemm2} shows a fault-tolerant version of BLIS \gemm, with
the changes with respect to the original algorithm highlighted in colors:
red for the computation of the right checksum vector; 
blue for the computation of the left checksum vector; and
green for those parts involved in
error recovery.
We next review each one of this parts in detail.

% \subsection{
\vspace{0.05in}
\noindent
{\bf Right checksum.}
We start by noting that $d_b$ and $d$ are small column vectors, of dimension $k_c$ and $m_c$
respectively. The two \gemv\ products 
involved in this checksum,
$d_b := -B_c w$ and
$d := A_c d_b$, 
can be performed when the
corresponding blocks, $B_c$ and $A_c$, are packed and, implicitly, loaded into
the L3 and L2 caches, respectively. 
In BLIS, packing is a memory-bound operation and, therefore, we can expect
that adding a \gemv\ to it has no 
\rvdgFromTo{}{real} effect on its execution time.
In the \gemv\ with $B_c$, the cost of this operation is furthermore amortized
over the iterations of Loop~3 (i.e., several executions of the
macro-kernel), 
\rvdgFromTo{turning
the actual cost of this larger \gemv\ even lower.}
{amortizing the cost of this larger \gemv\ over more computation.}
Similar arguments hold for the computation of 
$\| B_c \|_{\infty}$ and 
$\| A_c \|_{\infty}$.

\rvdgFromTo{Following with this, e}{E}ach execution of the micro-kernel computes a contribution 
$C_c(\Ir,\Jr)$ to be accumulated into the corresponding micro-block of $C_c$ if 
the global result passes the checksum test. 
We exploit that, after the execution of the micro-kernel,
$C_c(\Ir,\Jr)$ is available in the processor registers to 
compute the \gemv\ 
$d(\Ir) \mathrel{+}= C_c(\Ir,\Jr) w(\Jr)$ as part of
the computation $d := C_c w ~(-A_c B_c w)$.
Unlike the packing operations, this \gemv\ is not embedded into a memory-bound operation
and its cost is explicitly exposed. However, we expect its impact to be negligible,
since it requires $2m_cn_c$ flops compared with the $2m_cn_ck_c$ flops of the macro-kernel,
with $k_c$ in the range of 256.

% \subsection{
\vspace{0.05in}
\noindent
{\bf Left checksum.} 
In this case, $e_a^T$ and $e^T$ are both small row vectors of dimension
$m_c$. The computation of this checksum follows the same
ideas just exposed for its right counterpart,
but in this case the vector $w$ is first multiplied by $A_c$ and then by $B_c$.
Because of this, the fault-tolerance overhead is greater.
\rvdgFromTo{In}{While for} the right 
\rvdgFromTo{}{checksum} case, the \gemv\ operation can be used across multiple iterations of 
Loop~3, \rvdgFromTo{but in this case,}{now}
both \gemv\ operations must be performed inside of the body of Loop~3.%
\footnote{
\rvdgFromTo{}{Notice the benefit of how BLIS is structured: in the GotoBLAS and
OpenBLAS implementations of \gemm, the macro-kernel is 
typically assembly coded, making it difficult to add the kind of
changes we are now discussing.}
}
Thus, the relative overhead introduced by the \gemv\ involving
$B_c$ is no longer amortized over several macro-kernels, and is roughly
given by $2k_cn_c/(2m_cn_ck_c) = 1/m_c = 1/96 \approx 1.04\%$.
The operation $e_a^T B_c$ cannot be performed during a packing
operation, increasing its cost further.

Because the left checksum is more expensive than the right checksum,
it is preferable to avoid computing the former.
Since it is possible to detect errors using
only the right checksum,
when an error is detected with this mechanism,
the left checksum can be obtained in order to locate where the error occured.
During normal operation, no errors are expected to occur, and
the fault-tolerant \gemm\ will thus be more efficient.
Proceeding in this manner, 
we shift part of the cost from error detection to
to error correction.
We call this approach the {\bf lazy left checksum}.

\vspace{0.05in}
\noindent
{\bf Preventing false negatives.}
Performing these checksums while packing can be dangerous,
since if $A$ or $B$ is corrupted while it is in some
unprotected layer of memory, that same corrupted $A$ or $B$ can be both copied  
into $A_c$ or $B_c$ and used to compute the left or right checksum.
This could result in a false negative.
A way to prevent this from happening is to read $A$ or $B$ from the
fastest protected memory layer twice: Once for packing, and once for computing the checksum.
This has a larger performance impact the fewer levels of memory that are protected.

%One problem with this approach may arise if the vector $w$ is orthogonal
%to one of the rows of $B_c$. In such scenario, if an element of $A$ becomes corrupted,
%\rvdgFromTo{then}{} the approach is not guaranteed to detect the corruption.
%This is unlikely to occur, but one solution would be to use a few right checksums
%instead of a single one in order to further reduce the chances of such event. 
%For the experiments performed in this paper, we only use a single right checksum.

%Concretely, in this case
%$e^T = e_a^T \cdot B_c ~(= -v^T \cdot A_c \cdot B_c)$
%is computed piecewise, in blocks of $n_r$ elements.
%In Figure~\ref{fig:blis_ftgemm}, this is part of the
%\gemv\ product
%$e^T(\Jr) \mathrel{~}= e_a^T \cdot B_c(0:k_c-1,\Jr)$.
%%which preloads a slab of $k_c \times n_r$ elements of $B_c$ into the L1 cache, to be
%%then repeatedly accessed by the micro-kernel inside Loop~1.
%Alternatively, this small \gemv\ can be implemented into a specialized case of micro-kernel
%to be executed the first iteration of Loop~1.
%In any case, this collection of \gemv\ are not overlapped with a packing operation,
%but have a visible impact on the execution time equivalent to loading
%the $k_c \times n_r$ slab $B_c(0:k_c-1,\Jr)$ from the L3 cache into the L1 cache
%per iteration of Loop~2.
%However, we note that such loading also occurred implicitly, as part of the execution
%of the micro-kernels involved in the initial iteration of Loop~2.
%The second aspect to note is that the relative overhead introduced by the \gemv\ involving
%$B_c$ is no longer amortized over several macro-kernels, and is now roughly
%given by $2k_cn_c/(2m_cn_ck_c) = 1/m_c = 1/96 \approx 1.04\%$.

%\subsection{Error prevention.}
% \subsection{
\vspace{0.05in}
\noindent
{\bf Handling $C \mathrel{+}= AB$.} \label{sec:extended}
We will now discuss the costs and tradeoffs of the two options for handling the extended
\gemm\ operation $C \mathrel{+}= AB$ when injecting fault tolerance at the third loop.

The first option is to check
for errors before accumulating $\hat{C}_c$ into the final block of $C$ in order to prevent
corruption of the result. 
There are a couple of problems with this approach.
(1) Since $\hat{C}_c$ is a larger $m_c \times n_c$ buffer we can expect
its contents to lie low in the memory hierarchy. The update
$C({\cal I}_c,{\cal J}_c) \mathrel{+}= \hat{C}_c$ is therefore a memory-bound operation.
(2) If this operation is performed all at once, it may disturb the contents of the caches.
Notice the following:
$\hat{C}_c$ is $m_c \times n_c$.
$B_c$ is $n_c \times k_c$,
$m_c$ is typically on the same order of magnitude of $k_c$,
and thus $B_c$ and $\hat{C}_c$ are of similar size.
If there is an L3 cache, $n_c$ and $k_c$ are chosen such that $B_c$ occupies a large fraction of it.
Because of this, the operation $C({\cal I}_c,{\cal J}_c) \mathrel{+}= \hat{C}_c$ is likely to bump
large portions of $B_c$ out of the L3 cache.
The takeaway is that in the GotoBLAS approach,
$B_c$ is designed to reside in the L3 cache and then reused across many macro-kernels.
However if this option is used, $B_c$ will have to be re-read from main memory for each macro-kernel that uses it.

%with its cost given by the time required to fetch the contents of
%$C({\cal I}_c,{\cal J}_c)$ from the memory (and writing the result back there). 
%a similar cost was paid in the non-tolerance \gemm\ 
%in order to load (and store back) the contents
%of the $m_r \times n_r$ block of $C$ involved in each invocation to the micro-kernel.
%The difference is that, at each invocation micro-kernel in the fault-tolerant \gemm,
%$m_r \times n_r$ registers are initialized to zero, and when the micro-kernel completes
%its execution, these results are written back to a block of a compact buffer 
%$\hat{C}_c$. 

%After the macro-kernel completes its execution, 
%each block $\hat{C}_c$ composing it 
%is then recovered for the accumulation and the result is streamed back to memory.
%Thus during each micro-kernel, instead of both reading and writing $m_r \times n_r$ elements from and to main memory,
%those elements only need to be written.
%However, the later update $C_c \mathrel{+}= \hat{C}_c$ must read $m_c \times n_c$ elements of $C_c$
%and $m_c \times n_c$ elements of $\hat{C}_c$, and must write $m_c \times n_c$ elements to $C_c$.
%This amounts to an extra $m_c \times n_c$ elements that must be read, most likely from main memory,
%and an extra $m_c \times n_c$ elements that must be written, again to main memory.


The other option is to checkpoint $C_c$ into $\check{C}_c$,
perform $T := A_c B_c$,
and then $C_c \mathrel{+}= T$.
These three operations can be fused during the BLIS micro-kernel in order to reduce memory movements.
The micro-kernel computation,
$\hat{C}_c(\tilde{\cal I}_r,\tilde{\cal J}_r) := A_c(\tilde{\cal I}_r,0:k_c-1) B_c(0:k_c-1,\tilde{\cal J}_r)$
is generally implemented as a block dot-product
$T := A_c(\tilde{\cal I}_r,0:k_c-1) B_c(0:k_c-1,\tilde{\cal J}_r)$
followed by the update
$C(\tilde{\cal I}_r,\tilde{\cal J}_r) \mathrel{+}= T$, where the $m_r \times n_r$ matrix $T$ resides in registers only.
Thus, at the same time that $C(\tilde{\cal I}_r,\tilde{\cal J}_r)$ is brought into registers to update $T$,
it can also be used to checkpoint $\check{C}(\tilde{\cal I}_r,\tilde{\cal J}_r)$.
Thus the only extra memory movements needed for this technique are an extra
$m_c \times n_c$ elements of $C_c$ that must be written to $\check{C}_c$.
Since each element of $\check{C}_c$ is only used once during each macro-kernel,
it most likely lies high in the memory hierarchy, and possibly in main memory.
Thus this approach saves $m_c \times n_c$ elements that do not need to be read from main memory
per macro-kernel as compared to the first approch.
The disadvantage is that recovering from an error now requires the corrupted elements of $C$
to be rolled back. 
However, as errors are expected to be relatively rare,
the second aproach may be preferred, as it is cheaper when detecting errors.

%{\color{blue} 
%\rvdgFromTo{Note that t}{T}he big problem here is that the operation $C \mathrel{+}= \beta \hat{C}_c$ is fully exposed,
%so that it is more expensive than performing it during the micro-kernel.
%In order to make this more efficient, it would be preferable to overlap this update operation
%with computation.
%We attempted to use hyperthreads to perform $C \mathrel{+}= \beta \hat{C}_c$, 
%which would overlap computation performed
%by one hyperthread with the update perfomed by the other hyperthread on the core.
%However, this did not work out. Simply having two hyperthreads active during a \gemm\ operation
%reduced efficiency by around fifty percent, because certain hardware resources are partitioned between the hyperthreads.
%}

% \subsection{
\vspace{0.05in}
\noindent
{\bf Error correction.}
Detecting errors at the macro-kernel level, via the checksum vectors,
while computing the product with the granularity of a micro-kernel opens the door to a selective error
recovery method. For example, a single error
in a single entry of $d(\tilde{\cal I}_r)$
combined with an error in a single entry of $e^T(\tilde{\cal J}_r)$ points in the direction
of a problem during the \rvdgFromTo{computation of the}{} micro-kernel
\rvdgFromTo{}{computation}
$C_c(\tilde{\cal I}_r,\tilde{\cal J}_r) := A_c(\tilde{\cal I}_r,0:k_c-1) B_c(0:k_c-1,\tilde{\cal J}_r)$.
We can therefore recompute 
\rvdgFromTo{that specific micro-kernel}{only that specific result}, if
we consider that to be an indivisible unit of computation
\rvdgFromTo{}{(which it is the way BLIS is structured)}.
This implies that error detection roughly comes with the overhead ${\cal O}_d(m_c,n_c,k_c)$ (as we explained in
this section, it is actually lower if we reuse some of the operations), but the cost of error correction
is 
\rvdgFromTo{}{reduced from} ${\cal O}_c(m_r,n_r,k_c)$ 
\rvdgFromTo{instead of}{to} ${\cal O}_c(m_c,n_c,k_c)$.

\rvdgFromTo{Note that t}{T}he presence of one (or multiple) corrupted item(s) in $d(\tilde{\cal I}_r)$,
but one (or multiple) corrupted item(s) in two blocks
$e^T(\tilde{\cal J}_r^1)$,
$e^T(\tilde{\cal J}_r^2)$ will require the recomputation of two micro-kernels.
If the errors occur in two blocks $d(\tilde{\cal I}_r^1)$ and $d(\tilde{\cal I}_r^2)$
and two blocks $e^T(\tilde{\cal J}_r^1)$,
$e^T(\tilde{\cal J}_r^2)$, then four micro-kernels have to be recomputed, and so forth

On the other hand, nothing prevents us from being more aggressive in the error correction strategy
in case of single (or few) errors per micro-kernel. 
An error in a register holding a value for $C_c(\tilde{\cal I}_r,\tilde{\cal J}_r)$
will appear as single corrupted elements in both $d(\tilde{\cal I}_r)$ and $e^T(\tilde{\cal J}_r)$. 
A single error in a register holding a value of $A_c(\tilde{\cal I}_r,0:k_c-1)$ will corrupt a full row%
\footnote{This is true if the error occurs immediately after
that particular entry of $A_c(\tilde{\cal I}_r,0:k_c-1)$ is loaded into the register, but before it is used, and persists as long as that data item is in the register. 
If the error occurs when it value already loaded in the register and used in part of the update of 
$C_c(\tilde{\cal I}_r,\tilde{\cal J}_r)$,
then the corrupted data will only appear in a few elements of the same row of the latter. 
A similar comment holds for the errors in the registers holding $B_c(0:k_c-1,\tilde{\cal J}_r)$ and the columns
of $\hat{C}_c(\tilde{\cal I}_r,\tilde{\cal J}_r)$.}
of $\hat{C}_c(\tilde{\cal I}_r,\tilde{\cal J}_r)$
and all entries of $d(\tilde{\cal I}_r)$.
Analogously, a single error in a register holding a value of $B_c(0:k_c-1,\tilde{\cal J}_r)$ will corrupt an entire column of $C_c(\tilde{\cal I}_r,\tilde{\cal J}_r)$
and all of $e^T(\tilde{\cal J}_r)$.
In all these three cases we can recompute only the corrupted entries of $C_c(\tilde{\cal I}_r,\tilde{\cal J}_r)$,
as dictated by the specific corrupted entries of $d(\tilde{\cal I}_r)$ and $e^T(\tilde{\cal J}_r)$.

% \subsection{
\vspace{0.05in}
\noindent
{\bf Repacking $A$ or $B$.}
%\rvdgFromTo{Remember that i}{I}f an error occurs in either $A_c$ or $B_c$, this error can propogate, 
%corrupting many elements of $C_c$.
%\rvdgFromTo{In addition, i}{I}
It is important that errors in either $A_c$ or $B_c$ do not propagate %affect the recomputation
%and it is desireable to prevent these errors from 
corrupting more elements of the result than necessary.
\rvdgFromTo{Because we do not want these errors to occur in the recomputation,}{Thus,}
\rvdgFromTo{it is desireable to either }{we must} detect which elements of $A_c$ or $B_c$ may be corrupted
and \rvdgFromTo{then}{} repack them\rvdgFromTo{}{}.
An alternate solution is to use the original buffers containing $A$ or $B$ during recomputation.

When detecting and correcting errors at the macro-kernel level,
by the time that errors are detected and corrected,
$A_c$ is no longer used and the buffer containing the packed block of $A_c$ has been recycled to store the next block of $A$.
\rvdgFromTo{For this reason, it is suggested that it is desireable
  to}{This suggests that} \rvdgFromTo{recompute}{} $C_c$
\rvdgFromTo{}{be recomputed} by using
the original matrix $A$ rather than repacking $A$.
On the other hand $B_c$ is reused many times after errors in it are detected,
so repacking corrupted elements of $B_c$ is beneficial to prevent
errors from \rvdgFromTo{}{also} occuring in 
\rvdgFromTo{ever}{future} iterations of Loop~3.

\rvdgFromTo{Note that w}{W}hile one cannot determine for certain if an element of $A_c$ or $B_c$
is corrupted, if (for instance) an entire column of $C_c$ is corrupted,
in such scenario it is most likely that an element of $B_c$ was corrupted,
rather than each element of $C_c$ having been corrupted independently.
